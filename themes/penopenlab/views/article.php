<?php
	$_CI = & get_instance();
?>
<?php $this->render("header"); ?>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <legend><?php echo $article->title; ?></legend>
                <address>
                    Author : <?php echo $article->author->nickname; ?><br/>
                    Updated : <?php echo date('d F Y', $article->date_updated); ?><br/>
                    <?php $categories = $article->categories; ?>
                    Category : <?php foreach ($categories as $category): ?><a href="#<?php echo $category->name; ?>"><span class="label label-primary"><?php echo $category->title; ?></span></a><?php endforeach; ?>
                </address>
                <p><?php echo $article->content; ?></p>
            </div>
        </div>
	</div>
</div>    

<?php $this->render("footer"); ?>
