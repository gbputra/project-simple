<?php
	$_CI =& get_instance();
	$_CI->load->library("form_validation");
	$_CI->load->library("message");
?>
<?php $this->render('header'); ?>
		<style>
            body {
                background-color: #f0f0f0;
            }
            #login {
                width: 400px;
                padding: 2% 0 0;
                margin: auto;
                font-weight: lighter;
				font-size: 14px;
            }
            #login .box .box-heading {
                border: none;
                font-family: 'Open Sans', sans-serif;
                padding-top: 40px;
            }
            #login .heading {
                font-size: 32px;
            }
            #login .form-control {
                margin: 5px 0;
            }
            #login-btn {
                border-radius: 0;
            }
            #whatsup {
                margin-top: 70px;
                font-family: 'Open Sans', sans-serif;
            }
        </style>
        <div class="content">
            <div id="login">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-heading">
                            <div class="heading">
							<img width="300px" src="<?php echo $this->theme_url; ?>img/logosympho.png"></div>
                        </div>
            
                        <div class="box-body">
                            <form role="form" method="post" action="<?php echo conf_item('base_url');  ?>auth/login">
                            	<input type="hidden" name="<?php echo csrf_token_name(); ?>" value="<?php echo csrf_hash() ?>" />
                            	<div>
									<?php $_CI->message->render_type("error", "<div class=\"form-group callout callout-danger\">","</div>"); ?>
                                    <?php $_CI->message->render_type("warning", "<div class=\"form-group callout callout-warning\">","</div>"); ?>
                                    <?php $_CI->message->render_type("info", "<div class=\"form-group callout callout-info\">","</div>"); ?>
                                    <?php $_CI->message->render_type("success", "<div class=\"form-group callout callout-success\">","</div>"); ?>
                                </div>
                                <div class="form-group input-text <?php echo form_error('username') ? 'danger' : 'primary'; ?>">
                                    <span class="input-icon glyphicon glyphicon-user"></span>
                                    <input name="username" type="text" placeholder="Username">
                                </div>
                                <div class="form-group input-text <?php echo form_error('username') ? 'danger' : 'primary'; ?>">
                                    <span class="input-icon glyphicon glyphicon-lock"></span>
                                    <input name="password" type="password" placeholder="Kata Sandi">
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <!--
                                        <label>
                                            <input type="checkbox"> Remember me
                                        </label>
                                        -->
                                        <div class="pull-right">
                                            <button id="login-btn" name="login" value="login" type="submit" class="btn btn-primary">Masuk</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--
                        <div class="box-footer">
                            <p>Lupa password anda? Klik disini</p>
                        </div>
                        -->
                    </div>
                </div>
            </div>
		</div>

	</body>
</html>
