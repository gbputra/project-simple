<?php
	$_CI = & get_instance();
	$articles = $category->articles();
	$this->render('header');
?>
<main class="masterhead" id="header-master">
    <div class="container" align="center">
      
	   <h1></h1>
        <p class="lead"></p>
		
        <img width="300px" height="300px" src="<?php echo $this->theme_url; ?>img/logo.png">
		
    </div>
</main>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12" align="center">
                <h1><?php echo $articles[0]->title(); ?></h1>
                <p><?php echo $articles[0]->content(); ?></p>
            </div>
        </div>
		<!--
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-4" align="center">
                <img src="<?php echo $this->theme_url; ?>img/icon1.png">
                <h3><b>Mudah</b></h3>
                <p>Pengguna dapat dengan mudah mengakses materi-materi dan informasi terbaru terkait dengan karya ilmiah karena portal telah kami rancang sederhana, lengkap, dan terkategori.</p>
            </div>
            <div class="col-md-4" align="center">
                <img src="<?php echo $this->theme_url; ?>img/icon2.png">
                <h3><b>24/7</b></h3>
                <p>Materi dan informasi dapat diakses oleh pengguna di manapun dan kapanpun selama 24 jam 7 hari seminggu.</p>
            </div>
            <div class="col-md-4" align="center">
                <img src="<?php echo $this->theme_url; ?>img/icon3.png">
                <h3><b>Gratis</b></h3>
                <p>Pengguna dapat mengakses PEN Open Lab secara gratis.</p>
            </div>
        </div>
		!-->
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-12" align="center">
                <h1><?php echo $articles[1]->title(); ?></h1>
                <p><?php echo $articles[1]->content(); ?></p>
            </div>
        </div>
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-12" align="center">
               <!-- <a href="<?php echo conf_item('base_url'); ?>auth/register"><button type="button" class="btn btn-primary btn-lg">Daftar sekarang</button></a>
!-->               
			   <a href="<?php echo conf_item('base_url'); ?>auth/login"><button type="button" class="btn btn-default btn-lg">Masuk</button></a>
            </div>
        </div>
	</div>
</div>

<!--
<div class="content" style="background-color:#f0f0f0;">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="img-responsive" src="<?php echo $this->theme_url; ?>img/gallery/pic1.jpg">
            </div>
            <div class="col-md-4">
                <img class="img-responsive" src="<?php echo $this->theme_url; ?>img/gallery/pic2.jpg">
            </div>
            <div class="col-md-4">
                <img class="img-responsive" src="<?php echo $this->theme_url; ?>img/gallery/pic3.jpg">
            </div>
        </div>
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-4">
                <img class="img-responsive" src="<?php echo $this->theme_url; ?>img/gallery/pic4.jpg">
            </div>
            <div class="col-md-4">
                <img class="img-responsive" src="<?php echo $this->theme_url; ?>img/gallery/pic5.jpg">
            </div>
            <div class="col-md-4">
                <img class="img-responsive" src="<?php echo $this->theme_url; ?>img/gallery/pic6.jpg">
            </div>
        </div>
    </div>
</div>
!-->

<?php $this->render('footer'); ?>
