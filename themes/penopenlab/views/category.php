<?php
	$_CI = & get_instance();
	$articles = $category->articles();
?>
<?php $this->render('header'); ?>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12" align="center">
                <h1><?php echo $category->title(); ?></h1>
            </div>
        </div>
        <?php if (empty($articles)): ?>
            <h1>Mohon maaf, belum ada artikel tersedia</h1>
        <?php else: ?>
        <?php foreach ($articles as $article): ?>
        <div class="row">
            <div class="col-md-12">
                <legend><?php echo $article->title(); ?></legend>
                <address>
                    Penulis : <?php echo $article->author()->nickname(); ?><br/>
                    Diperbaharui : <?php echo date('d F Y', $article->date_updated()); ?><br/>
                    Kategori : <a href="#<?php echo $article->category()->name(); ?>"><span class="label label-primary"><?php echo $article->category()->title(); ?></span></a>
                </address>
                <p><?php echo $article->content(); ?></p>
            </div>
        </div>
        <?php endforeach; ?>
        <ul class="pager">
            <?php if($page > 0): ?><li class="previous"><a href="<?php out($category->url.'/'.$num.'/'.($page-1)); ?>">&larr; Sebelumnya</a></li><?php endif; ?>
            <?php if($page < $page_num-1): ?><li class="next"><a href="<?php out($category->url.'/'.$num.'/'.($page+1)); ?>">Berikutnya &rarr;</a></li><?php endif; ?>
        </ul>
        <?php endif; ?>
	</div>
</div>       

<?php $this->render('footer'); ?>
