<?php
	$_CI =& get_instance();
	$_CI->load->library("message");
	$messages = $_CI->message->get_messages();
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $this->site_name.' | '.$this->title; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->theme_url; ?>css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $this->theme_url; ?>css/ganeshatiga.css" />
		<link rel="icon" type="image/png" href="<?php echo $this->theme_url; ?>img/icon.png">
        <script type="text/javascript" src="<?php echo $this->theme_url; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo $this->theme_url; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $this->theme_url; ?>js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script>
			function generate(text,type) {
				var n = noty({
					text        : text,
					type        : type,
					dismissQueue: true,
					layout      : 'bottomRight',
					theme       : 'defaultTheme',
					maxVisible  : 10
				});
			}
			$(document).ready(function(e) {
				<?php if (!empty($messages['success'])){ ?>
					<?php foreach ($messages['success'] as $message):  ?>
						generate('<?php echo $message; ?>','success');
					<?php endforeach; ?>
				<?php } ?>
				<?php if (!empty($messages['warning'])){ ?>
					<?php foreach ($messages['warning'] as $message):  ?>
						generate('<?php echo $message; ?>','warning');
					<?php endforeach; ?>
				<?php } ?>
				<?php if (!empty($messages['info'])){ ?>
					<?php foreach ($messages['info'] as $message):  ?>
						generate('<?php echo $message; ?>','information');
					<?php endforeach; ?>
				<?php } ?>
				<?php if (!empty($messages['error'])){ ?>
					<?php foreach ($messages['error'] as $message):  ?>
						generate('<?php echo $message; ?>','error');
					<?php endforeach; ?>
				<?php } ?>
            });
		</script>
        <style>
			* {
				font-family: 'Open Sans', sans-serif;
			}
			label, .label {
				font-weight: 300;
			}
			.container {
				width: 80%;
			}
			.masterhead {
				padding-top: 150px;
				padding-bottom: 100px;
				padding-left: 100px;
				padding-right: 100px;
			}
			#header-master {
				background-image: url('<?php echo $this->theme_url; ?>img/sman3semarang.jpg');
				background-size: cover;
				background-position: center;
				background-attachment: fixed;
				border-bottom: solid 5px #1882CE;
				color: white;
				box-shadow: 0px -3px 2px 0px rgba(0, 0, 0, 0.15);
			}
			#header-master h1 {
				font-size: 52px;
			}
			#header-master img {
				margin-top: 50px;
			}
			.footer {
				padding: 50px 150px;
				text-align: center;
				background-image: url('<?php echo $this->theme_url; ?>img/footer.jpg');
				font-size: 18px;
				color: white;
				box-shadow: 0px -3px 2px 0px rgba(0, 0, 0, 0.15);
			}
			.content {
				position: relative;
				padding-top: 80px;
				padding-bottom: 80px;
				font-size: 18px;
				margin: 0 0;
			}
			.content h1{
				font-size: 42px;
			}
		</style>
	</head>
	<body>
    
    	<nav class="navbar navbar-fixed-top navbar-ganeshatiga" role="navigation">
			<div class="container">
		    	<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
				<span class="sr-only">Toggle responsive</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
		      		<a class="navbar-brand" href="<?php echo conf_item('base_url'); ?>"><?php echo $this->site_name; ?></a>
		    	</div>
                 <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav">
                	<?php $main_menu = $this->get_menu('main-menu'); ?>
                    <?php foreach ($main_menu->items as $item): ?>
                    	<li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a></li>
                    <?php endforeach; ?>
                </ul>
               
                <ul class="nav navbar-nav navbar-right">
                	<?php if ($this->auth()->is_logged_in()): ?>
                    <form action="<?php echo base_url('auth/logout'); ?>" method="post" id="logout_frm">
                    	<input type="hidden" name="logout" value="logout">
                        <input type="hidden" name="<?php echo csrf_token_name(); ?>" value="<?php echo csrf_hash() ?>" />
                    </form>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><?php echo user('fullname'); ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('member'); ?>">Your Profile</a></li>
                            <li class="divider"></li>
                            <li><a onClick="$('#logout_frm').submit();" style="cursor: pointer;">Sign Out</a></li>
                        </ul>
                    </li>
                    <?php else: ?>
                   <!-- <li><a href="<?php echo base_url('auth/register'); ?>">Sign Up</a></li>!-->
                    <li><a href="<?php echo base_url('auth/login'); ?>"><span class="glyphicon glyphicon-log-in"></span>   Masuk</a></li>
                    <?php endif; ?>
                </ul>
				</div>
		  	</div>
		</nav>