<?php
		$_CI = & get_instance();
		$_CI->load->library("form_validation");
		$_CI->load->library("message");
?>
<?php $this->render('header'); ?>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->theme_url; ?>css/datepicker.css" />
		<script type="text/javascript" src="<?php echo $this->theme_url; ?>js/bootstrap-datepicker.js"></script>
		<script>
			$(document).ready(function(e) {
				var d = $('#birthdate_frm').val();
				if (d == '')
					$('#birthdate_frm').val('1999-01-01');
               	$('#birthdate_frm').datepicker({
					format: 'yyyy-mm-dd'
				}); 
            });			
		</script>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->theme_url; ?>css/bootstrapValidator.min.css" />
        <script type="text/javascript" src="<?php echo $this->theme_url; ?>js/bootstrapValidator.min.js"></script>
        <script>
			$(document).ready(function(e) {
                $("#register_form").bootstrapValidator();
            });			
		</script>
		<div class="content" style="font-family: calibri; font-size: 16px;">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-12" align="center">
                    	<h1>Make New Account</h1>
                    </div>
                </div>
            	<div class="row" style="margin-top: 30px;">
                	<div class="col-md-10 col-md-offset-1">
                   		<form id="register_form" action="<?php echo conf_item('base_url');  ?>auth/register" class="form-horizontal" role="form" method="post" data-bv-message="Data tidak valid" data-bv-feedbackicons-valid="glyphicon glyphicon-ok" data-bv-feedbackicons-invalid="glyphicon glyphicon-remove" data-bv-feedbackicons-validating="glyphicon glyphicon-refresh" data-bv-trigger="mouseup change">
                        	<input type="hidden" name="<?php echo csrf_token_name(); ?>" value="<?php echo csrf_hash() ?>" />
                        	<legend>Data Pribadi</legend>
                        	<div class="form-group <?php echo form_error('fullname','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Nama Lengkap</label>
                            	<div class="col-sm-9">
                              		<input name="fullname" type="text" class="form-control" value="<?php echo set_value('fullname'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Nama lengkap harus diisi"
                                        data-bv-stringlength="true" data-bv-stringlength-min="3" data-bv-stringlength-max="255" data-bv-stringlength-message="Panjang karakter harus lebih dari 2 dan kurang dari 256"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Nama lengkap hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('fullname','<small class="control-label">','</small>'); ?>
                                </div>
                          	</div>
                            <div class="form-group <?php echo form_error('nickname','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Nama Panggilan</label>
                            	<div class="col-sm-9">
                              		<input name="nickname" type="text" class="form-control" value="<?php echo set_value('nickname'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Nama panggilan harus diisi"
                                        data-bv-stringlength="true" data-bv-stringlength-min="1" data-bv-stringlength-max="64" data-bv-stringlength-message="Panjang karakter harus kurang dari 64"
                                         data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Nama lengkap hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('nickname','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							
							<div class="form-group <?php echo form_error('classorign','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Asal Kelas</label>
                            	<div class="col-sm-9">
                                	<select name="classorign" class="form-control"
                                    	data-bv-notempty="true" data-bv-notempty-message="Asal Kelas harus diisi"
                                        data-bv-stringlength="true" data-bv-stringlength-min="1" data-bv-stringlength-max="32" data-bv-stringlength-message="Panjang karakter harus kurang dari 33"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Provinsi hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                        <option value="Nanggroe Aceh Darussalam" <?php echo set_select('classorign', '12 AKSELERASI', TRUE); ?>>12 AKSELERASI</option>
                                        <option value="Sumatera Utara" <?php echo set_select('classorign', '12 IPA 01'); ?>>12 IPA 01</option>
                                        <option value="Sumatera Barat" <?php echo set_select('classorign', '12 IPA 02'); ?>>12 IPA 02</option>
                                        <option value="Riau" <?php echo set_select('classorign', '12 IPA 03'); ?>>12 IPA 03</option>
                                        <option value="Jambi" <?php echo set_select('classorign', '12 IPA 04'); ?>>12 IPA 04</option>
                                        <option value="Sumatera Selatan" <?php echo set_select('classorign', '12 IPA 05'); ?>>12 IPA 05</option>	
                                        <option value="Bengkulu" <?php echo set_select('classorign', '12 IPA 06'); ?>>12 IPA 06</option>
                                        <option value="Lampung" <?php echo set_select('classorign', '12 IPA 07'); ?>>12 IPA 07</option>
                                        <option value="Babel" <?php echo set_select('classorign', '12 IPA 08'); ?>>12 IPA 08</option>
                                        <option value="Kepulauan Riau" <?php echo set_select('classorign', '12 IPA 09'); ?>>12 IPA 09</option>	
                                        <option value="DKI Jakarta" <?php echo set_select('classorign', '12 IPA 10'); ?>>12 IPA 10</option>
                                        <option value="Jawa Barat" <?php echo set_select('classorign', '12 IPA 11'); ?>>12 IPA 11</option>
                                        <option value="Jawa Tengah" <?php echo set_select('classorign', '12 IPA 12'); ?>>12 IPA 12</option>
                                        <option value="DI Yogyakarta" <?php echo set_select('classorign', '12 IPS 01'); ?>>12 IPS 01</option>
                                        <option value="Jawa Timur" <?php echo set_select('classorign', '12 IPS 02'); ?>>12 IPS 02</option>

                                    </select>
                                    <?php echo form_error('classorign','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							
                            <div class="form-group <?php echo form_error('gender','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Jenis Kelamin</label>
                            	<div class="col-sm-9">
                                	<div class="radio">
                                        <label><input type="radio" name="gender" value="1" <?php echo set_radio('gender','1'); ?>
                                        	data-bv-notempty="true" data-bv-notempty-message="Jenis kelamin harus diisi">Laki laki</label>
                                     </div>
                                     <div class="radio">
                                        <label><input type="radio" name="gender" value="0" <?php echo set_radio('gender','0'); ?>
                                        	data-bv-notempty="true" data-bv-notempty-message="Jenis kelamin harus diisi">Perempuan</label>
                                    </div>
                                    <?php echo form_error('gender','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							 <div class="form-group <?php echo form_error('birthplace','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Tempat Lahir</label>
                            	<div class="col-sm-9">
                              		<input name="birthplace" type="text" class="form-control" value="<?php echo set_value('birthplace'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Nama panggilan harus diisi"
                                        data-bv-stringlength="true" data-bv-stringlength-min="1" data-bv-stringlength-max="64" data-bv-stringlength-message="Panjang karakter harus kurang dari 64"
                                         data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Tempat lahir hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('birthplace','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
                            <div class="form-group <?php echo form_error('birthdate','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Tanggal Lahir</label>
                            	<div class="col-sm-9">
                              		<input id="birthdate_frm" name="birthdate" type="text" class="form-control" style="width:150px;" placeholder="YYYY-MM-DD" value="<?php echo set_value('birthdate'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Tanggal lahir harus diisi"
                                        data-bv-date="true" data-bv-date-format="YYYY-MM-DD" data-bv-date-message="Tanggal lahir harus berformat YYYY-MM-DD">
                                    <?php echo form_error('birthdate','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
                           
                            <legend>Informasi Kontak</legend>
                            <div class="form-group <?php echo form_error('email','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Email</label>
                            	<div class="col-sm-9">
                              		<input name="email" type="text" class="form-control" value="<?php echo set_value('email'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Email harus diisi"
                                        data-bv-stringlength="true" data-bv-stringlength-min="1" data-bv-stringlength-max="255" data-bv-stringlength-message="Panjang karakter harus kurang dari 256"
                                        data-bv-emailaddress="true" data-bv-emailaddress-message="Email harus valid">
                                    <?php echo form_error('email','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
                            <div class="form-group <?php echo form_error('mobile_number','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Nomor Handphone Utama</label>
                            	<div class="col-sm-9">
                              		<input name="phone_number" type="text" class="form-control" value="<?php echo set_value('phone_number'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Nomor telepon harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^\+?([0-9])+$" data-bv-regexp-message="Nomor telepon harus berisi nomor telepon yang valid">
                                    <?php echo form_error('phone_number','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							 <div class="form-group <?php echo form_error('mobile_number_add','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Nomor Handphone Tambahan</label>
                            	<div class="col-sm-9">
                              		<input name="mobile_number_add" type="text" class="form-control" value="<?php echo set_value('mobile_number_add'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Nomor telepon harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^\+?([0-9])+$" data-bv-regexp-message="Nomor telepon harus berisi nomor telepon yang valid">
                                    <?php echo form_error('mobile_number_add','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
                            <legend>Informasi Tempat Tinggal Saat Ini</legend>
                            <div class="form-group <?php echo form_error('home_address','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Alamat Rumah Saat Ini</label>
                            	<div class="col-sm-9">
                              		<textarea name="home_address" class="form-control"
                                    	data-bv-notempty="true" data-bv-notempty-message="Alamat Rumah harus diisi"
                                        data-bv-stringlength="true" data-bv-stringlength-min="3" data-bv-stringlength-max="255" data-bv-stringlength-message="Panjang karakter harus lebih dari 2 dan kurang dari 256"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ .,\s])+$" data-bv-regexp-message="Alamat rumah hanya boleh berisi karakter alfabet, angka, underscore, spasi, titik, koma, whitespace, dan dash"><?php echo set_value('home_address'); ?></textarea>
                                    <?php echo form_error('home_address','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<div class="form-group <?php echo form_error('home_kelurahan','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Kelurahan</label>
                            	<div class="col-sm-9">
                              		<input name="home_kelurahan" type="text" class="form-control" value="<?php echo set_value('home_kelurahan'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kota harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Kota hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('home_kelurahan','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<div class="form-group <?php echo form_error('home_kecamatan','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Kelurahan</label>
                            	<div class="col-sm-9">
                              		<input name="home_kecamatan" type="text" class="form-control" value="<?php echo set_value('home_kecamatan'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kelurahan harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Kelurahan hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('home_kecamatan','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
                            							 <div class="form-group <?php echo form_error('home_city','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Kota Saat Ini</label>
                            	<div class="col-sm-9">
                              		<input name="home_city" type="text" class="form-control" value="<?php echo set_value('home_city'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kota harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Kota hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('home_city','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<div class="form-group <?php echo form_error('home_postal','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Kodepos</label>
                            	<div class="col-sm-9">
                              		<input name="home_postal" type="text" class="form-control" value="<?php echo set_value('home_postal'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kota harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Kota hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('home_postal','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
                            <div class="form-group <?php echo form_error('home_province','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Provinsi</label>
                            	<div class="col-sm-9">
                                	<select name="home_province" class="form-control"
                                    	data-bv-notempty="true" data-bv-notempty-message="Provinsi harus diisi"
                                        data-bv-stringlength="true" data-bv-stringlength-min="1" data-bv-stringlength-max="32" data-bv-stringlength-message="Panjang karakter harus kurang dari 33"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Provinsi hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                        <option value="Nanggroe Aceh Darussalam" <?php echo set_select('home_province', 'Nanggroe Aceh Darussalam', TRUE); ?>>Nanggroe Aceh Darussalam</option>
                                        <option value="Sumatera Utara" <?php echo set_select('home_province', 'Sumatera Utara'); ?>>Sumatera Utara</option>
                                        <option value="Sumatera Barat" <?php echo set_select('home_province', 'Sumatera Barat'); ?>>Sumatera Barat</option>
                                        <option value="Riau" <?php echo set_select('home_province', 'Riau'); ?>>Riau</option>
                                        <option value="Jambi" <?php echo set_select('home_province', 'Jambi'); ?>>Jambi</option>
                                        <option value="Sumatera Selatan" <?php echo set_select('home_province', 'Sumatera Selatan'); ?>>Sumatera Selatan</option>	
                                        <option value="Bengkulu" <?php echo set_select('home_province', 'Bengkulu'); ?>>Bengkulu</option>
                                        <option value="Lampung" <?php echo set_select('home_province', 'Lampung'); ?>>Lampung</option>
                                        <option value="Babel" <?php echo set_select('home_province', 'Babel'); ?>>Babel</option>
                                        <option value="Kepulauan Riau" <?php echo set_select('home_province', 'Kepulauan Riau'); ?>>Kepulauan Riau</option>	
                                        <option value="DKI Jakarta" <?php echo set_select('home_province', 'DKI Jakarta'); ?>>DKI Jakarta</option>
                                        <option value="Jawa Barat" <?php echo set_select('home_province', 'Jawa Barat'); ?>>Jawa Barat</option>
                                        <option value="Jawa Tengah" <?php echo set_select('home_province', 'Jawa Tengah'); ?>>Jawa Tengah</option>
                                        <option value="DI Yogyakarta" <?php echo set_select('home_province', 'DI Yogyakarta'); ?>>DI Yogyakarta</option>
                                        <option value="Jawa Timur" <?php echo set_select('home_province', 'Jawa Timur'); ?>>Jawa Timur</option>
                                        <option value="Banten" <?php echo set_select('home_province', 'Banten'); ?>>Banten</option>
                                        <option value="Bali" <?php echo set_select('home_province', 'Bali'); ?>>Bali</option>
                                        <option value="Nusa Tenggara Barat" <?php echo set_select('home_province', 'Nusa Tenggara Barat'); ?>>Nusa Tenggara Barat</option>	
                                        <option value="Nusa Tenggara Timur" <?php echo set_select('home_province', 'Nusa Tenggara Timur'); ?>>Nusa Tenggara Timur</option>
                                        <option value="Kalimantan Barat" <?php echo set_select('home_province', 'Kalimantan Barat'); ?>>Kalimantan Barat</option>
                                        <option value="Kalimantan Tengah" <?php echo set_select('home_province', 'Kalimantan Tengah'); ?>>Kalimantan Tengah</option>
                                        <option value="Kalimantan Selatan" <?php echo set_select('home_province', 'Kalimantan Selatan'); ?>>Kalimantan Selatan</option>
                                        <option value="Kalimantan Timur" <?php echo set_select('home_province', 'Kalimantan Timur'); ?>>Kalimantan Timur</option>
                                        <option value="Kalimantan Utara" <?php echo set_select('home_province', 'Kalimantan Utara'); ?>>Kalimantan Utara</option>
                                        <option value="Sulawesi Utara" <?php echo set_select('home_province', 'Sulawesi Utara'); ?>>Sulawesi Utara</option>
                                        <option value="Sulawesi Tengah" <?php echo set_select('home_province', 'Sulawesi Tengah'); ?>>Sulawesi Tengah</option>
                                        <option value="Sulawesi Selatan" <?php echo set_select('home_province', 'Sulawesi Selatan'); ?>>Sulawesi Selatan</option>
                                        <option value="Sulawesi Tenggara" <?php echo set_select('home_province', 'Sulawesi Tenggara'); ?>>Sulawesi Tenggara</option>
                                        <option value="Gorontalo" <?php echo set_select('home_province', 'Gorontalo'); ?>>Gorontalo</option>
                                        <option value="Sulawesi Barat" <?php echo set_select('home_province', 'Sulawesi Barat'); ?>>Sulawesi Barat</option>
                                        <option value="Maluku" <?php echo set_select('home_province', 'Maluku'); ?>>Maluku</option>
                                        <option value="Maluku Utara" <?php echo set_select('home_province', 'Maluku Utara'); ?>>Maluku Utara</option>
                                        <option value="Papua Barat" <?php echo set_select('home_province', 'Papua Barat'); ?>>Papua Barat</option>
                                        <option value="Papua" <?php echo set_select('home_province', 'Papua'); ?>>Papua</option>
                                        <option value="0" <?php echo set_select('home_province', '0'); ?>>Lainnya</option>
                                    </select>
                                    <?php echo form_error('home_province','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<legend>Media Sosial</legend>
							<div class="form-group <?php echo form_error('Facebook','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Facebook</label>
                            	<div class="col-sm-9">
                              		<input name="Facebook" type="text" class="form-control" value="<?php echo set_value('facebook'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kota harus diisi"
                                      >
                                    <?php echo form_error('facebook','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<div class="form-group <?php echo form_error('LINE','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">LINE</label>
                            	<div class="col-sm-9">
                              		<input name="LINE" type="text" class="form-control" value="<?php echo set_value('LINE'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kota harus diisi"
                                       >
                                    <?php echo form_error('LINE','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<legend>Riwayat pendidikan</legend>
							<div class="form-group <?php echo form_error('campus_orign','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Asal Kampus/Institusi</label>
                            	<div class="col-sm-9">
                              		<input name="campus_orign" type="text" class="form-control" value="<?php echo set_value('campus_orign'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Asal Kampus harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Asal Kampus hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('campus_orign','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<div class="form-group <?php echo form_error('campus_city','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Kota Kampus/Institusi</label>
                            	<div class="col-sm-9">
                              		<input name="campus_city" type="text" class="form-control" value="<?php echo set_value('campus_city'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kota Kampus harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Kota asal Kampus hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('campus_city','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<div class="form-group <?php echo form_error('campus_faculty','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Jurusan/Pendidikan yang Diambil</label>
                            	<div class="col-sm-9">
                              		<input name="campus_faculty" type="text" class="form-control" value="<?php echo set_value('campus_faculty'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Jurusan harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Jurusan hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('campus_faculty','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<legend>Riwayat Pekerjaan</legend>
						<div class="form-group <?php echo form_error('company_name','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Nama Perusahaan</label>
                            	<div class="col-sm-9">
                              		<input name="company_name" type="text" class="form-control" value="<?php echo set_value('company_name'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Asal Kampus harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Asal Kampus hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('company_name','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<div class="form-group <?php echo form_error('company_position','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Posisi</label>
                            	<div class="col-sm-9">
                              		<input name="company_position" type="text" class="form-control" value="<?php echo set_value('company_position'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kota Kampus harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Kota asal Kampus hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('company_position','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<div class="form-group <?php echo form_error('company_address','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Alamat Kantor</label>
                            	<div class="col-sm-9">
                              		<input name="company_address" type="text" class="form-control" value="<?php echo set_value('company_address'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kota Kampus harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Kota asal Kampus hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('company_address','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<div class="form-group <?php echo form_error('company_city','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Kota Kantor</label>
                            	<div class="col-sm-9">
                              		<input name="company_city" type="text" class="form-control" value="<?php echo set_value('company_city'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kota Kampus harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Kota asal Kampus hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('company_city','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
							<div class="form-group <?php echo form_error('company_phone','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Telepon Kantor</label>
                            	<div class="col-sm-9">
                              		<input name="company_phone" type="text" class="form-control" value="<?php echo set_value('company_phone'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Kota Kampus harus diisi"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Kota asal Kampus hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                    <?php echo form_error('company_phone','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
                            <legend>Informasi Akun</legend>
                            <div class="form-group <?php echo form_error('username','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Username</label>
                            	<div class="col-sm-9">
                              		<input name="username" type="text" class="form-control" value="<?php echo set_value('username'); ?>"
                                    	data-bv-notempty="true" data-bv-notempty-message="Username harus diisi"
                                        data-bv-stringlength="true" data-bv-stringlength-min="1" data-bv-stringlength-max="33" data-bv-stringlength-message="Panjang karakter harus kurang dari 33"
                                        data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_-])+$" data-bv-regexp-message="Username hanya boleh berisi karakter alfabet, dan dash">
                                    <?php echo form_error('username','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
                            <div class="form-group <?php echo form_error('password','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Password</label>
                            	<div class="col-sm-9">
                              		<input name="password" type="password" class="form-control"
                                    	data-bv-notempty="true" data-bv-notempty-message="Password harus diisi"
                                        data-bv-stringlength="true" data-bv-stringlength-min="4" data-bv-stringlength-max="32" data-bv-stringlength-message="Panjang karakter harus lebih dari 3 dan kurang dari 33">
                                    <?php echo form_error('password','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
                            <div class="form-group <?php echo form_error('cpassword','','') ? 'has-error' : '';  ?>">
                            	<label class="col-sm-3 control-label">Konfirmasi Password</label>
                            	<div class="col-sm-9">
                              		<input name="cpassword" type="password" class="form-control"
                                    	data-bv-notempty="true" data-bv-notempty-message="Konfirmasi Password harus diisi"
                                    	data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="Konfirmasi password harus sama dengan password">
                                    <?php echo form_error('password','<small class="control-label">','</small>'); ?>
                            	</div>
                          	</div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                	<button name="register" type="submit" value="register" class="btn btn-primary">Daftar</button>   
                                </div>
                            </div>                                
                        </form>
                    </div>
                </div>
            </div>
        </div>
	<?php $this->render('footer'); ?>
