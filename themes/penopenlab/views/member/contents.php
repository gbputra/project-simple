<?php $this->render('member/header'); ?>
<div class="content">
	<div class="container">
    	<div class="row">
        	<div class="col-md-12" align="center" style="margin-bottom: 50px;">
            	<h1>Pilih konten yang ingin anda pelajari</h1>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
            	<ul>
				<?php foreach ($contents as $content): ?>
                	<li><a href="<?php out(base_url('member/learn/content/'.$content->id())); ?>"><?php echo $content->title(); ?></a></li>
                <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>