<?php 
$_CI = & get_instance();
$_CI->load->library('form_validation');
$this->render('member/header');
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->theme_url; ?>css/datepicker.css" />
<script type="text/javascript" src="<?php echo $this->theme_url; ?>js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function(e) {
        var d = $('#birthdate_frm').val();
        if (d == '')
            $('#birthdate_frm').val('1999-01-01');
        $('#birthdate_frm').datepicker({
            format: 'yyyy-mm-dd'
        }); 
    });			
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->theme_url; ?>css/bootstrapValidator.min.css" />
<script type="text/javascript" src="<?php echo $this->theme_url; ?>js/bootstrapValidator.min.js"></script>
<script>
    $(document).ready(function(e) {
        $("#update_form").bootstrapValidator();
		$("#update_pass_form").bootstrapValidator();
    });			
</script>

<div class="content">
    <div class="container">
    	<div class="row">
        	<div class="col-md-12" align="center">
            	<h1>Profil Anda</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form id="update_form" class="form-horizontal" role="form" action="<?php echo base_url('member/profile/update_profile'); ?>" method="post"
                	data-bv-message="Data tidak valid"
                    data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                    data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                    data-bv-feedbackicons-validating="glyphicon glyphicon-refresh"
                    data-bv-trigger="mouseup change">
                	<input type="hidden" name="<?php echo csrf_token_name(); ?>" value="<?php echo csrf_hash() ?>" />
                    <legend>Data Pribadi</legend>
                    <div class="form-group <?php echo form_error('fullname','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Nama Lengkap</label>
                        <div class="col-sm-9">
                            <input name="fullname" type="text" class="form-control" value="<?php echo htmlentities(user('fullname')); ?>"
                                data-bv-notempty="true" data-bv-notempty-message="Nama lengkap harus diisi"
                                data-bv-stringlength="true" data-bv-stringlength-min="3" data-bv-stringlength-max="255" data-bv-stringlength-message="Panjang karakter harus lebih dari 2 dan kurang dari 256"
                                data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Nama lengkap hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                            <?php echo form_error('fullname','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo form_error('nickname','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Nama Panggilan</label>
                        <div class="col-sm-9">
                            <input name="nickname" type="text" class="form-control" value="<?php echo htmlentities(user('nickname')); ?>"
                                data-bv-notempty="true" data-bv-notempty-message="Nama panggilan harus diisi"
                                data-bv-stringlength="true" data-bv-stringlength-min="1" data-bv-stringlength-max="64" data-bv-stringlength-message="Panjang karakter harus kurang dari 64"
                                 data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Nama lengkap hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                            <?php echo form_error('nickname','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo form_error('gender','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Jenis Kelamin</label>
                        <div class="col-sm-9">
                            <div class="radio">
                                <label><input type="radio" name="gender" value="1" <?php echo user('gender') == 1 ? "checked" : ""; ?>
                                    data-bv-notempty="true" data-bv-notempty-message="Jenis kelamin harus diisi">Laki laki</label>
                             </div>
                             <div class="radio">
                                <label><input type="radio" name="gender" value="0" <?php echo user('gender') == 0 ? "checked" : ""; ?>
                                    data-bv-notempty="true" data-bv-notempty-message="Jenis kelamin harus diisi">Perempuan</label>
                            </div>
                            <?php echo form_error('gender','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo form_error('birthdate','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Tanggal Lahir</label>
                        <div class="col-sm-9">
                            <input id="birthdate_frm" name="birthdate" type="text" class="form-control" style="width:150px;" placeholder="YYYY-MM-DD" value="<?php echo htmlentities(user('birthdate')); ?>"
                                data-bv-notempty="true" data-bv-notempty-message="Tanggal lahir harus diisi"
                                data-bv-date="true" data-bv-date-format="YYYY-MM-DD" data-bv-date-message="Tanggal lahir harus berformat YYYY-MM-DD">
                            <?php echo form_error('birthdate','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <legend>Informasi Asal Sekolah</legend>
                    <div class="form-group <?php echo form_error('school_name','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Nama Sekolah</label>
                        <div class="col-sm-9">
                            <input name="school_name" type="text" class="form-control" value="<?php echo htmlentities(user('data','school_name')); ?>"
                                data-bv-notempty="true" data-bv-notempty-message="Nama sekolah harus diisi"
                                data-bv-stringlength="true" data-bv-stringlength-min="3" data-bv-stringlength-max="255" data-bv-stringlength-message="Panjang karakter harus lebih dari 2 dan kurang dari 256"
                                data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Nama sekolah hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                            <?php echo form_error('school_name','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo form_error('school_address','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Alamat Sekolah</label>
                        <div class="col-sm-9">
                            <textarea name="school_address" class="form-control"
                                data-bv-notempty="true" data-bv-notempty-message="Alamat sekolah harus diisi"
                                data-bv-stringlength="true" data-bv-stringlength-min="3" data-bv-stringlength-max="255" data-bv-stringlength-message="Panjang karakter harus lebih dari 2 dan kurang dari 256"
                                data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ .,\s])+$" data-bv-regexp-message="Alamat sekolah hanya boleh berisi karakter alfabet, angka, underscore, spasi, titik, koma, whitespace, dan dash"><?php echo htmlentities(user('data','school_address')); ?></textarea>
                            <?php echo form_error('school_address','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo form_error('grade','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Kelas</label>
                        <div class="col-sm-9">
                            <input name="grade" type="text" class="form-control" value="<?php echo htmlentities(user('data','grade')); ?>"
                                data-bv-notempty="true" data-bv-notempty-message="Kelas harus diisi"
                                data-bv-greaterthan="true" data-bv-greaterthan-value="7" data-bv-greaterthan-inclusive="false" data-bv-greaterthan-message="Kelas minimal adalah kelas 7"
                                data-bv-lessthan="true" data-bv-lessthan-value="12" data-bv-lessthan-inclusive="false" data-bv-greaterthan-message="Kelas maksimal adalah kelas 12">
                            <?php echo form_error('grade','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <legend>Informasi Kontak</legend>
                    <div class="form-group <?php echo form_error('phone_number','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Nomor Telepon</label>
                        <div class="col-sm-9">
                            <input name="phone_number" type="text" class="form-control" value="<?php echo htmlentities(user('data','phone_number')); ?>"
                                data-bv-notempty="true" data-bv-notempty-message="Nomor telepon harus diisi"
                                data-bv-regexp="true" data-bv-regexp-regexp="^\+?([0-9])+$" data-bv-regexp-message="Nomor telepon harus berisi nomor telepon yang valid">
                            <?php echo form_error('phone_number','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <legend>Informasi Tempat Tinggal</legend>
                    <div class="form-group <?php echo form_error('home_address','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Alamat Rumah</label>
                        <div class="col-sm-9">
                            <textarea name="home_address" class="form-control"
                                data-bv-notempty="true" data-bv-notempty-message="Alamat Rumah harus diisi"
                                data-bv-stringlength="true" data-bv-stringlength-min="3" data-bv-stringlength-max="255" data-bv-stringlength-message="Panjang karakter harus lebih dari 2 dan kurang dari 256"
                                data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ .,\s])+$" data-bv-regexp-message="Alamat rumah hanya boleh berisi karakter alfabet, angka, underscore, spasi, titik, koma, whitespace, dan dash"><?php echo htmlentities(user('data','home_address')); ?></textarea>
                            <?php echo form_error('home_address','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo form_error('home_city','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Kota</label>
                        <div class="col-sm-9">
                            <input name="home_city" type="text" class="form-control" value="<?php echo htmlentities(user('data','home_city')); ?>"
                                data-bv-notempty="true" data-bv-notempty-message="Kota harus diisi"
                                data-bv-stringlength="true" data-bv-stringlength-min="1" data-bv-stringlength-max="32" data-bv-stringlength-message="Panjang karakter harus kurang dari 33"
                                data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Kota hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                            <?php echo form_error('home_city','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo form_error('home_province','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Provinsi</label>
                        <div class="col-sm-9">
                            <select name="home_province" class="form-control"
                                data-bv-notempty="true" data-bv-notempty-message="Provinsi harus diisi"
                                data-bv-stringlength="true" data-bv-stringlength-min="1" data-bv-stringlength-max="32" data-bv-stringlength-message="Panjang karakter harus kurang dari 33"
                                data-bv-regexp="true" data-bv-regexp-regexp="^([-a-zA-Z0-9_ ])+$" data-bv-regexp-message="Provinsi hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash">
                                <option value="Nanggroe Aceh Darussalam" <?php echo user('data','home_province') == 'Nanggroe Aceh Darussalam' ? "selected" : ""; ?>>Nanggroe Aceh Darussalam</option>
                                <option value="Sumatera Utara" <?php echo user('data','home_province') == 'Sumatera Utara' ? "selected" : ""; ?>>Sumatera Utara</option>
                                <option value="Sumatera Barat" <?php echo user('data','home_province') == 'Sumatera Barat' ? "selected" : ""; ?>>>Sumatera Barat</option>
                                <option value="Riau" <?php echo user('data','home_province') == 'Riau' ? "selected" : ""; ?>>Riau</option>
                                <option value="Jambi" <?php echo user('data','home_province') == 'Jambi' ? "selected" : ""; ?>>Jambi</option>
                                <option value="Sumatera Selatan" <?php echo user('data','home_province') == 'Sumatera Selatan' ? "selected" : ""; ?>>Sumatera Selatan</option>	
                                <option value="Bengkulu" <?php echo user('data','home_province') == 'Bengkulu' ? "selected" : ""; ?>>Bengkulu</option>
                                <option value="Lampung" <?php echo user('data','home_province') == 'Lampung' ? "selected" : ""; ?>>Lampung</option>
                                <option value="Babel" <?php echo user('data','home_province') == 'Babel' ? "selected" : ""; ?>>Babel</option>
                                <option value="Kepulauan Riau" <?php echo user('data','home_province') == 'Kepulauan Riau' ? "selected" : ""; ?>>Kepulauan Riau</option>	
                                <option value="DKI Jakarta" <?php echo user('data','home_province') == 'DKI Jakarta' ? "selected" : ""; ?>>DKI Jakarta</option>
                                <option value="Jawa Barat" <?php echo user('data','home_province') == 'Jawa Barat' ? "selected" : ""; ?>>Jawa Barat</option>
                                <option value="Jawa Tengah" <?php echo user('data','home_province') == 'Jawa Tengah' ? "selected" : ""; ?>>Jawa Tengah</option>
                                <option value="DI Yogyakarta" <?php echo user('data','home_province') == 'DI Yogyakarta' ? "selected" : ""; ?>>DI Yogyakarta</option>
                                <option value="Jawa Timur" <?php echo user('data','home_province') == 'Jawa Timur' ? "selected" : ""; ?>>Jawa Timur</option>
                                <option value="Banten" <?php echo user('data','home_province') == 'Banten' ? "selected" : ""; ?>>Banten</option>
                                <option value="Bali" <?php echo user('data','home_province') == 'Bali' ? "selected" : ""; ?>>Bali</option>
                                <option value="Nusa Tenggara Barat" <?php echo user('data','home_province') == 'Nusa Tenggara Barat' ? "selected" : ""; ?>>Nusa Tenggara Barat</option>	
                                <option value="Nusa Tenggara Timur" <?php echo user('data','home_province') == 'Nusa Tenggara Timur' ? "selected" : ""; ?>>Nusa Tenggara Timur</option>
                                <option value="Kalimantan Barat" <?php echo user('data','home_province') == 'Kalimantan Barat' ? "selected" : ""; ?>>Kalimantan Barat</option>
                                <option value="Kalimantan Tengah" <?php echo user('data','home_province') == 'Kalimantan Tengah' ? "selected" : ""; ?>>Kalimantan Tengah</option>
                                <option value="Kalimantan Selatan" <?php echo user('data','home_province') == 'Kalimantan Selatan' ? "selected" : ""; ?>>Kalimantan Selatan</option>
                                <option value="Kalimantan Timur" <?php echo user('data','home_province') == 'Kalimantan Timur' ? "selected" : ""; ?>>Kalimantan Timur</option>
                                <option value="Kalimantan Utara" <?php echo user('data','home_province') == 'Kalimantan Utara' ? "selected" : ""; ?>>Kalimantan Utara</option>
                                <option value="Sulawesi Utara" <?php echo user('data','home_province') == 'Sulawesi Utara' ? "selected" : ""; ?>>Sulawesi Utara</option>
                                <option value="Sulawesi Tengah" <?php echo user('data','home_province') == 'Sulawesi Tengah' ? "selected" : ""; ?>>Sulawesi Tengah</option>
                                <option value="Sulawesi Selatan" <?php echo user('data','home_province') == 'Sulawesi Selatan' ? "selected" : ""; ?>>Sulawesi Selatan</option>
                                <option value="Sulawesi Tenggara" <?php echo user('data','home_province') == 'Sulawesi Tenggara' ? "selected" : ""; ?>>Sulawesi Tenggara</option>
                                <option value="Gorontalo"<?php echo user('data','home_province') == 'Gorontalo' ? "selected" : ""; ?>>Gorontalo</option>
                                <option value="Sulawesi Barat" <?php echo user('data','home_province') == 'Sulawesi Barat' ? "selected" : ""; ?>>Sulawesi Barat</option>
                                <option value="Maluku" <?php echo user('data','home_province') == 'Maluku' ? "selected" : ""; ?>>Maluku</option>
                                <option value="Maluku Utara" <?php echo user('data','home_province') == 'Maluku Utara' ? "selected" : ""; ?>>Maluku Utara</option>
                                <option value="Papua Barat" <?php echo user('data','home_province') == 'Papua Barat' ? "selected" : ""; ?>>Papua Barat</option>
                                <option value="Papua" <?php echo user('data','home_province') == 'Papua' ? "selected" : ""; ?>>Papua</option>
                                <option value="0" <?php echo user('data','home_province') == '0' ? "selected" : ""; ?>>Lainnya</option>
                            </select>
                            <?php echo form_error('home_province','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button name="update_profile" type="submit" value="update_profile" class="btn btn-primary">Update</button>   
                        </div>
                    </div>
                </form>
                <form id="update_pass_form" class="form-horizontal" role="form" action="<?php echo base_url('member/profile/update_password'); ?>" method="post"
                	data-bv-message="Data tidak valid"
                    data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                    data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                    data-bv-feedbackicons-validating="glyphicon glyphicon-refresh"
                    data-bv-trigger="mouseup change">
                    <input type="hidden" name="<?php echo csrf_token_name(); ?>" value="<?php echo csrf_hash() ?>" />
                    <legend>Informasi Akun</legend>
                    <div class="form-group <?php echo form_error('oldpassword','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Password lama</label>
                        <div class="col-sm-9">
                            <input name="oldpassword" type="password" class="form-control"
                                data-bv-notempty="true" data-bv-notempty-message="Password harus diisi"
                                data-bv-stringlength="true" data-bv-stringlength-min="4" data-bv-stringlength-max="32" data-bv-stringlength-message="Panjang karakter harus lebih dari 3 dan kurang dari 33">
                            <?php echo form_error('oldpassword','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo form_error('password','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Password baru</label>
                        <div class="col-sm-9">
                            <input name="password" type="password" class="form-control"
                                data-bv-notempty="true" data-bv-notempty-message="Password harus diisi"
                                data-bv-stringlength="true" data-bv-stringlength-min="4" data-bv-stringlength-max="32" data-bv-stringlength-message="Panjang karakter harus lebih dari 3 dan kurang dari 33">
                            <?php echo form_error('password','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo form_error('cpassword','','') ? 'has-error' : '';  ?>">
                        <label class="col-sm-3 control-label">Konfirmasi Password</label>
                        <div class="col-sm-9">
                            <input name="cpassword" type="password" class="form-control"
                                data-bv-notempty="true" data-bv-notempty-message="Konfirmasi Password harus diisi"
                                data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="Konfirmasi password harus sama dengan password">
                            <?php echo form_error('password','<small class="control-label">','</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button name="update_password" type="submit" value="update_password" class="btn btn-primary">Update Password</button>   
                        </div>
                    </div>
                </form>
            </div>
        </div>
	</div>
</div>
