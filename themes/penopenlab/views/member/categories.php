<?php $this->render('member/header'); ?>
<?php
	function show_categories($categories)
	{
		echo "<ul>";
		foreach ($categories as $category)
		{
			echo "<li><a href=\"".base_url('member/learn/category/'.$category->id())."\">".$category->title()."</a>";
			if (count($category->child) > 0)
				show_categories($category->child());
			echo "</li>";
		}
		echo "</ul>";
	}
?>
<div class="content">
	<div class="container">
    	<div class="row">
        	<div class="col-md-12" align="center" style="margin-bottom: 50px;">
            	<h1>Silakan cari data yang Anda kehendaki</h1>
            </div>
        </div>
		<!--
        <div class="row">
        	<div class="col-md-12">
            	<ul>
				<?php foreach ($categories as $category): ?>
                	<li><a href="<?php out(base_url('member/learn/category/'.$category->id())); ?>"><?php echo $category->title(); ?></a><?php show_categories($category->child()); ?></li>
                <?php endforeach; ?>
                </ul>
            </div>
        </div>
		!-->
    </div>
</div>

<?php include('config.php');?>
<!DOCTYPE html>
<html>
<head>
<title>Menampilkan Data dengan PHP, MySQL dan DataTables</title>
<link rel="stylesheet" type="text/css" href="<?php echo $this->theme_url; ?>css/jquery.dataTables.css">
<style>
table {
margin: 0 auto;
border-collapse: collapse;
}

tbody {
color: #000;
}

table td {
padding: 5px 10px;
border: 1px solid #e0e0e0;
}

table tr {
font: normal 14px Tahoma, Geneva, sans-serif;
}

#konten {
	width:800px;
	height:auto;
	padding:10px;
	margin:0 auto;	
	border: 1px solid #e5e5e5;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
	-moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
	box-shadow: 0 1px 2px rgba(0,0,0,.05);
}
</style>
</head>
<body>
<div id="konten">
<table id="contoh">
	<thead>
		<tr>
			<th>No</th>
		    <th>Nama Karyawan</th>
		    <th>Jenis Kelamin</th>
		    <th>Jabatan</th>
		    <th>Penempatan</th>
		</tr>
	</thead>

	<tbody>
	<?php
	$no = 1;
	$query = mysql_query("SELECT * FROM pen_users ORDER BY id ASC");
	while($data = mysql_fetch_array($query)){
		echo "<tr>";
		echo "<td>$data[id]</td>";
		echo "<td>$data[username]</td>";
		echo "<td>$data[email]</td>";
		echo "<td>$data[fullname]</td>";
		echo "<td>$data[nickname]</td>";
		echo "</tr>";
		$no++;
	}
	?>
	</tbody>
</table>
</div>

<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script><!-- -->
<script src="<?php echo $this->theme_url; ?>js/jquery.dataTables.js"></script>
<script>
$(document).ready(function() {
    $('#contoh').dataTable(); // Menjalankan plugin DataTables pada id contoh. id contoh merupakan tabel yang kita gunakan untuk menampilkan data
} );
</script>
</body>
<div>
<div>
</html>