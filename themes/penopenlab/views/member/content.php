<?php $this->render('member/header'); ?>
<div class="content">
	<div class="container">
    	<div class="row">
        	<div class="col-md-12" align="center" style="margin-bottom: 50px;">
            	<legend>
            	<h1><?php out($content->title()); ?></h1>
                <div>
                	<p><?php out($content->subtitle()); ?></p>
                	<p>Author : <?php out($content->author()->fullname()); ?></p>
                </div>
                </legend>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
            	<?php echo $content->content(); ?>
            </div>
        </div>
    </div>
</div>