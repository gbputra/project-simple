<?php $this->render('admin/header'); ?>
<style>
	.list-group {
		margin: 0;
	}
</style>
	<form id="delete_frm" method="post" action="<?php out(base_url('admin/article/delete')); ?>">
    	<input type="hidden" name="delete" value="delete" />
        <input type="hidden" name="id" value="" />
    	<input type="hidden" name="<?php echo csrf_token_name(); ?>" value="<?php echo csrf_hash() ?>" />
    </form>
    <script>
		function delete_article(id){
			$("#delete_frm input[name='id']").val(id.toString());
			$("#delete_frm").submit();
		}
	</script>
    <legend>Articles</legend>
    <div align="right" style="margin:10px 0;">
        <span id="item_0" style="margin-right:5px;" data-page="<?php echo intval($page); ?>">Page <?php echo intval($page+1); ?></span>
        <div class="btn-group" style="margin-right:5px;">
            <a href="<?php echo base_url('admin/article/index').'/'.$num.'/'.(($page-1 < 0) ? 0 : ($page-1)); ?>" class="btn btn-default"><span class="clickable glyphicon glyphicon-chevron-left"></span></a>
            <a href="<?php echo base_url('admin/article/index').'/'.$num.'/'.($page+1); ?>" class="btn btn-default"><span class="clickable glyphicon glyphicon-chevron-right"></span></a>
        </div>
        <a href="<?php echo base_url('admin/article/create'); ?>"><button type="button" class="btn btn-primary">Create Article</button></a>
    </div>
    <?php if (empty($articles)): ?>
        <div class="panel-body">
            No Articles
        </div>
    <?php else: ?>
    <table class="table table-hover">
        <thead>
            <th>Title</th>
            <th>Category</th>
            <th>Author</th>                    
            <th>Created</th>
            <th>Updated</th>
            <th></th>
        </thead>
        <tbody>
        <?php foreach ($articles as $article): ?>
            <tr<?php if($article->status() < 2): ?> class="warning"<?php endif; ?>>
                <td><?php echo htmlentities($article->title()); ?></td>
                <td><?php echo htmlentities($article->category()->title()); ?></td>
                <td><?php echo htmlentities($article->author()->fullname()); ?></td>
                <td><?php echo date('d/m/Y', $article->date_created()); ?></td>
                <td><?php echo date('d/m/Y', $article->date_updated()); ?></td>
                <td><a href="#" onclick="delete_article(<?php out($article->id()); ?>);">Delete</a> | <a href="<?php out(base_url('admin/article/edit/'.$article->name())); ?>">Edit</a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
<?php $this->render('admin/footer'); ?>
