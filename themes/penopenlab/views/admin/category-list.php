<?php $this->render('admin/header'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->theme_url; ?>/css/jquery.treegrid.css" />
<style>
	.space {
		display: inline-block;
		width: 20px;
	}
</style>
<legend>Content Categories Manager</legend>
<div align="right" style="margin:10px 0;">
    <a id="create_content_anchor" href="#"><button type="button" class="btn btn-primary" disabled="disabled">Create Category</button></a>
</div>
<table class="table table-hover">
	<tr>
		<td>ID</td><td>Title</td><td>Subtitle</td><td>Window Title</td><td>Author</td><td>Status</td><td></td>
	</tr>
    <?php foreach ($categories as $category): ?>
    <tr>
    	<td><?php out($category->id()); ?></td>
        <td><?php out($category->title()); ?></td>
        <td><?php out($category->subtitle()); ?></td>
        <td><?php out($category->window_title()); ?></td>
        <td><?php out($category->author()->fullname()); ?></td>
        <td><?php out(($category->status() == 1) ? "Draft" : "Published"); ?></td>
        <td></td>
    </tr>
    <?php endforeach; ?>
</table>
<?php $this->render('admin/footer'); ?>