<?php
	$this->render('admin/header');
	$_CI =& get_instance();
	$_CI->load->model('category_model', 'Category');
	$categories = $_CI->Category->get_num();
	$_CI->load->library('form_validation');
	//echo validation_errors();
?>
<script>
	$(document).ready(function(e) {
		$("#save_btn").click(function(e) {
			$("#articlenew").submit();
		});
    });
</script>
<style>
	.section {
		border-bottom: solid 1px #CCC;
		padding: 10px 0;
	}
</style>
<div align="right">
	<button id="save_btn" type="button" class="btn btn-primary">Create</button>
</div>
<legend>Article Manager</legend>
<form id="articlenew" action="<?php out(base_url('admin/article/create/')); ?>" method="post" class="form-horizontal">
	<input type="hidden" id="action" name="create" value="create" />
    <input type="hidden" name="<?php echo csrf_token_name(); ?>" value="<?php echo csrf_hash() ?>" />
    <div class="form-group">
        <label class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input id="tmp_name" name="name" type="text" class="form-control" value="<?php out(set_value('name')); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Title</label>
        <div class="col-sm-10">
            <input id="tmp_title" name="title" type="text" class="form-control" value="<?php out(set_value('title')); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Subtitle</label>
        <div class="col-sm-10">
            <input id="tmp_subtitle" name="subtitle" type="text" class="form-control" value="<?php out(set_value('subtitle')); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">View</label>
        <div class="col-sm-10">
            <input id="tmp_view" name="view" type="text" class="form-control" value="<?php if (set_value('view')) out(set_value('view')); else echo htmlentities('article'); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10">
            <select id="tmp_status" name="status" class="form-control">
                <option <?php if (set_value('status') == 1): ?>selected<?php endif; ?> value="1">Draft</option>
                <option <?php if (set_value('status') == 2): ?>selected<?php endif; ?> value="2">Publish</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Category</label>
        <div class="col-sm-10">
            <select class="form-control" name="category">
            	<?php foreach ($categories as $category): ?>
               	<option value="<?php out($category->id()); ?>" <?php  if (set_value('category') == $category->id()):?>selected<?php endif; ?>><?php out($category->title()); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <legend>Editor</legend>
    <script src="<?php echo $this->theme_url; ?>js/ckeditor/ckeditor.js"></script>
	<textarea name="content" class="ckeditor" rows="10"><?php echo set_value("content"); ?></textarea>
</form>
<?php $this->render('admin/footer'); ?>
