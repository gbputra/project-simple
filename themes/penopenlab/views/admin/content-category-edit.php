<?php
	$this->render('admin/header');
	$_CI =& get_instance();
	$_CI->load->library('form_validation');
	$_CI->lang->load('form');
?>
<script>
	$(document).ready(function(e) {
		$("#save_btn").click(function(e) {
			$("#contentnew").submit();
		});
    });
</script>
<style>
	.section {
		border-bottom: solid 1px #CCC;
		padding: 10px 0;
	}
</style>
<div align="right">
	<button id="save_btn" type="button" class="btn btn-primary">Edit</button>
</div>
<legend>Edit Content Category</legend>
<form id="contentnew" action="<?php echo base_url('admin/content/editcategory/'.$category->id()); ?>" method="post" class="form-horizontal">
	<input type="hidden" name="edit" value="edit" />
	<input type="hidden" name="<?php echo csrf_token_name(); ?>" value="<?php echo csrf_hash() ?>" />
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo lang('name_field'); ?></label>
        <div class="col-sm-10">
            <input id="name" name="name" type="text" class="form-control" value="<?php if (set_value('name')) echo htmlentities(set_value('name')); else echo htmlentities($category->name()); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo lang('title_field'); ?></label>
        <div class="col-sm-10">
            <input id="title" name="title" type="text" class="form-control" value="<?php if (set_value('title')) echo htmlentities(set_value('title')); else echo htmlentities($category->title()); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo lang('subtitle_field'); ?></label>
        <div class="col-sm-10">
            <input id="subtitle" name="subtitle" type="text" class="form-control" value="<?php if (set_value('subtitle')) echo htmlentities(set_value('subtitle')); else echo htmlentities($category->subtitle()); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo lang('parent_field'); ?></label>
        <div class="col-sm-10">
            <input id="parent" name="parent" type="text" class="form-control" value="<?php if (set_value('parent')) echo htmlentities(set_value('parent')); else echo htmlentities($category->parent()->id()); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo lang('status_field'); ?></label>
        <div class="col-sm-10">
            <select id="status" name="status" class="form-control">
            	<?php if (set_value('status')): ?>
                <option <?php if (set_value('status') == 1): ?>selected<?php endif; ?> value="1">Draft</option>
                <option <?php if (set_value('status') == 2): ?>selected<?php endif; ?> value="2">Publish</option>
                <?php else: ?>
                <option <?php if ($category->status() == 1): ?>selected<?php endif; ?> value="1">Draft</option>
                <option <?php if ($category->status() == 2): ?>selected<?php endif; ?> value="2">Publish</option>
                <?php endif; ?>
            </select>
        </div>
    </div>
</form>
<?php $this->render('admin/footer'); ?>
