<?php
	$this->render('admin/header');
	$categories = get_categories(10);
	$articles = get_articles(10);
?>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Last 10 categories
            </div>
            <?php if (empty($categories)): ?>
                <div class="panel-body">
                    No Categories
                </div>
            <?php else: ?>
            <table class="table table-hover">
                <thead>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Created</th>
                    <th>Updated</th>
                </thead>
                <tbody>
                <?php foreach ($categories as $category): ?>
                    <tr<?php if($category->status < 2): ?> class="warning"<?php endif; ?>>
                        <td><?php out($category->title()); ?></td>
                        <td><?php out($category->author()->fullname()); ?></td>
                        <td><?php out(date('d/m/Y', $category->date_created())); ?></td>
                        <td><?php out(date('d/m/Y', $category->date_updated())); ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Last 10 Articles
            </div>
            <?php if (empty($articles)): ?>
                <div class="panel-body">
                    No Articles
                </div>
            <?php else: ?>
            <table class="table table-hover">
                <thead>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Created</th>
                    <th>Updated</th>
                </thead>
                <tbody>
                <?php foreach ($articles as $article): ?>
                    <tr<?php if($article->status() < 2): ?> class="warning"<?php endif; ?>>
                        <td><?php out($article->title()); ?></td>
                        <td><?php out($article->author()->fullname()); ?></td>
                        <td><?php out(date('d/m/Y', $article->date_created())); ?></td>
                        <td><?php out(date('d/m/Y', $article->date_updated())); ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php $this->render('admin/footer'); ?>