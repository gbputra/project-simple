<?php
	$_CI=& get_instance(); 
	$_CI->load->model("auth_model", "Auth");
	$_CI->load->library("message");
	$messages = $_CI->message->get_messages();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Administrator</title>
        <link rel="stylesheet" type="text/css" href="<?php out($this->theme_url); ?>css/bootstrap.min.css" />
        <script src="<?php out($this->theme_url); ?>js/jquery.js"></script>
    	<script src="<?php out($this->theme_url); ?>js/bootstrap.min.js"></script>
       	<script src="<?php out($this->theme_url); ?>js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script>
			function generate(text,type) {
				var n = noty({
					text        : text,
					type        : type,
					dismissQueue: true,
					layout      : 'bottomRight',
					theme       : 'defaultTheme',
					maxVisible  : 10
				});
			}
			$(document).ready(function(e) {
				<?php if (!empty($messages['success'])){ ?>
					<?php foreach ($messages['success'] as $message):  ?>
						generate('<?php out($message); ?>','success');
					<?php endforeach; ?>
				<?php } ?>
				<?php if (!empty($messages['warning'])){ ?>
					<?php foreach ($messages['warning'] as $message):  ?>
						generate('<?php out($message); ?>','warning');
					<?php endforeach; ?>
				<?php } ?>
				<?php if (!empty($messages['info'])){ ?>
					<?php foreach ($messages['info'] as $message):  ?>
						generate('<?php out($message); ?>','information');
					<?php endforeach; ?>
				<?php } ?>
				<?php if (!empty($messages['error'])){ ?>
					<?php foreach ($messages['error'] as $message):  ?>
						generate('<?php out($message); ?>','error');
					<?php endforeach; ?>
				<?php } ?>
            });
		</script>
		<style>
			.navbar {
				margin-bottom: 0;
			}
			.stretch {
				width: 90%;
				padding: 20px 0;
			}
		</style>
	</head>
    <body>
    	<nav class="navbar navbar-default navbar-static-top" role="navigation">
        	<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="#"><?php out(config_item('site_name')); ?> Administrator</a>
				</div>
                <ul class="nav navbar-nav navbar-right">
                    <li><a>{elapsed_time} Second | {memory_usage}</a></li>
                    <?php if (is_logged_in(1)): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php out(htmlentities(user("fullname"))); ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <form action="<?php out(base_url('auth/logout')); ?>" method="post" id="logout_frm">
                                <input type="hidden" name="logout" value="logout">
                                <input type="hidden" name="<?php out(csrf_token_name()); ?>" value="<?php out(csrf_hash()); ?>" />
                            </form>
                            <li><a onClick="$('#logout_frm').submit();" style="cursor: pointer;">Keluar</a></li>
                        </ul>
                    </li>
                    <?php endif; ?>
                </ul>
        	</div>
        </nav>
        <div class="container stretch">
            <div class="row">
                <div class="col-md-3">
                    <ul class="nav nav-pills nav-stacked">
                    	<li>
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	<a href="<?php out(base_url('admin')); ?>">Home</a>
                                </div>
								<div class="list-group">
                                    <a href="<?php out(base_url('admin/setting')); ?>" class="list-group-item">Setting</a>
                                </div>
                            </div>
                        </li>
                        <li>
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	Article
                                </div>
                                <div class="list-group">
                                	<a href="<?php out(base_url('admin/article/create')); ?>" class="list-group-item">New Article</a>
                                    <a href="<?php out(base_url('admin/article')); ?>" class="list-group-item">Article list</a>
                                </div>
                            </div>
                        </li>
                        <li>
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	Category
                                </div>
                                <div class="list-group">
                                    <a href="<?php out(base_url('admin/category')); ?>" class="list-group-item">Category list</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Content
                                </div>
                                <div class="list-group">
                                    <a href="<?php out(base_url('admin/content/createcategory')); ?>" class="list-group-item">New Content Categories</a>
                                    <a href="<?php out(base_url('admin/content/createcontent')); ?>" class="list-group-item">New Content</a>
                                    <a href="<?php out(base_url('admin/content')); ?>" class="list-group-item">Content Manager</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9">
