<?php
	$this->render('admin/header');
	$_CI =& get_instance();
	$_CI->load->library('form_validation');
	$_CI->lang->load('form');
?>
<script>
	$(document).ready(function(e) {
		$("#save_btn").click(function(e) {
			$("#contentnew").submit();
		});
    });
</script>
<style>
	.section {
		border-bottom: solid 1px #CCC;
		padding: 10px 0;
	}
</style>
<div align="right">
	<button id="save_btn" type="button" class="btn btn-primary">Edit</button>
</div>
<legend>Create Content</legend>
<form id="contentnew" action="<?php echo base_url('admin/content/editcontent/'.$content->id()); ?>" method="post" class="form-horizontal">
	<input type="hidden" name="edit" value="edit" />
	<input type="hidden" name="<?php echo csrf_token_name(); ?>" value="<?php echo csrf_hash() ?>" />
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo lang('name_field'); ?></label>
        <div class="col-sm-10">
            <input id="name" name="name" type="text" class="form-control" value="<?php if (set_value('name')) out(set_value('name')); else out($content->name()); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo lang('title_field'); ?></label>
        <div class="col-sm-10">
            <input id="title" name="title" type="text" class="form-control" value="<?php if (set_value('title')) out(set_value('title')); else out($content->title()); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo lang('subtitle_field'); ?></label>
        <div class="col-sm-10">
            <input id="subtitle" name="subtitle" type="text" class="form-control" value="<?php if (set_value('subtitle')) out(set_value('subtitle')); else out($content->subtitle()); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo lang('category_field'); ?></label>
        <div class="col-sm-10">
            <input id="category" name="category" type="text" class="form-control" value="<?php if (set_value('category')) out(set_value('category')); else out($content->category); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo lang('status_field'); ?></label>
        <div class="col-sm-10">
            <select id="status" name="status" class="form-control">
                <option <?php if (set_value('status') == 1): ?>selected<?php endif; ?> value="1">Draft</option>
                <option <?php if (set_value('status') == 2): ?>selected<?php endif; ?> value="2">Publish</option>
            </select>
        </div>
    </div>
    <legend>Editor</legend>
    <script src="<?php echo $this->theme_url; ?>js/ckeditor/ckeditor.js"></script>
    <textarea class="ckeditor" name="content" class="form-control" rows="10"><?php if (set_value('content')) out(set_value('content')); else out($content->content()); ?></textarea>
</form>
<?php $this->render('admin/footer'); ?>
