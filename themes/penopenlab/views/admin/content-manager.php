<?php $this->render('admin/header'); ?>
<div id="category-page">
	<style>
		#category-container {
			-webkit-user-select: none;
			-khtml-user-select: none;
			-moz-user-select: none;
			-o-user-select: none;
			user-select: none;
		}
        .space {
            display: inline-block;
            width: 20px;
        }
		.clickable:hover {
			cursor: pointer;
		}
		.clickable.glyphicon:hover {
			color: #428bca;
		}
		.clickable.label:hover {
			background-color: #428bca;
		}
    </style>
    <script>
		var current_category_id = 0;
		var current_content_page = 0;
        function remove_child(parent){
            parent = parseInt(parent);
            $("[data-parent=\""+parent.toString()+"\"]").each(function(index,element){
                remove_child($(this).attr("data-id"));
                $(this).remove();
            });
        }
        function goto_prev(id){
            var id = parseInt(id);
            var page = parseInt($("#item_"+String(id)).attr("data-page"));
            if (page > 0)
            {
                var page = parseInt($("#item_"+String(id)).attr("data-page"));
                remove_child(id);
                $("#item_"+id.toString()).attr("data-page", (page-1).toString());
                get_child_using_ajax(id, <?php echo $num; ?>, page-1);
            }
        }
        function goto_next(id){
            var id = parseInt(id);
            var page = parseInt($("#item_"+String(id)).attr("data-page"));
            remove_child(id);
            $("#item_"+id.toString()).attr("data-page", (page+1).toString());
            get_child_using_ajax(id, <?php echo $num; ?>, page+1);
        }
        function get_child_using_ajax(id,num,page){
            var id = parseInt(id);
            $.ajax({
                type: "GET",
                url: "<?php  echo base_url('admin/content/get-category-json/byParent/'); ?>/"+id.toString()+"/"+num.toString()+"/"+page.toString(),
                dataType:"json",
                beforeSend: function(){
                    $("#loader_animation_gif").show();
                },
                complete: function(){
                    $('#loader_animation_gif').hide();
                },
                success: function(data){
                    level = parseInt($("#item_"+id.toString()).attr("data-level")) + 1;					
                    var str = "";
                    var ids = new Array();
                    var c = 0;
                    for (i = 0; i < data.length; i++){
                        str += "<tr id=\"item_"+data[i].id.toString()+"\" class=\"clickable\" data-id=\""+data[i].id.toString()+"\" data-level=\""+level.toString()+"\" data-open=\"0\" data-dir=\""+(data[i].child.length > 0 ? 1 : 0)+"\" data-parent=\""+id.toString()+"\" data-page=\"0\" ondblclick=\"open_directory("+data[i].id.toString()+");\"><td>"+data[i].id+"</td>";
                        str += "<td>";
                        for (j = 0; j < level; j++){
                            if (j == level - 1)
                                if (data[i].child.length > 0)
                                    str += "<i id=\"icon_"+data[i].id.toString()+"\" class=\"glyphicon glyphicon-folder-close\" style=\"margin-right:5px;\"></i>";
                                else
                                    str += "<i id=\"icon_"+data[i].id.toString()+"\" class=\"clickable glyphicon glyphicon glyphicon-file\" style=\"margin-right:5px;\"></i>";
                            else
                                str += "<span class=\"space\"></span>";
                        }
                        str += data[i].title;
                        if (data[i].child.length > 0){
                            ids[c++] = data[i].id;						
                            str += "<span controller class=\"btn-group btn-group-xs\" style=\"margin-left: 10px; display:none;\"><a controller-prev=\""+data[i].id.toString()+"\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-chevron-left\"></span></a><a controller-next=\""+data[i].id.toString()+"\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-chevron-right\"></span></a></span>";	
                        }
                        str += " <span style=\"margin-left: 10px;\" class=\"clickable label label-default\" onclick=\"show_contents("+data[i].id.toString()+");\">"+data[i].contents.length.toString()+" content</span>";
                        str += "</td>";
                        str += "<td>"+((data[i].status == 1) ? "Draft" : "Published")+"</td>";
                        str += "<td><a href=\"<?php echo base_url('admin/content/createcategory'); ?>?parent="+data[i].id.toString()+"\"><span class=\"glyphicon glyphicon-plus\"></span></a> | <a href=\"<?php echo base_url('admin/content/editcategory'); ?>/"+data[i].id.toString()+"\"><span class=\"glyphicon glyphicon-pencil\"></span></a> | <a onclick=\"delete_category("+data[i].id.toString()+");\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr></tr>";
                    }
                    $("#icon_"+id.toString()).removeClass();
                    $("#icon_"+id.toString()).addClass("glyphicon glyphicon-folder-open");
                    $("#item_"+id.toString()).after(str);
                    for (i = 0; i < ids.length; i++){
                        $("#item_"+ids[i].toString()+" span[controller] a[controller-prev]").click(function(e) {
                            goto_prev(ids[i]);
                        });
                        $("#item_"+ids[i].toString()+" span[controller] a[controller-next]").click(function(e) {
                            goto_next(ids[i]);
                        });
                    }
                    $("#item_"+id.toString()).attr("data-open", 1);
                    $("#item_"+id.toString()+" span[controller]").show();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    generate("<?php echo lang('error_fetch_data'); ?>", "error");
                }
            });
        }
        function open_directory(id){
            id = parseInt(id);
            id = String(id);
            if ($("#item_"+id).attr("data-dir") <= 0)
                return;
            if ($("#item_"+id).attr("data-open") <= 0){
                get_child_using_ajax(id, <?php echo $num; ?>, <?php echo $page; ?>);
            }else {
                $("#icon_"+id).removeClass();
                $("#icon_"+id).addClass("glyphicon glyphicon-folder-close");
                $("#item_"+id).attr("data-open", 0);
                $("#item_"+id+" span[controller]").hide();
                remove_child(id);
            }
        }
        function init(){
            $("span[controller] a[controller-prev]").click(function(e) {
                var id = parseInt($(this).attr("controller-prev"));
                goto_prev(id);
            });
            $("span[controller] a[controller-next]").click(function(e) {
                var id = parseInt($(this).attr("controller-next"));
                goto_next(id);
            });
        }
		function user_fullname_id(id){
			$.ajax({
                type: "GET",
                url: "<?php  echo base_url('admin/ajax/getUserById/'); ?>/"+id.toString(),
                dataType:"json",
                beforeSend: function(){
                    $("#loader_animation_gif").show();
                },
                complete: function(){
                    $('#loader_animation_gif').hide();
                },
                success: function(data){
					$("*[user="+id.toString()+"]").html(data.fullname.toString());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    generate("<?php echo lang('error_fetch_data'); ?>", "error");
                }
            });
			return '';
		}
        $(document).ready(init);
		function set_contents(id,page){
			page = parseInt(page);
			if (page < 0)
				page = 0;
			id = parseInt(id);
			$.ajax({
				type: "GET",
				url: "<?php echo base_url('admin/content/get-content-json/byCategory/'); ?>/"+id.toString()+"/<?php echo $num; ?>/"+page.toString(),
				dataType:"json",
				beforeSend: function(){
					$("#loader_animation_gif").show();
				},
				complete: function(){
					$('#loader_animation_gif').hide();
				},
				success: function(data){
					var str = "";
					if (data.length > 0){
						for (var i = 0; i < data.length; i++){
							str += "<tr>";
							str += "<td>"+data[i].id.toString()+"</td>";
							str += "<td>"+data[i].title+"</td>";
							str += "<td>"+data[i].subtitle.toString()+"</td>";
							str += "<td user=\""+data[i].author.toString()+"\">"+user_fullname_id(data[i].author)+"</td>";
							str += "<td>"+((data[i].status == 1) ? "Draft" : "Published")+"</td>";
							str += "<td><a href=\"<?php echo base_url('admin/content/editcontent'); ?>/"+data[i].id.toString()+"\"><span class=\"glyphicon glyphicon-pencil\"></span></a> | <a onclick=\"delete_content("+data[i].id.toString()+");\" class=\"clickable\"><span class=\"glyphicon glyphicon-remove\"></span></a></td>";
							str += "</tr>";
						}
					}else{
						str += "<tr><td colspan=\"6\" align=\"center\">No Content Yet, <a href=\"<?php echo base_url('admin/content/createcontent/'); ?>/?category="+id.toString()+"\">create one</a></td></tr>";
					}
					$("#content-container tbody").empty();
					$("#content-container tbody").append(str);
					$("#content_item_0").attr("data-page", page.toString());
					$("#content_item_0").html("Page "+(page+1).toString());
					current_content_page = (page < 0) ? 0 : page;
					$("#create_content_anchor").attr("href", "<?php echo base_url('admin/content/createcontent?category='); ?>"+current_category_id.toString());
				},error: function (xhr, ajaxOptions, thrownError) {
					generate("<?php echo lang('error_fetch_data'); ?>", "error");
				}
			});
		}
		function show_contents(id){
			current_category_id = parseInt(id);
			current_content_page = 0;
			set_contents(current_category_id,0);
			$("#category-page").slideUp("fast");
			$("#content-page").slideDown("fast");
		}
		function show_categories(){
			$("#category-page").slideDown("fast");
			$("#content-page").slideUp("fast");
		}
		function delete_category(id){
			$("#delete_category_frm input[name='id']").val(id.toString());
			$("#delete_category_frm").submit();
		}
		function delete_content(id){
			$("#delete_content_frm input[name='id']").val(id.toString());
			$("#delete_content_frm").submit();
		}
    </script>
    <form action="<?php out(base_url('admin/content/deletecategory/')); ?>" method="post" id="delete_category_frm">
        <input type="hidden" name="delete" value="delete" />
        <input type="hidden" name="id" value="" />
        <input type="hidden" name="<?php out(csrf_token_name()); ?>" value="<?php out(csrf_hash()); ?>" />
    </form>
    <form action="<?php out(base_url('admin/content/deletecontent/')); ?>" method="post" id="delete_content_frm">
        <input type="hidden" name="delete" value="delete" />
        <input type="hidden" name="id" value="" />
        <input type="hidden" name="<?php out(csrf_token_name()); ?>" value="<?php out(csrf_hash()); ?>" />
    </form>
    <legend>Content Categories Manager</legend>
    <img id="loader_animation_gif" style="float:left; display: none;" src="<?php echo $this->theme_url; ?>img/loader.gif" width="30px" />
    <div align="right" style="margin:10px 0;">
        <span id="item_0" style="margin-right:5px;" data-page="<?php echo intval($page); ?>">Page <?php echo intval($page+1); ?></span>
        <div class="btn-group" style="margin-right:5px;">
            <a href="<?php echo base_url('admin/content/categories').'/'.$num.'/'.(($page-1 < 0) ? 0 : ($page-1)); ?>" class="btn btn-default"><span class="clickable glyphicon glyphicon-chevron-left"></span></a>
            <a href="<?php echo base_url('admin/content/categories').'/'.$num.'/'.($page+1); ?>" class="btn btn-default"><span class="clickable glyphicon glyphicon-chevron-right"></span></a>
        </div>
        <a href="<?php echo base_url('admin/content/createcategory/?parent=0'); ?>"><button type="button" class="btn btn-primary">Create Content Category</button></a>
        <a href="<?php echo base_url('admin/content/createcontent/?category=0'); ?>"><button type="button" class="btn btn-primary">Create Content</button></a>
    </div>
    <table id="category-container" class="table table-hover">
        <colgroup>
           <col span="1" width="10px;">
           <col span="1" width="auto;">
           <col span="1" width="10px;">
           <col span="1" width="100px;">
        </colgroup>
        <tr>
            <td>ID</td><td>Title</td><td>Status</td><td></td>
        </tr>
        <?php
            if (is_array($categories))
            {
				if (empty($categories)):?>
		<tr><td colspan="4" align="center">No Content Categories Yet, <a href="<?php echo base_url('admin/content/createcategory/?parent=0'); ?>">Create one</a></td></tr>
				<?php endif;
                foreach ($categories as $a)
                {
                    $child = $a->child();
                    $l = "";
                    echo "<tr id=\"item_".$a->id()."\" class=\"clickable\" data-id=\"".$a->id()."\" data-level=\"1\" data-open=\"0\" data-dir=\"".intval(count($child) > 0)."\" data-parent=\"0\" data-page=\"0\" ondblclick=\"open_directory(".$a->id().");\"><td>".$a->id()."</td>";
                    echo "<td>";
                    if(!empty($child))
                        echo "<i class=\"glyphicon glyphicon-folder-close\" style=\"margin-right:5px;\"></i>";
                    else
                        echo "<i id=\"icon_".$a->id()."\" class=\"clickable glyphicon glyphicon-file\" style=\"margin-right:5px;\"></i>";
                    echo $a->title();
                    if (!empty($child))
                        echo "<span controller class=\"btn-group btn-group-xs\" style=\"margin-left: 10px; display:none;\"><a controller-prev=\"".$a->id()."\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-chevron-left\"></span></a><a controller-next=\"".$a->id()."\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-chevron-right\"></span></a></span>";
                    echo " <span style=\"margin-left: 10px;\" class=\"clickable label label-default\" onclick=\"show_contents(".$a->id().");\">".count($a->contents)." content</span>";
                    echo "</td>";
                    echo "<td>".(($a->status() == 1) ? "Draft" : "Published")."</td>";
                    echo "<td><a href=\"".base_url('admin/content/createcategory/?parent='.$a->id())."\"><span class=\"glyphicon glyphicon-plus\"></span></a> | <a href=\"".base_url('admin/content/editcategory/'.$a->id())."\"><span class=\"glyphicon glyphicon-pencil\"></span></a> | <a onclick=\"delete_category(".$a->id().");\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr>";
                }
            }
        ?>
    </table>
</div>
<div id="content-page" style="display:none;">
	<legend>Content Manager</legend>
    <img id="loader_animation_gif" style="float:left; display: none;" src="<?php echo $this->theme_url; ?>img/loader.gif" width="30px" />
    <div align="right" style="margin:10px 0;">
        <span id="content_item_0" style="margin-right:5px;" data-page="0">Page 1</span>
        <div class="btn-group" style="margin-right:5px;">
            <a onclick="set_contents(current_category_id,current_content_page-1);" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a onclick="set_contents(current_category_id,current_content_page+1);" class="btn btn-default"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div>
        <a id="create_content_anchor" href="#"><button type="button" class="btn btn-primary">Create Content</button></a>
        <button type="button" class="btn btn-default" onclick="show_categories();"><span class="glyphicon glyphicon-arrow-up"></span> Back</button>
    </div>
    <table id="content-container" class="table table-hover">
    	<colgroup>
           <col span="1" width="10px;">
           <col span="1" width="auto;">
           <col span="1" width="200px;">
           <col span="1" width="150px;">
           <col span="1" width="100px;">
           <col span="1" width="100px;">
        </colgroup>
        <thead>
        	<tr><th>ID</th><th>Title</th><th>Subtitle</th><th>Author</th><th>Status</th><th></th></tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<?php $this->render('admin/footer'); ?>