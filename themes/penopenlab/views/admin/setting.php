<?php $this->render('admin/header'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#save_btn").click(function(){
            $("#editform").submit();
        });
    });
</script>
<style>
	.section {
		border-bottom: solid 1px #CCC;
		padding: 10px 0;
	}
</style>
<div align="right">
    <div class="btn-group">
		<button id="save_btn" type="button" class="btn btn-primary">Save</button>
    </div>
</div>
<legend>Setting Manager</legend>
<form id="editform" action="<?php echo base_url('admin/setting'); ?>" method="post" class="form-horizontal">
	<input type="hidden" name="<?php echo csrf_token_name(); ?>" value="<?php echo csrf_hash() ?>" />
	<input type="hidden" id="action" name="edit" value="edit" />
    <div class="form-group">
        <label class="col-sm-2 control-label">Site Name</label>
        <div class="col-sm-10">
            <input name="site_name" type="text" class="form-control" value="<?php echo htmlentities($settings['site_name']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Site Description</label>
        <div class="col-sm-10">
            <input name="site_description" type="text" class="form-control" value="<?php echo htmlentities($settings['site_description']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Theme</label>
        <div class="col-sm-10">
            <input name="theme[theme_name]" type="text" class="form-control" value="<?php echo htmlentities($settings['theme']['theme_name']); ?>">
        </div>
    </div>
</form>
<?php $this->render('admin/footer'); ?>
