<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Theme {
	
	public $title;
	
	private $themes_path;
	private $themes_url;
	private $theme_name;
	private $theme_path;
	private $theme_url;

	private $variable_data = array();

	private $cached_vars	= array();
	private $ob_level;

	private $_CI;
	
	public function __construct()
	{
		$this->_CI =& get_instance();
		$this->themes_path = conf_item('theme.themes_path');

		if ($this->themes_path == '')
		{
			$this->themes_path = FCPATH.'themes';
		}
		
		$this->themes_url = conf_item('theme.themes_url');
		if ($this->themes_url == '')
		{
			$this->themes_url = conf_item('base_url').'themes';
		}
		
		$this->theme_name = conf_item('theme.theme_name');
		if ($this->theme_name == '')
		{
			$this->theme_name = 'default';
		}
		
		$this->theme_path = $this->themes_path.'/'.$this->theme_name.'/';
		
		$this->theme_url = $this->themes_url.'/'.$this->theme_name.'/';
		
		$this->ob_level  = ob_get_level();
	}
	
	public function set_theme($name = "")
	{
		if ($name == "")
		{
			$name = "default";
		}
		$this->theme_name = $name;
		$this->theme_path = $this->themes_path.'/'.$this->theme_name;
		$this->theme_url = $this->themes_url.'/'.$this->theme_name;
	}
	
	public function __get($property = '')
	{
		switch ($property)
		{
			case 'site_name':
				return conf_item('site_name');
				break;
			case 'site_description':
				return conf_item('site_description');
				break;
			case 'theme_name':
				return $this->theme_name;
				break;
			case 'theme_url':
				return $this->theme_url;
				break;
		}
		return NULL;
	}

	public function set_var($name="",$value="")
	{
		if (is_array($name))
		{
			foreach ($name as $key => $val)
				$this->set_var($key,$val);
			return;
		}
		if ($name==null || !is_string($name))
			return;
		$this->variable_data[$name] = $value;
	}

	public function remove_var($name="")
	{
		if ($name==null || !is_string($name))
			return;
		unset($this->variable_data[$name]);
	}

	public function reset_var()
	{
		$this->variable_data = array();
	}
	
	public function render($_ci_view,$_ci_return = FALSE)
	{
		$_ci_vars = $this->variable_data;

		$_ci_file_exists = FALSE;
		
		$_ci_path = $this->theme_path.'/views/'.$_ci_view.'.php';
		$_ci_x = explode('/', $_ci_path);
		$_ci_file = end($_ci_x);
		
		if ( ! $_ci_file_exists && ! file_exists($_ci_path))
		{
			show_error('Unable to load the requested file: '.$_ci_file);
		}
		
		if (is_array($_ci_vars))
		{
			$this->cached_vars = array_merge($this->cached_vars, $_ci_vars);
		}
		extract($this->cached_vars);
		
		ob_start();
		
		include($_ci_path);
		
		log_message('debug', 'File loaded: '.$_ci_path);
		
		if ($_ci_return === TRUE)
		{
			$_ci_buffer = ob_get_contents();
			@ob_end_clean();
			return $_ci_buffer;
		}
		
		$_ci_CI =& get_instance();		
		if (ob_get_level() > $this->ob_level + 1)
		{
			ob_end_flush();
		}
		else
		{
			$output = ob_get_contents();
			$_ci_CI->output->append_output($output);
			@ob_end_clean();
		}
	}

	public function get_menu($m=0)
	{
		if (is_array($m))
		{
			$menus = array();
			foreach($m as $i)
			{
				$menu = $this->get_menu($i);
				array_push($menus, $menu);
			}
			return $menus;
		}
		$this->_CI->load->model('menu_model', 'Menu');
		$menu = $this->_CI->Menu->get_by_id($m);
		if ($menu == NULL || $menu === NULL || !$menu->is_valid())
			$menu = $this->_CI->Menu->get_by_name($m);
		return $menu;
	}

	public function auth()
	{
		$this->_CI->load->model('auth_model','Auth');
		return $this->_CI->Auth;
	}
	
}

/* End of file Theme.php */
/* Location: ./application/core/Theme.php */