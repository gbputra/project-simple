<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->theme =& load_class('Theme', 'core', '');
	}
	
}

class MY_Admin_Controller extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		if (!is_logged_in(1))
		{
			$this->message->add_message('error', lang('you_must_login_as_admin_first'));
			if (!is_logged_in(2))
				redirect('auth/login');
			else
				redirect('member');
		}
	}
	
}

class MY_Member_Controller extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		// if not logged in as member
		if (!is_logged_in(2))
		{
			if (is_logged_in(1))
			{
				$this->message->add_message('error', lang('you_must_login_first'));
				redirect('admin');
			}else
			{
				$this->message->add_message('error', lang('you_must_login_first'));
				redirect('auth/login');
			}
		}
	}
	
}

class MY_MainMenu_Controller extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		// if not logged in as member
		if (!is_logged_in(2))
		{
			if (is_logged_in(1))
			{
				$this->message->add_message('error', lang('you_must_login_first'));
				redirect('admin');
			}else
			{
				$this->message->add_message('error', lang('you_must_login_first'));
				redirect('auth/login');
			}
		}
	}
	
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */