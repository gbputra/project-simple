<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Config extends CI_Config {
	
	public function __construct()
	{
		parent::__construct();
		
		require_once(BASEPATH.'database/DB.php');
		$db =& DB();		
		$res = $db->get('settings');
		$res = $res->result_array();
		foreach ($res as $r)
		{
			$n = explode('.',$r['name']);
			
			if (count($n) > 1)
			{
				$item =& $this->config;
				for ($i = 0; $i < count($n); $i++)
				{
					if (array_key_exists($n[$i], $item))
					{
						$item =& $item[$n[$i]];
						continue;
					}
					$item[$n[$i]] = array();
					$item =& $item[$n[$i]];
				}
			}else
				$item =& $this->config[$r['name']];

			$item = $r['value'];
		}
	}
	
}

/* End of file MY_Config.php */
/* Location: ./application/core/MY_Config.php */