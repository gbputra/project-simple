<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Gafest
 * 
 * @author		Jauhar Arifin <jauhararifin10@gmail.com>
 * 
 */

// ------------------------------------------------------------------------

/**
 * Gafest Category Controller
 *
 * @author		Jauhar Arifin <jauhararifin10@gmail.com>
 *
 */

class Category extends MY_Controller {

	/**
	 * Model Constructor
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
		
		if (is_logged_in(2))
		{
			$this->message->keep_data();
			redirect('member');
			return;
		}
		/*
		if (is_logged_in(1))
		{
			$this->message->keep_data();
			redirect('admin');
			return;
		}
		*/
		$this->load->model("category_model", "Category");
	}

	/**
	 * Display category
	 *
	 * @param string | integer		category identifier, can be category ID or category name
	 */
	public function index($p = "",$num=2,$page=0)
	{
		// Get category identifier
		if ($p == NULL || $p == "")
		{
			$p = $this->input->get("p");
		}
		
		$category;

		// Get category object
		if ($p == NULL)
		{
			$category = $this->Category->home_category();
		}else
		{
			$category = $this->Category->get_by_name($p);
			if (!$category->is_valid())
			{
				$category = $this->Category->get_by_id($p);
			}
		}
		
		$this->load->model("menu_model","Menu");
		$default_menu = $this->Menu->default_menu();
		
		// Is category exists
		if ($category->is_valid())
		{
			//Render category
			$this->theme->title = $category->window_title();
			if ($num < 0)
				$num = count($category->articles);
			$page_num = ceil(count($category->articles))/$num;
			$category->articles = array_slice($category->articles, $page*$num, $num);
			$this->theme->set_var('category',$category);
			$this->theme->set_var('num',$num);
			$this->theme->set_var('page',$page);
			$this->theme->set_var('page_num',$page_num);
			$this->theme->render($category->view);
		}else
		{
			// Category not found
			$data = array(
				"code"=>404,
				"name"=>"category_not_found",
				"message"=>$this->lang->line('category_not_found'),
				"title"=>$this->lang->line('category_error')
			);
			$this->theme->set_var($data);
			$this->theme->render('error');
		}
	}
}

/* End of file category.php */
/* Location: ./application/controllers/category.php */
