<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Auth_model', 'Auth');
		$this->load->library("session");
	}
	
	public function index()
	{
		if ($this->Auth->is_logged_in() && $this->Auth->is_admin())
		{
			redirect(base_url().'member');
		}else
		{
			redirect('login');
		}
	}
	
	public function login()
	{	
		if (is_logged_in(2))
		{
			//$this->message->add_message("error",  $this->lang->line('you_must_logout_first'));
			redirect(base_url('member'));
		}else if (is_logged_in(1))
		{
			//$this->message->add_message("error",  $this->lang->line('you_must_logout_first'));
			redirect(base_url('admin'));
		}else if ($this->input->post("login"))
		{
			$this->load->library("form_validation");
			
			$this->lang->load('form');
			
			$rules=array(
				
				array(
					"field"=>"username",
					"label"=>$this->lang->line('username_field'),
					"rules"=>"required|xss_clean"
				),
				
				array(
					"field"=>"password",
					"label"=>$this->lang->line('password'),
					"rules"=>"required|xss_clean"
				)
							
			);
			
			$this->form_validation->set_rules($rules);
			
			if ($this->form_validation->run())
			{
				if ($this->Auth->login($this->input->post("username"), $this->input->post("password")))
				{					
					$this->message->add_message("success", $this->lang->line('login_successfull'));
					if (is_logged_in(1))
					{
						redirect (base_url('admin'));
					}else
					{
						redirect (base_url('member'));
					}
				}else
				{
					$this->message->add_message("error",  $this->lang->line('login_authentication_error'), TRUE);
					$this->theme->title = lang('login');
					$this->theme->render("login");
				}
			}else
			{
				$this->message->add_message("error",  $this->lang->line('login_information_not_valid'), TRUE);
				$this->theme->title = lang('login');
				$this->theme->render("login");
			}
		}else
		{
			$this->theme->title = lang('login');
			$this->theme->render("login");
		}
	}

	public function register()
	{
		if (is_logged_in(2))
		{
			$this->message->add_message("error",  $this->lang->line('you_must_logout_first'));
			redirect(base_url().'member');
		}
		else if (is_logged_in(1))
		{
			$this->message->add_message("error",  $this->lang->line('you_must_logout_first'));
			redirect(base_url().'admin');
		}else if ($this->input->post("register"))
		{
			$this->load->library("form_validation");
			
			$this->lang->load('form');
			
			$rules=array(
				
				array(
					"field"=>"fullname",
					"label"=>$this->lang->line('fullname_field'),
					"rules"=>"required|min_length[3]|max_length[255]|alpha_numeric_dash_space|xss_clean"
				),
				
				array(
					"field"=>"nickname",
					"label"=>$this->lang->line('nickname_field'),
					"rules"=>"required|max_length[64]|alpha_numeric_dash_space|xss_clean"
				),
				
				array(
					"field"=>"gender",
					"label"=>$this->lang->line('gender_field'),
					"rules"=>"required|integer|less_than[2]|greater_than[-1]|xss_clean"
				),
				
				array(
					"field"=>"birthdate",
					"label"=>$this->lang->line('birthdate_field'),
					"rules"=>"required|valid_date_yyyy_mm_dd|xss_clean"
				),
				
				
				array(
					"field"=>"class_orign",
					"label"=>$this->lang->line('class_orign'),
					"rules"=>"required|integer|greater_than[6]|less_than[13]|xss_clean"
				),
				
				array(
					"field"=>"email",
					"label"=>$this->lang->line('email_field'),
					"rules"=>"required|max_length[255]|valid_email|is_unique[users.email]|xss_clean"
				),
				
				array(
					"field"=>"mobile_number",
					"label"=>$this->lang->line('phone_number_field'),
					"rules"=>"required|max_length[16]|valid_phone|xss_clean"
				),
				
				array(
					"field"=>"home_address",
					"label"=>$this->lang->line('home_address_field'),
					"rules"=>"required|min_length[3]|max_length[255]|valid_address|xss_clean"
				),
				
				array(
					"field"=>"home_city",
					"label"=>$this->lang->line('home_city_field'),
					"rules"=>"required|max_length[32]|alpha_numeric_dash_space|xss_clean"
				),
				
				array(
					"field"=>"home_province",
					"label"=>$this->lang->line('home_province_field'),
					"rules"=>"required|max_length[32]|alpha_numeric_dash_space|xss_clean"
				),
				
				array(
					"field"=>"username",
					"label"=>$this->lang->line('username_field'),
					"rules"=>"required|max_length[32]|alpha_dash|is_unique[users.username]|xss_clean"
				),
				
				array(
					"field"=>"password",
					"label"=>$this->lang->line('password_field'),
					"rules"=>"required|min_length[4]|max_length[32]|xss_clean"
				),
				
				array(
					"field"=>"cpassword",
					"label"=>$this->lang->line('cpassword_field'),
					"rules"=>"required|matches[password]|xss_clean"
				)
							
			);
			
			$this->form_validation->set_rules($rules);
			
			if ($this->form_validation->run())
			{
				$user = array(
					'fullname' 		=> $this->input->post('fullname'),
					'nickname' 		=> $this->input->post('nickname'),
					'gender'		=> $this->input->post('gender'),
					'birthdate'		=> $this->input->post('birthdate'),
					'classorign'	=> $this->input->post('classorign'),
					'email'			=> $this->input->post('email'),
					'mobile_number'	=> $this->input->post('phone_number'),
					'home_address'	=> $this->input->post('home_address'),
					'home_city'		=> $this->input->post('home_city'),
					'home_province'	=> $this->input->post('home_province'),
					'username'		=> $this->input->post('username'),
					'password'		=> $this->input->post('password')
				);
				
				$this->load->model("user_model", "User");
				
				if ($this->User->register($user))
				{
					$this->message->add_message('success',  $this->lang->line('user_registered'));
					redirect(conf_item('base_url').'auth/login');
				}else
				{
					$this->message->add_message('error',  $this->lang->line('register_error'), TRUE);
					$this->theme->title = lang('register');
					$this->theme->render('register');
				}
			}else
			{
				$this->message->add_message('error',  $this->lang->line('data_not_valid'), TRUE);
				$this->theme->title = lang('register');
				$this->theme->render('register');
			}
		}else
		{
			$this->theme->title = lang('register');
			$this->theme->render('register');
		}
	}
	
	public function logout()
	{
		if (!is_logged_in())
		{
			$this->message->add_message('warning', lang('you_are_not_logged_in'));
		}else
		{
			if ($this->input->post('logout'))
			{
				$this->Auth->logout();
				$this->message->add_message('success',  $this->lang->line('logout_successfull'));
			}else
			{
				redirect('');
				return;
			}
		}
		redirect ('auth/login');
	}
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */