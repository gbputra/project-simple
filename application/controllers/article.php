<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Article extends MY_Controller {
	

	public function __construct()
	{
		parent::__construct();
		
		if (is_logged_in(2))
		{
			$this->message->keep_data();
			redirect('member');
			return;
		}
		/*
		if (is_logged_in(1))
		{
			$this->message->keep_data();
			redirect('admin');
			return;
		}
		*/
		$this->load->model("Article_model","Article");
	}
	
	public function index($a="")
	{
		// Set value of a, article identifier
		if ($a == NULL)
		{
			$a = $this->input->get("a");
		}
		
		$article;
		
		if ($a == NULL)
		{
			$article = $this->Article->default_article();
		}else
		{
			$article = $this->Article->get_by_name($a);
			if (!$article->is_valid())
			{
				$article = $this->Article->get_by_id($a);
			}
		}

		$this->load->model("menu_model","Menu");
		$default_menu = $this->Menu->default_menu();
		
		if ($article->is_valid())
		{
			$this->theme->title = $article->title;
			$this->theme->set_var("article",$article);
			$this->theme->render($article->view);
		}else
		{
			$data = array(
				"code"=>404,
				"name"=>"article_not_found",
				"message"=>$this->lang->line('article_not_found'),
				"title"=>$this->lang->line('article_error')
			);
			$this->theme->set_var($data);
			$this->theme->render('error');
		}
	}
	
}

/* End of file article.php */
/* Location: ./application/controllers/article.php */