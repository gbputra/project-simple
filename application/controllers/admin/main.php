<?php

class Main extends MY_Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->theme->title = lang('administrator');
		$this->theme->render('admin/main');
	}
	
}