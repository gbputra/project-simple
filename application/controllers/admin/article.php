<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article extends MY_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('article_model', 'Article');
	}
	
	/**
	 * Controller index action, display articles
	 * 
	 * @access public
	 */
	public function index($num=10,$page=0)
	{
		$articles = $this->Article->get_num($num,array(),$page*$num);
		$this->theme->set_var(array('articles'=>$articles, 'page'=>$page, 'num'=>$num));
		$this->theme->render('admin/article-list');
	}
	
	/**
	 * Controller edit action helper to render new article editor
	 * 
	 * @param string | integer
	 * @access public
	 */
	public function create()
	{
		if ($this->input->post('create'))
		{
			$this->load->library('form_validation');
		
			// Set input validation
			$rules = array(
				
				array(
					'field'=>'name',
					'label'=>lang('name_field'),
					'rules'=>'required|max_length[32]|alpha_dash|is_unique[articles.name]|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'title',
					'label'=>lang('title_field'),
					'rules'=>'required|max_length[64]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'subtitle',
					'label'=>lang('subtitle_field'),
					'rules'=>'required|max_length[255]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'view',
					'label'=>lang('view_field'),
					'rules'=>'required|max_length[32]|alpha_dash|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'status',
					'label'=>lang('status_field'),
					'rules'=>'required|numeric|integer|greater_than[0]|less_than[5]|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'category',
					'label'=>lang('category_field'),
					'rules'=>'required|numeric|integer|greater_than[0]|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'content',
					'label'=>lang('content_field'),
					'rules'=>'required|prep_for_form|xss_clean'
				),
				
			);
			
			$this->form_validation->set_rules($rules);
			
			if ($this->form_validation->run())
			{				
				// Set article data
				$article = array(
					'name'			=>$this->input->post('name'),
					'title'			=>$this->input->post('title'),
					'subtitle'		=>$this->input->post('subtitle'),
					'content'		=>$this->input->post('content'),
					'view'			=>$this->input->post('view'),
					'category'		=>$this->input->post('category'),
					'status'		=>$this->input->post('status'),
				);
				
				$this->load->model('article_model', 'Article');
				if ($this->Article->create($article))
				{
					$this->message->add_message('success', lang('article_created'));
					redirect(base_url('admin/article/edit/'.$article['name']));
				}else
				{
					$this->message->add_message('error',  lang('article_create_error'), TRUE);
					$this->theme->render('admin/article-new');
				}
			}else
			{
				$this->message->add_message('error', lang('data_not_valid'), TRUE);
				$this->theme->render('admin/article-new');
			}
		}else
		{
			$this->theme->render('admin/article-new');
		}
	}
	
	/**
	 * Controller edit action, edit article in editor
	 * 
	 * @access public
	 */
	public function edit($a='')
	{
		if ($a == NULL || $a == '')
			$a = $this->input->get('a');
		$id = intval($a);
		
		if ($this->input->post('edit'))
		{
			$this->load->library('form_validation');
				
			$rules = array(
				
				array(
					'field'=>'name',
					'label'=>lang('name_field'),
					'rules'=>'required|max_length[32]|alpha_dash|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'title',
					'label'=>lang('title_field'),
					'rules'=>'required|max_length[64]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'subtitle',
					'label'=>lang('subtitle_field'),
					'rules'=>'required|max_length[255]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'view',
					'label'=>lang('view_field'),
					'rules'=>'required|max_length[32]|alpha_dash|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'status',
					'label'=>lang('status_field'),
					'rules'=>'required|numeric|integer|greater_than[0]|less_than[5]|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'category',
					'label'=>lang('category_field'),
					'rules'=>'required|numeric|integer|greater_than[0]|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'content',
					'label'=>lang('content_field'),
					'rules'=>'required|prep_for_form|xss_clean'
				),
				
			);
			
			$this->form_validation->set_rules($rules);
			
			if ($this->form_validation->run())
			{
				$article = array(
					'name'			=>$this->input->post('name'),
					'title'			=>$this->input->post('title'),
					'subtitle'		=>$this->input->post('subtitle'),
					'content'		=>$this->input->post('content'),
					'view'			=>$this->input->post('view'),
					'category'		=>$this->input->post('category'),
					'status'		=>$this->input->post('status'),
				);
				
				$this->load->model('article_model', 'Article');
				if ($this->Article->update($id, $article))
				{
					$this->message->add_message('success', lang('article_updated'));
					redirect(base_url('admin/article/edit/'.$article['name']));
				}else
				{
					$this->message->add_message('error', lang('article_update_failed'), TRUE);
					$this->edit_view($this->input->post('id'));
				}
			}else
			{
				$this->message->add_message('error', lang('data_not_valid'), TRUE);
				$this->edit_view($this->input->post('id'));
			}
		}else
		{
			$this->edit_view($a);
		}
	}
	
	/**
	 * Controller edit action helper to render article editor
	 * 
	 * @param string | integer
	 * @access public
	 */
	private function edit_view($a='')
	{			
		$article = $this->Article->get_by_name($a);
		
		if (!$article->is_valid())
			$article = $this->Article->get_by_id($a);
		
		$this->theme->set_var(array('article'=>$article));
		$this->theme->render('admin/article-edit');
	}
	
	public function delete($p='')
	{
		if ($this->input->post('delete'))
		{
			$id = $this->input->post('id');
	
			if ($this->Article->delete($id))
				$this->message->add_message('success', lang('article_deleted'));
			else
				$this->message->add_message('error', lang('article_delete_failed'));
		}
		redirect('admin/article');
	}

}

/* End of file article.php */
/* Location: ./application/controllers/admin/article.php */
