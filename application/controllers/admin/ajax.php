<?php

class Ajax extends MY_Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		echo '';
	}
	
	public function getContentById($id=0)
	{
		$this->load->model('content_model','Content');
		$content = $this->Content->get_by_id($id);
		if ($content->is_valid())
			echo json_encode($content->obj());
		else
			echo json_encode(NULL);
	}
	
	public function getContentByName($name='')
	{
		$this->load->model('content_model','Content');
		$content = $this->Content->get_by_name($name);
		if ($content->is_valid())
			echo json_encode($content->obj());
		else
			echo json_encode(NULL);
	}
	
	public function getContentByCategory($category=0,$num=-1,$offset=0)
	{
		$this->load->model('content_model','Content');
		$content = $this->Content->get_by_category($category,array(),$num,$offset);
		foreach ($content as $i => $j)
			if (!$j->is_valid())
				unset($content[$i]);
			else
				$content[$i] = $j->obj();
		echo json_encode($content);
	}
	
	public function getContentCategoryById($id=0)
	{
		$this->load->model('content_category_model','Content_category');
		$content_category = $this->Content_category->get_by_id($id);
		if ($content_category->is_valid())
			echo json_encode($content_category->obj());
		else
			echo json_encode(NULL);
	}
	
	public function getContentCategoryByName($name='')
	{
		$this->load->model('content_category_model','Content_category');
		$content_category = $this->Content_category->get_by_id($name);
		if ($content_category->is_valid())
			echo json_encode($content_category->obj());
		else
			echo json_encode(NULL);
	}
	
	public function getContentCategoryByParent($parent=0,$num=-1,$offset=0)
	{
		$this->load->model('content_category_model','Content_category');
		$content_category = $this->Content_category->get_by_parent($parent,$num,$offset);
		foreach ($content_category as $i => $j)
			if (!$j->is_valid())
				unset($content_category[$i]);
			else
				$content_category[$i] = $j->obj();
		echo json_encode($content_category);
	}
	
	public function getUserById($id=0)
	{
		$this->load->model('user_model','User');
		$user = $this->User->get_by_id($id);
		if ($user->is_valid())
			echo json_encode($user->obj());
		else
			echo json_encode(NULL);
	}
	
	public function getUserByUsername($username='')
	{
		$this->load->model('user_model','User');
		$user = $this->User->get_by_username($username);
		if ($user->is_valid())
			echo json_encode($user->obj());
		else
			echo json_encode(NULL);
	}
	
	public function getUserByEmail($email='')
	{
		$this->load->model('user_model','User');
		$user = $this->User->get_by_email($email);
		if ($user->is_valid())
			echo json_encode($user->obj());
		else
			echo json_encode(NULL);
	}
	
}