<?php

class Setting extends MY_Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		if ($this->input->post('edit'))
		{
			$this->load->library('form_validation');

			$rules = array(

				array(
					'field'=>'site_name',
					'label'=>'Site Name',
					'rules'=>'required|max_length[255]|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'site_description',
					'label'=>'Site Description',
					'rules'=>'required|max_length[255]|prep_for_form|xss_clean'
				),

				array(
					'field'=>'theme[theme_name]',
					'label'=>'Theme',
					'rules'=>'required|max_length[255]|prep_for_form|xss_clean'
				),

			);
			$this->form_validation->set_rules($rules);
			
			if ($this->form_validation->run())
			{
				$setting = array(
					'site_name'			=>$this->input->post('site_name'),
					'site_description'	=>$this->input->post('site_description'),
					'theme' 			=>$this->input->post('theme'),
				);
				
				$this->load->model('setting_model', 'Setting');
				if ($this->Setting->set_value($setting))
				{
					$this->message->add_message('success', lang('setting_saved'), TRUE);
				}else
				{
					$this->message->add_message('error', lang('setting_save_error'), TRUE);
				}
			}else
			{
				$this->message->add_message('error', lang('data_not_valid'), TRUE);
			}
		}
		$this->theme->title = lang('administrator');
		$settings = get_config();
		$this->theme->set_var(array('settings'=>$settings));
		$this->theme->render('admin/setting');
	}
	
}