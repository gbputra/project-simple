<?php

class Category extends MY_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('category_model', 'Category');
	}
	
	public function index()
	{
		$categories = $this->Category->get_num(-1,array());
		
		$this->theme->title = lang('categories');
		$this->theme->set_var(array('categories'=>$categories));
		$this->theme->render('admin/category-list');
	}
}
