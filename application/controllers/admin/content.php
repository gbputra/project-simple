<?php

class Content extends MY_Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('content_category_model', 'Content_category');
		$this->load->model('content_model', 'Content');
	}
	
	public function _remap($method, $params = array())
	{
		if ($method == 'get-category-json')
			$method = 'get_category_json';
		if ($method == 'get-content-json')
			$method = 'get_content_json';
		if ($method == 'categories')
			$method = 'index';
		if (method_exists($this, $method))
			return call_user_func_array(array($this, $method), $params);
		show_404();
	}
	
	public function index($num=8, $page=0)
	{
		$page = intval($page);
		$offset = intval($page*$num);
		
		$categories = $this->Content_category->get_by_parent(0,$num,$offset);

		$this->theme->title = lang('administrator');
		$this->theme->set_var(array('categories'=>$categories, 'num'=>$num, 'page'=>$page));
		$this->theme->render('admin/content-manager');
	}
	
	public function get_category_json($action='byID', $identifier=1, $num=-1, $page=0)
	{
		$offset = $num*$page;
		$category = NULL;
		if ($action == 'byID')
		{
			$category = $this->Content_category->get_by_id($identifier,$num,$offset);
			$category = array(
				'id'			=>	$category->id,
				'name'			=>	$category->name,
				'title'			=>	$category->title,
				'parent'		=>	$category->parent,
				'subtitle'		=>	$category->subtitle,
				'author'		=>	$category->author,
				'datecreated'	=>	$category->date_created,
				'dateupdated'	=>	$category->dateupdated,
				'contents'		=>	$category->contents,
				'child'			=>	$category->child,
				'status'		=>	$category->status
			);
			echo json_encode($category);
		}else if ($action == 'byParent')
		{
			$cat = $this->Content_category->get_by_parent($identifier,$num,$offset);
			$categories = array();
			foreach ($cat as $c)
			{			
				$category = array(
					'id'			=>	$c->id,
					'name'			=>	$c->name,
					'title'			=>	$c->title,
					'parent'		=>	$c->parent,
					'subtitle'		=>	$c->subtitle,
					'author'		=>	$c->author,
					'datecreated'	=>	$c->datecreated,
					'dateupdated'	=>	$c->dateupdated,
					'contents'		=>	$c->contents,
					'child'			=>	$c->child,
					'status'		=>	$c->status
				);
				array_push($categories, $category);
			}
			echo json_encode($categories);
		}
	}
	
	public function get_content_json($action='byID', $identifier=1, $num=-1, $page=0)
	{
		$offset = $num*$page;
		$content = NULL;
		$this->load->model('content_model', 'Content');
		if ($action == 'byID')
		{
			$identifier = intval($identifier);			
			$content = $this->Content->get_by_id($identifier,$num,$offset);
			$content = array(
				'id'			=>	$content->id,
				'name'			=>	$content->name,
				'title'			=>	$content->title,
				'subtitle'		=>	$content->subtitle,
				'author'		=>	$content->author,
				'datecreated'	=>	$content->datecreated,
				'dateupdated'	=>	$content->dateupdated,
				'datepublished' =>	$content->datepublished,
				'content'		=>	$content->content,
				'status'		=>	$content->status,
				'category'		=>	$content->category
			);
			echo json_encode($content);
		}else if ($action == 'byCategory')
		{
			$identifier = intval($identifier);
			$content = $this->Content->get_by_category($identifier,array(),$num,$offset);
			$contents = array();
			foreach($content as $c)
			{
				$cc = array();
				$cc = array(
					'id'			=>	$c->id,
					'name'			=>	$c->name,
					'title'			=>	$c->title,
					'subtitle'		=>	$c->subtitle,
					'author'		=>	$c->author,
					'datecreated'	=>	$c->datecreated,
					'dateupdated'	=>	$c->dateupdated,
					'datepublished' =>	$c->datepublished,
					'content'		=>	$c->content,
					'status'		=>	$c->status,
					'category'		=>	$c->category
				);
				array_push($contents, $cc);
			}
			echo json_encode($contents);
		}
	}
	
	public function deletecategory($p='')
	{
		if ($this->input->post('delete'))
		{
			$id = $this->input->post('id');
			$category = $this->Content_category->get_by_id($id);
	
			if ($this->Content_category->delete($category->id))
				$this->message->add_message('success', lang('content_category_deleted'));
			else
				$this->message->add_message('error', lang('content_category_delete_failed'));
		}
		redirect('admin/content');
	}
	
	public function deletecontent($p='')
	{
		if ($this->input->post('delete'))
		{
			$id = $this->input->post('id');
			$content = $this->Content->get_by_id($id);
	
			if ($this->Content->delete($content->id))
				$this->message->add_message('success', lang('content_deleted'));
			else
				$this->message->add_message('error', lang('content_delete_failed'));
		}
		redirect('admin/content');
	}

	public function createcategory()
	{
		if ($this->input->post('create'))
		{
			$this->load->library('form_validation');

			$this->lang->load('form');

			$rules = array(

				array(
					'field'=>'name',
					'label'=>lang('name_field'),
					'rules'=>'required|max_length[64]|alpha_dash|is_unique[content_categories.name]|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'title',
					'label'=>lang('title_field'),
					'rules'=>'required|max_length[64]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),

				array(
					'field'=>'subtitle',
					'label'=>lang('subtitle_field'),
					'rules'=>'required|max_length[255]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),

				array(
					'field'=>'parent',
					'label'=>lang('parent_field'),
					'rules'=>'required|numeric|integer|is_natural|prep_for_form|xss_clean'
				),

				array(
					'field'=>'status',
					'label'=>lang('status_field'),
					'rules'=>'required|numeric|integer|greater_than[0]|less_than[5]|prep_for_form|xss_clean'
				),

			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run())
			{
				$category = array(
					'name'		=> $this->input->post('name'),
					'title'		=> $this->input->post('title'),
					'subtitle'	=> $this->input->post('subtitle'),
					'parent'	=> $this->input->post('parent'),
					'status'	=> $this->input->post('status')
				);
				
				$this->load->model('content_category_model','Content_category');
				
				if ($this->Content_category->create($category))
				{
					$this->message->add_message('success',  lang('content_category_created'));
					redirect('admin/content');
				}else
					$this->message->add_message('error',  lang('create_content_category_error'), TRUE);
			}else
			{
				$this->message->add_message('error',  lang('data_not_valid'), TRUE);
			}
		}
		$this->theme->title = lang('create_content_category');
		$this->theme->render('admin/content-category-new');
	}
	
	public function createcontent()
	{
		if ($this->input->post('create'))
		{
			$this->load->library('form_validation');

			$this->lang->load('form');

			$rules = array(

				array(
					'field'=>'name',
					'label'=>lang('name_field'),
					'rules'=>'required|max_length[64]|alpha_dash|is_unique[contents.name]|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'title',
					'label'=>lang('title_field'),
					'rules'=>'required|max_length[64]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),

				array(
					'field'=>'subtitle',
					'label'=>lang('subtitle_field'),
					'rules'=>'required|max_length[255]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),

				array(
					'field'=>'category',
					'label'=>lang('category_field'),
					'rules'=>'required|numeric|integer|is_natural|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'content',
					'label'=>lang('content_field'),
					'rules'=>'xss_clean'
				),

				array(
					'field'=>'status',
					'label'=>lang('status_field'),
					'rules'=>'required|numeric|integer|greater_than[0]|less_than[5]|prep_for_form|xss_clean'
				),

			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run())
			{
				$content = array(
					'name'		=> $this->input->post('name'),
					'title'		=> $this->input->post('title'),
					'subtitle'	=> $this->input->post('subtitle'),
					'category'	=> $this->input->post('category'),
					'content'	=> $this->input->post('content'),
					'status'	=> $this->input->post('status')
				);
				
				$this->load->model('content_model','Content');
				
				if ($this->Content->create($content))
				{
					$this->message->add_message('success',  lang('content_created'));
					redirect('admin/content');
				}else
					$this->message->add_message('error',  lang('create_content_error'), TRUE);
			}else
			{
				$this->message->add_message('error',  lang('data_not_valid'), TRUE);
			}
		}
		$this->theme->title = lang('create_content');
		$this->theme->render('admin/content-new');
	}
	
	public function editcategory($id)
	{
		if (!isset($id))
			$id = 0;
		$id = intval($id);
		
		if ($this->input->post('edit'))
		{			
			$this->load->library('form_validation');

			$this->lang->load('form');

			$rules = array(

				array(
					'field'=>'name',
					'label'=>lang('name_field'),
					'rules'=>'required|max_length[64]|alpha_dash|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'title',
					'label'=>lang('title_field'),
					'rules'=>'required|max_length[64]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),

				array(
					'field'=>'subtitle',
					'label'=>lang('subtitle_field'),
					'rules'=>'required|max_length[255]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),

				array(
					'field'=>'parent',
					'label'=>lang('parent_field'),
					'rules'=>'required|numeric|integer|is_natural|prep_for_form|xss_clean'
				),

				array(
					'field'=>'status',
					'label'=>lang('status_field'),
					'rules'=>'required|numeric|integer|greater_than[0]|less_than[5]|prep_for_form|xss_clean'
				),

			);
			
			
			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() && $id > 0)
			{
				$category = array(
					'name'		=> $this->input->post('name'),
					'title'		=> $this->input->post('title'),
					'subtitle'	=> $this->input->post('subtitle'),
					'parent'	=> $this->input->post('parent'),
					'status'	=> $this->input->post('status')
				);
				
				$this->load->model('content_category_model','Content_category');
				
				if ($this->Content_category->update($id, $category))
				{
					$this->message->add_message('success',  lang('content_category_updated'));
					redirect('admin/content');
				}else
					$this->message->add_message('error',  lang('update_content_category_error'), TRUE);
			}else
			{
				$this->message->add_message('error',  lang('data_not_valid'), TRUE);
			}
		}
		
		$category = $this->Content_category->get_by_id($id);
		
		$this->theme->set_var(array('category'=>$category));
		$this->theme->title = lang('edit_content_category');
		$this->theme->render('admin/content-category-edit');
	}
	
	public function editcontent($id)
	{
		if (!isset($id))
			$id = 0;
		$id = intval($id);
		
		if ($this->input->post('edit'))
		{			
			$this->load->library('form_validation');

			$this->lang->load('form');

			$rules = array(

				array(
					'field'=>'name',
					'label'=>lang('name_field'),
					'rules'=>'required|max_length[64]|alpha_dash|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'title',
					'label'=>lang('title_field'),
					'rules'=>'required|max_length[64]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),

				array(
					'field'=>'subtitle',
					'label'=>lang('subtitle_field'),
					'rules'=>'required|max_length[255]|alpha_numeric_dash_space|prep_for_form|xss_clean'
				),

				array(
					'field'=>'category',
					'label'=>lang('category_field'),
					'rules'=>'required|numeric|integer|is_natural|prep_for_form|xss_clean'
				),
				
				array(
					'field'=>'content',
					'label'=>lang('content_field'),
					'rules'=>'xss_clean'
				),

				array(
					'field'=>'status',
					'label'=>lang('status_field'),
					'rules'=>'required|numeric|integer|greater_than[0]|less_than[5]|prep_for_form|xss_clean'
				),

			);
			
			
			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() && $id > 0)
			{
				$content = array(
					'name'		=> $this->input->post('name'),
					'title'		=> $this->input->post('title'),
					'subtitle'	=> $this->input->post('subtitle'),
					'category'	=> $this->input->post('category'),
					'content'	=> $this->input->post('content'),
					'status'	=> $this->input->post('status')
				);
				
				$this->load->model('content_model','Content');
				
				if ($this->Content->update($id, $content))
				{
					$this->message->add_message('success',  lang('content_updated'));
					redirect('admin/content');
				}else
					$this->message->add_message('error',  lang('update_content_error'), TRUE);
			}else
			{
				$this->message->add_message('error',  lang('data_not_valid'), TRUE);
			}
		}
		
		$content = $this->Content->get_by_id($id);
		
		$this->theme->set_var(array('content'=>$content));
		$this->theme->title = lang('edit_content');
		$this->theme->render('admin/content-edit');
	}
	
}