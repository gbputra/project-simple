<?php

class Learn extends MY_Member_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{		
		$this->load->model('content_category_model', 'Content_category');
	
		$categories = $this->Content_category->get_by_parent(0);

		$this->theme->title = lang('category');
		$this->theme->set_var(array('categories'=>$categories));
		$this->theme->render('member/categories');
	}
	
	public function category($id=1)
	{
		$this->load->model('content_model', 'Content');
		
		$contents = $this->Content->get_by_category($id);
		
		$this->theme->title = lang('content');
		$this->theme->set_var(array('contents'=>$contents));
		$this->theme->render('member/contents');
	}
	
	public function content($id)
	{
		$this->load->model('content_model', 'Content');
		
		$content = $this->Content->get_by_id($id);
		
		$this->theme->title = lang('content');
		$this->theme->set_var(array('content'=>$content));
		$this->theme->render('member/content');
	}
	
}