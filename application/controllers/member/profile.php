<?php

class Profile extends MY_Member_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->theme->title = lang('profile');
		$this->theme->render('member/profile');
	}
	
	public function update_profile()
	{
		if ($this->input->post('update_profile'))
		{
			$this->load->library("form_validation");
			
			$this->lang->load('form');
			
			$rules=array(
				
				array(
					"field"=>"fullname",
					"label"=>$this->lang->line('fullname_field'),
					"rules"=>"required|min_length[3]|max_length[255]|alpha_numeric_dash_space|xss_clean"
				),
				
				array(
					"field"=>"nickname",
					"label"=>$this->lang->line('nickname_field'),
					"rules"=>"required|max_length[64]|alpha_numeric_dash_space|xss_clean"
				),
				
				array(
					"field"=>"gender",
					"label"=>$this->lang->line('gender_field'),
					"rules"=>"required|integer|less_than[2]|greater_than[-1]|xss_clean"
				),
				
				array(
					"field"=>"birthdate",
					"label"=>$this->lang->line('birthdate_field'),
					"rules"=>"required|valid_date_yyyy_mm_dd|xss_clean"
				),
				
				array(
					"field"=>"school_name",
					"label"=>$this->lang->line('school_name_field'),
					"rules"=>"required|min_length[3]|max_length[255]|alpha_numeric_dash_space|xss_clean"
				),
				
				array(
					"field"=>"school_address",
					"label"=>$this->lang->line('school_address_field'),
					"rules"=>"required|min_length[3]|max_length[255]|valid_address|xss_clean"
				),
				
				array(
					"field"=>"grade",
					"label"=>$this->lang->line('grade_field'),
					"rules"=>"required|integer|greater_than[6]|less_than[13]|xss_clean"
				),
				
				array(
					"field"=>"phone_number",
					"label"=>$this->lang->line('phone_number_field'),
					"rules"=>"required|max_length[16]|valid_phone|xss_clean"
				),
				
				array(
					"field"=>"home_address",
					"label"=>$this->lang->line('home_address_field'),
					"rules"=>"required|min_length[3]|max_length[255]|valid_address|xss_clean"
				),
				
				array(
					"field"=>"home_city",
					"label"=>$this->lang->line('home_city_field'),
					"rules"=>"required|max_length[32]|alpha_numeric_dash_space|xss_clean"
				),
				
				array(
					"field"=>"home_province",
					"label"=>$this->lang->line('home_province_field'),
					"rules"=>"required|max_length[32]|alpha_numeric_dash_space|xss_clean"
				),
											
			);
			
			$this->form_validation->set_rules($rules);
			
			if ($this->form_validation->run())
			{
				$user = array(
					'fullname' 		=> $this->input->post('fullname'),
					'nickname' 		=> $this->input->post('nickname'),
					'gender'		=> $this->input->post('gender'),
					'birthdate'		=> $this->input->post('birthdate'),
					'school_name'	=> $this->input->post('school_name'),
					'school_address'=> $this->input->post('school_address'),
					'grade'			=> $this->input->post('grade'),
					'phone_number'	=> $this->input->post('phone_number'),
					'home_address'	=> $this->input->post('home_address'),
					'home_city'		=> $this->input->post('home_city'),
					'home_province'	=> $this->input->post('home_province'),
				);
				
				$this->load->model('user_model', 'User');
				
				if ($this->User->update($user))
				{
					$this->message->add_message('success',  $this->lang->line('user_updated'));
					redirect('member/profile');
				}else
				{
					$this->message->add_message('warning',  $this->lang->line('user_update_error'), TRUE);
					$this->theme->title = lang('profile');
					$this->theme->render('member/profile');
				}
			}else
			{
				$this->theme->title = lang('profile');
				$this->theme->render('member/profile');
			}
		}else
		{
			redirect('member/profile');
		}
	}
	
	public function update_password()
	{
		if ($this->input->post('update_password'))
		{
			$this->load->library("form_validation");
			
			$this->lang->load('form');
			
			$rules = array(
				array(
					"field"=>"oldpassword",
					"label"=>$this->lang->line('oldpassword_field'),
					"rules"=>"required|min_length[4]|max_length[32]|xss_clean"
				),
				
				array(
					"field"=>"password",
					"label"=>$this->lang->line('password_field'),
					"rules"=>"required|min_length[4]|max_length[32]|xss_clean"
				),
				
				array(
					"field"=>"cpassword",
					"label"=>$this->lang->line('cpassword_field'),
					"rules"=>"required|matches[password]|xss_clean"
				)
							
			);
			
			$this->form_validation->set_rules($rules);
			
			if ($this->form_validation->run())
			{
				$oldpassword = $this->input->post('oldpassword');
				$password = $this->input->post('password');
								
				$this->load->model("user_model", "User");
				
				if ($this->User->update_password($oldpassword,$password))
				{
					$this->message->add_message("success",  $this->lang->line('password_updated'));
					redirect('member/profile');
				}else
				{
					$this->message->add_message("error",  $this->lang->line('password_update_error'), TRUE);
					$this->theme->title = lang('profile');
					$this->theme->render('member/profile');
				}
			}else
			{
				$this->theme->title = lang('profile');
				$this->theme->render('member/profile');
			}	
		}else
		{
			redirect('member/profile');
		}
	}
	
}