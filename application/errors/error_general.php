<!DOCTYPE html>
<html lang="en">
<head>
<title>Error</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
<style>
	body {
		background-color: #09F;
		color: #FFF;
		font-family: 'Open Sans Light', sans-serif;
	}
	#container {
		margin-top: 150px;
		text-align: center;
	}
</style>
</head>
<body>
	<div id="container">
		<h1 style="font-size: 52px;"><?php echo $heading; ?></h1>
        <h3><?php echo $message; ?></h3>
	</div>
</body>
</html>