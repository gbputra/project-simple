<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function get_value($name='')
	{
		if (!empty($name) && $name != '' && $name !== '' && $name != NULL)
		{
			$val = $this->db->select('value')->from('settings')->where('name',$name)->get();
			if ($val->num_rows() > 0)
				return $val->row('value');
		}
		return NULL;
	}
	
	public function get_all()
	{
		$settings = $this->db->get('settings');
		$settings = $settings->result_array();
		return $settings;
	}

	public function set_value($data=array()) {
		foreach ($data as $i => $j)
		{
			if (is_array($j))
			{
				$tmp = array();
				foreach ($j as $k => $l)
					$tmp[$i.'.'.$k] = $l;
				$this->set_value($tmp);
			}
			else
			{
				$this->db->where('name',$i)->update('settings',array('value'=>$j));
				$this->config->set_item($i, $j);
			}
		}
		return TRUE;
	}
	
}
