<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article_model extends CI_Model {
	
	const ARTICLE_STATUS_DRAFT = 1;
	const ARTICLE_STATUS_PUBLISHED = 2;
	
	private $ignore_draft = FALSE;
	private $found_rows = 0;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function ignore_draft()
	{
		$this->ignore_draft = TRUE;
	}
	
	public function get_by_id($id,$criteria=array())
	{
		$article = new Article_object;
		
		if (!$this->ignore_draft)
			$criteria['status'] = array(Article_model::ARTICLE_STATUS_DRAFT, Article_model::ARTICLE_STATUS_PUBLISHED);
		else
			$criteria['status'] = Article_model::ARTICLE_STATUS_PUBLISHED;
		
		$id = intval($id);
		if ($id == NULL || $id <= 0)
			return $article;
		$criteria['id'] = $id;
		
		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}
		
		$a = $this->db->get('articles');
		
		if ($a->num_rows() > 0)
		{
			$a = $a->row();
			foreach ($a as $i => $j)
			{
				$article->$i = $j;
			}
			if ($article->view == NULL || $article->view === NULL || $article->view == '')
			{
				$article->view = 'article';
			}			
		}
		return $article;
	}
	
	public function get_by_name($name,$criteria=array())
	{
		$article = new Article_object;
		
		if (!$this->ignore_draft)
			$criteria['status'] = array(Article_model::ARTICLE_STATUS_DRAFT, Article_model::ARTICLE_STATUS_PUBLISHED);
		else
			$criteria['status'] = Article_model::ARTICLE_STATUS_PUBLISHED;
		
		$name = (string) $name;
		if ($name == '' || $name == NULL)
			return $article;
			
		$criteria['name'] = $name;
				
		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}
		
		$a = $this->db->get('articles');
		
		if ($a->num_rows() > 0)
		{
			$a = $a->row();
			foreach ($a as $i => $j)
			{
				$article->$i = $j;
			}
			if ($article->view == NULL || $article->view === NULL || $article->view == '')
			{
				$article->view = 'article';
			}			
		}
		return $article;
	}
	
	public function get_num($num=-1,$criteria=array(),$offset=0)
	{
		$articles = array();
		
		if (!$this->ignore_draft)
			$criteria['status'] = array(Article_model::ARTICLE_STATUS_DRAFT, Article_model::ARTICLE_STATUS_PUBLISHED);
		else
			$criteria['status'] = Article_model::ARTICLE_STATUS_PUBLISHED;
		
		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}
		
		if ($num >= 0)
		{
			$ar = $this->db->get('articles', $num, $offset);
		}else
		{
			$ar = $this->db->get('articles');
		}
		if ($ar->num_rows() > 0)
		{
			$ar = $ar->result_array();
			foreach ($ar as $a)
			{
				$article = new Article_object;
				foreach ($a as $i => $j)
				{
					$article->$i = $j;
				}
				if (($article->view == NULL) || ($article->view === NULL) || ($article->view == ''))
				{
					$article->view = 'article';
				}
				array_push($articles, $article);
			}
		}
		
		$query = $this->db->query('SELECT FOUND_ROWS() AS `Count`');
		$this->found_rows = $query->row()->Count;
		
		return $articles;
	}
	
	public function gets_by_id($id=array(),$criteria=array())
	{
		$articles = array();
		
		if (!$this->ignore_draft)
			$criteria['status'] = array(Article_model::ARTICLE_STATUS_DRAFT, Article_model::ARTICLE_STATUS_PUBLISHED);
		else
			$criteria['status'] = Article_model::ARTICLE_STATUS_PUBLISHED;
		
		if (empty($id))
		{
			return $articles;
		}
		
		$criteria['id'] = $id;
		
		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}
		
		$article = $this->db->get('articles');
		
		if ($article->num_rows() > 0)
		{
			$article = $article->result_array();
			
			foreach ($article as $a)
			{
				$ar = new Article_object;
				foreach ($a as $i => $j)
				{
					$ar->$i = $j;
				}
				if ($ar->view == NULL || $ar->view === NULL || $ar->view == '')
				{
					$ar->view = 'article';
				}
				
				array_push($articles, $ar);
			}
		}
		return $articles;
	}
	
	public function default_article($criteria=array())
	{
		$article = new Article_object;
		
		if (!$this->ignore_draft)
			$criteria['status'] = array(Article_model::ARTICLE_STATUS_DRAFT, Article_model::ARTICLE_STATUS_PUBLISHED);
		else
			$criteria['status'] = Article_model::ARTICLE_STATUS_PUBLISHED;
		
		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}
		
		$this->db->select_min('id');
		$a = $this->db->get('articles');
		if ($a->num_rows() > 0)
		{
			$a = $a->result_array();
			$a = $a[0]['id'];
			$article = $this->get_by_id($a);
		}
		return $article;
	}
	
	public function create($article)
	{
		foreach ($article as $key => $val)
			if (empty($val) || $val == '' || $val == NULL)
				unset($article[$key]);
		
		if (!isset($article['name']) || $article['name'] == '')
			return FALSE;
		if (!isset($article['title']))
			$article['title'] = '';
		if (!isset($article['subtitle']))
			$article['subtitle'] = '';
		if (isset($article['category']))
			$article['category'] = intval($article['category']);
		if (!isset($article['category']) || $article['category'] < 0)
			$article['category'] = 0;
		if (!isset($article['view']) || $article['view'] == '' || $article == NULL)
			$article['view'] = 'article';	
		if (isset($article['status']))
			$article['status'] = intval($article['status']);
		if (!isset($article['status']) || $article['status'] < 1 || $article['status'] > 2)
			$article['status'] = 1;
		
		$article['author'] = user('id');
		
		if (isset($article['name']))
			$identifier = $article['name'];
		else
			return FALSE;
		if ($identifier == NULL || $identifier == '')
			return FALSE;
			
		$article['date_created'] = mktime();
		$article['date_updated'] = 0;
		$article['date_published'] = 0;
		if ($article['status'] == 2)
			$article['date_published'] = mktime();
		
		foreach($article as $i => $j)
		{
			switch($i)
			{
				case 'name':
				case 'title':
				case 'subtitle':
				case 'content':
				case 'view':
				case 'category':
				case 'author':
				case 'date_created':
				case 'date_updated':
				case 'date_published':
				case 'status':
					break;
				default:
					unset($article[$i]);
					break;
			}
		}
		
		$this->db->insert('articles', $article);
		
		return $this->db->insert_id();
	}
	
	public function update($id, $article)
	{
		foreach ($article as $key => $val)
			if (empty($val) || $val == '' || $val == NULL)
				unset($article[$key]);
		
		if (!isset($id))
			$id = 0;
		$id = intval($id);
		if ($id <= 0)
			return FALSE;
		
		if (!isset($article['name']) || $article['name'] == '')
			return FALSE;
		if (!isset($article['title']))
			$article['title'] = '';
		if (!isset($article['subtitle']))
			$article['subtitle'] = '';
		if (isset($article['category']))
			$article['category'] = intval($article['category']);
		if (!isset($article['category']) || $article['category'] < 0)
			$article['category'] = 0;
		if (!isset($article['view']) || $article['view'] == '' || $article == NULL)
			$article['view'] = 'article';	
		if (isset($article['status']))
			$article['status'] = intval($article['status']);
		if (!isset($article['status']) || $article['status'] < 1 || $article['status'] > 2)
			$article['status'] = 1;
		
		$article['author'] = user('id');
		
		if (isset($article['name']))
			$identifier = $article['name'];
		else
			return FALSE;
		if ($identifier == NULL || $identifier == '')
			return FALSE;
		
		$article['date_updated'] = mktime();
		
		foreach($article as $i => $j)
		{
			switch($i)
			{
				case 'name':
				case 'title':
				case 'subtitle':
				case 'content':
				case 'view':
				case 'category':
				case 'author':
				case 'date_created':
				case 'date_updated':
				case 'date_published':
				case 'status':
					break;
				default:
					unset($article[$i]);
					break;
			}
		}
		
		$this->db->where('id', $id)->update('articles', $article);
		
		return $this->db->affected_rows();
	}
	
	public function delete($id)
	{		
		if ($id > 0)
		{
			$this->db->delete('articles', array('id' => $id));
			return TRUE;
		}
		return FALSE;
	}
		
}

class Article_object {
	
	private $id					= 0;
	private $name				= '';
	private $author				= 0;
	private $date_created		= 0;
	private $date_updated		= 0;
	private $date_published		= 0;
	private $title				= '';
	private $subtitle			= '';
	private $content			= '';
	private $view				= 'article';
	private $status 			= 1;
	private $category	 		= 0;
	
	private $_CI;
	
	public function __construct()
	{
		$this->_CI = & get_instance();
	}
	
	/**
	 * Article Validity Check
	 *
	 * @access public
	 */
	public function is_valid()
	{
		// if id > 0 and name not '' and author > 0 and status > 0 then return true
		if ($this->id > 0 && $this->name != '' && $this->author > 0 && $this->status > 0)
			return TRUE;
		return FALSE;
	}
	
	public function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	public function __call($method, $arguments)
	{
		switch($method)
		{
			case 'id':
				return $this->id;
			case 'name':
				return $this->name;
			case 'title':
				return $this->title;
			case 'subtitle':
				return $this->subtitle;
			case 'author':
				$this->_CI->load->model('user_model', 'User');
				return $this->_CI->User->get_by_id($this->author);
			case 'date_created':
				return $this->date_created;
			case 'date_updated':
				return $this->date_updated;
			case 'date_published':
				return $this->date_updated;
			case 'category':
				return M_CATEGORY()->get_by_id($this->category);
			case 'content':
				return $this->content;
			case 'view':
				$this->view = ''.$this->view;
				return ($this->view == '') ? 'article' : $this->view;
			case 'status':
				return $this->status;
			default:
				return call_user_func_array(array($this, $method), $arguments);
		}
	}
	
	public function __get($property)
	{
		switch ($property)
		{
			case 'url':
				return config_item("base_url")."article/".$this->name;
				break;
		}
		return NULL;
	}
	
	public function obj()
	{
		$content = new stdClass();
		$content->id 			= 	$this->id;
		$content->name			=	$this->name;
		$content->title			=	$this->title;
		$content->subtitle		=	$this->subtitle;
		$content->author		=	$this->author;
		$content->date_created	=	$this->date_created;
		$content->date_updated	=	$this->date_updated;
		$content->date_published=	$this->date_published;
		$content->content		=	$this->content;
		$content->view			=	$this->view;
		$content->status		=	$this->status;
		$content->category		=	$this->category;
		return $content;
	}
	
}

/* End of file article_model.php */
/* Location: ./application/models/article_model.php */