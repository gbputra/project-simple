<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_category_model extends CI_Model {
	
	const CONTENT_CATEGORY_STATUS_DRAFT = 1;
	const CONTENT_CATEGORY_STATUS_PUBLISHED = 2;
		
	private $draft = TRUE;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function ignore_draft() {
		$this->draft = FALSE;
	}
	
	private function _normalize_criteria(&$criteria, &$content_criteria)
	{
		$this->load->model('content_model','Content');
		if ($this->draft)
		{
			$criteria['status'] = array(Content_category_model::CONTENT_CATEGORY_STATUS_DRAFT, Content_category_model::CONTENT_CATEGORY_STATUS_PUBLISHED);
			$criteria['content'] = array('status'=>array(Content_model::CONTENT_STATUS_DRAFT, Content_model::CONTENT_STATUS_PUBLISHED));
		}else
		{
			$criteria['status'] = Content_category_model::CONTENT_CATEGORY_STATUS_PUBLISHED;
			$criteria['content'] = array('status'=>Content_model::CONTENT_STATUS_PUBLISHED);
		}
		
		$content_criteria = array();
		if (isset($criteria['content']))
		{
			$content_criteria = $criteria['content'];
			if (!is_array($content_criteria))
				$content_criteria = array();
		}
		unset($criteria['content']);
	}
	
	private function _set_db_criteria($criteria)
	{
		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}
	}
	
	public function get_by_id($id,$criteria=array())
	{
		$category = new Content_category_object;

		if (!is_array($criteria))
			$criteria = array();

		if (empty($id) || $id == '' || $id == NULL || $id <= 0)
			return $category;
		
		$criteria['id'] = $id;
		$content_criteria = array();
		
		$this->_normalize_criteria($criteria, $content_criteria);

		$this->_set_db_criteria($criteria);

		$p = $this->db->get('content_categories',1,0);
		if ($p->num_rows() > 0)
		{
			$p = $p->row();
			foreach ($p as $i => $j)
				$category->$i = $j;
			$category->content = $this->get_content_category_contents($category->id, $content_criteria, -1, 0);
		}

		return $category;
	}
	
	public function get_by_name($name,$criteria=array())
	{
		$category = new Content_category_object;

		if (!is_array($criteria))
			$criteria = array();

		if (empty($name) || $name == '' || $name == NULL)
			return $category;

		$criteria['name'] = $name;
		$content_criteria = array();

		$this->_normalize_criteria($criteria, $content_criteria);

		$this->_set_db_criteria($criteria);

		$p = $this->db->get('content_categories', 1, 0);
		if ($p->num_rows() > 0)
		{
			$p = $p->row();
			foreach ($p as $i => $j)
				$category->$i = $j;	
			$category->contents = $this->get_content_category_contents($category->id, $content_criteria, -1, 0);
		}
		
		return $category;
	}

	public function gets_by_id($id=array(),$criteria=array(), $num=-1, $offset=0)
	{
		$categories = array();
		
		if (empty($id))
			return $categories;
		
		$criteria['id'] = $id;
		$content_criteria = array();

		$this->_normalize_criteria($criteria, $content_criteria);
		
		$this->_set_db_criteria($criteria);
		
		if ($num < 0)
			$category = $this->db->get('content_categories');
		else
			$category = $this->db->get('content_categories', $num, $offset);		
		if ($category->num_rows() > 0)
		{
			$category = $category->result_array();
			
			foreach ($category as $c)
			{
				$cr = new Content_category_object;
				foreach ($c as $i => $j)
					$cr->$i = $j;
				$cr->contents = $this->get_content_category_contents($cr->id, $content_criteria, -1, 0);				
				array_push($categories, $cr);
			}
		}
		return $categories;
	}
	
	public function get_num($num=-1,$criteria=array(),$offset=0)
	{
		$categories = array();
		
		if (!is_array($criteria))
			$criteria = array();

		$content_criteria = array();

		$this->_normalize_criteria($criteria, $content_criteria);
		
		$this->_set_db_criteria($criteria);

		$offset = intval($offset);
		$num = intval($num);
		if ($num >= 0)
			$pg = $this->db->get('content_categories', $num, $offset);
		else
			$pg = $this->db->get('content_categories');
		
		if ($pg->num_rows() > 0)
		{
			$pg = $pg->result_array();
			foreach ($pg as $p)
			{
				$category = new Content_category_object;
				foreach ($p as $i => $j)
					$category->$i = $j;
				$category->contents = $this->get_content_category_contents($category->id, $content_criteria, -1, 0);
				array_push($categories, $category);
			}
		}
		return $categories;
	}
	
	public function get_by_parent($parent=0,$num=-1,$offset=0)
	{
		$parent = intval($parent);
		if ($parent < 0)
			$parent = 0;
		return $this->get_num($num, array('parent'=>$parent), $offset);
	}
	
	public function get_content_category_contents($id,$criteria=array(), $num=-1, $offset=0)
	{	
		$contents = array();

		if (!is_array($criteria))
			$criteria = array();

		if (empty($id) || $id == '' || $id == NULL)
			return $contents;		

		$contents_tmp = array();

		if ($num < 0)
			$category_content = $this->db->get_where('contents', array('category'=>$id));
		else
			$category_content = $this->db->get_where('contents', array('category'=>$id), $num, $offset);

		if ($category_content->num_rows() > 0)
		{
			$category_content = $category_content->result_array();
			
			$c = 0;
			foreach ($category_content as $i => $j)
			{			
				array_push($contents_tmp, $j['id']);				
				$c++;
				if (($c >= $num) && ($num >= 0))
					break;
			}
		}		
		return $contents_tmp;
	}
	
	/** 
	 * Get the minimum category id
	 *
	 * @return	Content_category_object		default category
	 *
	 */
	public function default_category($criteria=array())
	{
		$category = new Content_category_object;

		$content_criteria = array();
		
		$this->_normalize_criteria($criteria, $content_criteria);
		
		$this->_set_db_criteria($criteria);
				
		$this->db->select_min('id');		
		$p = $this->db->get('content_categories',1,0);
		if ($p->num_rows() > 0)
		{
			$p = $p->row();
			foreach ($p as $i => $j)
				$category->$i = $j;
			$category->content = $this->get_content_category_contents($category->id, -1, $content_criteria);
		}
		
		return $category;
	}
	
	public function home_category($criteria=array())
	{
		$category = $this->get_by_id(conf_item('default_content_category'),$criteria);
		if ($category == NULL || $category === NULL || !$category->is_valid())
			return $this->default_category();
		return $category;
	}
	
	public function create($category)
	{
		foreach ($category as $key => $val)
			if (empty($val) || $val == '' || $val == NULL)
				unset($category[$key]);
		
		if (!isset($category['name']) || $category['name'] == '')
			return FALSE;
		if (!isset($category['title']))
			$category['title'] = '';
		if (!isset($category['subtitle']))
			$category['subtitle'] = '';
		if (isset($category['parent']))
			$category['parent'] = intval($category['parent']);
		if (!isset($category['parent']) || $category['parent'] < 0)
			$category['parent'] = 0;
		if (isset($category['status']))
			$category['status'] = intval($category['status']);
		if (!isset($category['status']) || $category['status'] < 1 || $category['status'] > 2)
			$category['status'] = 1;
		
		$category['author'] = user('id');
		
		if (isset($category['name']))
			$identifier = $category['name'];
		else
			return FALSE;
		if ($identifier == NULL || $identifier == '')
			return FALSE;
			
		$category['datecreated'] = mktime();
		$category['dateupdated'] = 0;
		
		foreach($category as $i => $j)
		{
			switch($i)
			{
				case 'name':
				case 'title':
				case 'subtitle':
				case 'parent':
				case 'author':
				case 'datecreated':
				case 'dateupdated':
				case 'status':
					break;
				default:
					unset($category[$i]);
					break;
			}
		}
		
		$this->db->insert('content_categories', $category);
		
		return $this->db->insert_id();
	}
	
	public function update($id, $category)
	{
		foreach ($category as $key => $val)
			if (empty($val) || $val == '' || $val == NULL)
				unset($category[$key]);
		
		if (!isset($id))
			$id = 0;
		$id = intval($id);
		if ($id <= 0)
			return FALSE;
		
		if (!isset($category['name']) || $category['name'] == '')
			return FALSE;
		if (!isset($category['title']))
			$category['title'] = '';
		if (!isset($category['subtitle']))
			$category['subtitle'] = '';
		if (isset($category['parent']))
			$category['parent'] = intval($category['parent']);
		if (!isset($category['parent']) || $category['parent'] < 0)
			$category['parent'] = 0;
		if (isset($category['status']))
			$category['status'] = intval($category['status']);
		if (!isset($category['status']) || $category['status'] < 1 || $category['status'] > 2)
			$category['status'] = 1;
		
		$category['author'] = user('id');
		
		if (isset($category['name']))
			$identifier = $category['name'];
		else
			return FALSE;
		if ($identifier == NULL || $identifier == '')
			return FALSE;
		
		$category['dateupdated'] = mktime();
		
		foreach($category as $i => $j)
		{
			switch($i)
			{
				case 'name':
				case 'title':
				case 'subtitle':
				case 'parent':
				case 'author':
				case 'dateupdated':
				case 'status':
					break;
				default:
					unset($category[$i]);
					break;
			}
		}
		
		$this->db->where('id', $id)->update('content_categories', $category);
		
		return $this->db->affected_rows();
	}
	
	public function delete($id)
	{
		if ($id > 0)
		{
			$this->db->delete('content_categories', array('id' => $id));
			$this->db->where('parent', $id)->update('content_categories', array('parent'=>'0'));
			return TRUE;
		}
		return FALSE;
	}
	
}

class Content_category_object {
	
	private $id				= 0;
	private $name			= '';
	private $title		 	= '';
	private $parent 		= 0;
	private $subtitle		= '';
	private $author			= 0;
	private $datecreated	= 0;
	private $dateupdated	= 0;
	private $contents		= array();
	private $status			= 1;
	
	private $_CI;
	
	public function __construct()
	{
		$this->_CI = & get_instance();
	}

	public function is_valid()
	{
		// if id > 0 and name not '' and author > 0 and status > 0 then return true
		if ($this->id > 0 && $this->name != '' && $this->author > 0 && $this->status > 0)
			return TRUE;
		return FALSE;
	}
	
	public function __call($method, $arguments)
	{
		switch($method)
		{
			case 'id':
				return $this->id;
			case 'name':
				return $this->name;
			case 'title':
				return $this->title;
			case 'parent':
				$this->_CI->load->model('content_category_model', 'Content_category');
				return $this->_CI->Content_category->get_by_id($this->parent);
			case 'subtitle':
				return $this->subtitle;	
			case 'author':
				$this->_CI->load->model('user_model', 'User');
				return $this->_CI->User->get_by_id($this->author);
			case 'datecreated':
				return $this->datecreated;
			case 'dateupdated':
				return $this->dateupdated;
			case 'contents':
				return $this->contents;
			case 'status':
				return $this->status;
			case 'child':
				$this->_CI->load->model('content_category_model', 'Content_category');
				return $this->_CI->Content_category->get_by_parent($this->id);
			default:
				return call_user_func_array(array($this, $method), $arguments);
		}
	}
	
	public function __set($property,$value)
	{
		$this->$property = $value;
	}
	
	public function __get($property)
	{
		if ($property == 'child')
		{
			$this->_CI->load->model('content_category_model', 'Content_category');
			$cat = $this->_CI->Content_category->get_by_parent($this->id);
			$categories = array();
			foreach ($cat as $c)
				array_push($categories, $c->id);
			return $categories;
		}
		return $this->$property;
	}
	
	public function obj()
	{
		$content = new stdClass();
		$content->id 			= $this->id;
		$content->name			=	$this->name;
		$content->title			=	$this->title;
		$content->subtitle		=	$this->subtitle;
		$content->author		=	$this->author;
		$content->datecreated	=	$this->datecreated;
		$content->dateupdated	=	$this->dateupdated;
		$content->contents		=	$this->contents;
		$content->status		=	$this->status;
		$content->parent		=	$this->parent;
		return $content;
	}
	
}

/* End of file category_model.php */
/* Location: ./application/model/category_model.php */
