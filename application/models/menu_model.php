<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_by_id($id,$criteria=array())
	{
		$menu = new Menu_object;

		if (!is_array($criteria))
			$criteria = array();

		if (empty($id) || $id == '' || $id == NULL)
			return $menu;
		
		$criteria['id'] = $id;

		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}

		$p = $this->db->get('menu');
		if ($p->num_rows() > 0)
		{
			$p = $p->row();
			foreach ($p as $i => $j)
			{
				$menu->$i = $j;
			}
			//$menu->items = $this->get_menu_items($menu->id, -1, $item_criteria);
		}
		
		return $menu;
	}

	public function get_by_name($name,$criteria=array())
	{
		$menu = new Menu_object;

		if (!is_array($criteria))
			$criteria = array();

		if (empty($name) || $name == '' || $name == NULL)
			return $menu;

		$criteria['name'] = $name;

		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}

		$p = $this->db->get("menu");
		if ($p->num_rows() > 0)
		{
			$p = $p->row();
			foreach ($p as $i => $j)
			{
				$menu->$i = $j;
			}			
			//$menu->items = $this->get_menu_items($menu->id, -1, $item_criteria);
		}
		
		return $menu;
	}

	public function get_num($num=-1,$criteria=array())
	{
		$menus = array();
		
		if (!is_array($criteria))
			$criteria = array();

		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}

		if ($num >= 0)
		{
			$pg = $this->db->get('menu', $num);
		}else
		{
			$pg = $this->db->get('menu');
		}
		
		if ($pg->num_rows() > 0)
		{
			$pg = $pg->result_array();
			foreach ($pg as $p)
			{
				$menu = new Menu_object;
				
				foreach ($p as $i => $j)
				{
					$menu->$i = $j;
				}				
				//$menu->items = $this->get_menu_items($menu->id, -1, $item_criteria);
				array_push($menus, $menu);
			}
		}
		return $menus;
	}

	public function default_menu($criteria=array())
	{
		$menu = new Menu_object;

		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}
		
		$this->db->select_min("id");
		$p = $this->db->get("menu");
		if ($p->num_rows() > 0)
		{
			$p = $p->result_array();
			$p = $p[0]["id"];
			$menu = $this->get_by_id($p);
		}
		return $menu;
	}

	public function save($menu)
	{
		$this->load->model("auth_model", "Auth");
		
		// If not admin then return false, cannot save data
		if ($this->Auth->is_logged_in() && $this->Auth->is_admin())
		{
			// Remove empty variable in menu
			foreach ($menu as $key => $val)
			{
				if (empty($val) || $val == '' || $val == NULL)
					unset($menu[$key]);
			}
			
			// Define menu's author by current logged user
			$menu["author"] = $this->Auth->user("id");
			
			// Setting identifier
			if (isset($menu['name']))
				$identifier = $menu['name'];
			else
				return FALSE;
			if ($identifier == NULL || $identifier == "")
				return FALSE;
			
			// Is using table update
			$update = TRUE;
			
			// Setting date data item
			$c_menu = $this->get_by_name($identifier,TRUE);
			
			$menu["date_updated"] = mktime();
			if (!$c_menu->is_valid())
			{
				// New item
				$update = FALSE;
				$menu["date_created"] = mktime();
			}

			foreach ($menu as $key => $val)
			{
				switch ($key)
				{
					case 'id':
					case 'name':
					case 'items':
					case 'author':
					case 'date_created':
					case 'date_updated':
						break;
					default:
						unset($menu[$key]);
				}
			}
			
			$id = $c_menu->id;
			unset($menu["id"]);
					
			if ($update)
				$this->db->where("name",$menu['name'])->update("menu", $menu);
			else{
				$this->db->insert('menu',$menu);
				$id = $this->db->insert_id();
			}

			return TRUE; // ?????
			$return = TRUE;
			if ($this->db->affected_rows() > 0)
				$return = $return && TRUE;
			else
				$return = $return && FALSE;
			
			return $return;
		}
		return FALSE;
	}

	public function delete($id)
	{
		$this->load->model("auth_model", "Auth");
		// If not admin then return false, cannot save data
		if ($this->Auth->is_logged_in() && $this->Auth->is_admin())
		{
			if ($id > 0)
			{
				$this->db->delete('menu', array('id' => $id));
				return TRUE;
			}else
				return FALSE;
		}
		return FALSE;
	}

}

class Menu_object {
	
	private $id				= 0;	
	private $name			= '';	
	private $author			= 0;	
	private $date_created	= 0;	
	private $date_updated	= 0;
	private $items			= array();
	
	private $_CI;

	public function __construct()
	{
		$this->_CI = & get_instance();
	}

	public function is_valid()
	{
		if ($this->id >= 0 && $this->name != '' && $this->author >= 0)
			return TRUE;
		return FALSE;
	}
	
	public function __set($property,$value)
	{
		if ($property == 'items')
		{
			if (is_string($value))
				$this->items = json_decode($value);
			return;
		}
		$this->$property = $value;
	}
	
	public function __get($property)
	{
		switch ($property)
		{
			case 'author':
				$this->_CI->load->model('user_model', 'User');
				return $this->_CI->User->get_by_id($this->author);
				break;
			case 'items':
				if (is_string($this->items))
					$items = json_decode($this->items);
				else
					$items = $this->items;
				if (!is_array($items) || empty($items))
					return array();
				return $items;
				break;
			default:
				return $this->$property;
				break;
		}
		return NULL;
	}
	
}

/* End of file menu_model.php */
/* Location: ./application/models/menu_model.php */