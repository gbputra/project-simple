<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model {
	
	const CONTENT_STATUS_DRAFT = 1;
	const CONTENT_STATUS_PUBLISHED = 2;
	
	private $ignore_draft = FALSE;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function ignore_draft()
	{
		$this->ignore_draft = TRUE;
	}
	
	private function _set_db_criteria($criteria)
	{
		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}
	}
	
	public function get_by_id($id,$criteria=array())
	{
		$content = new Content_object;
		
		if (!$this->ignore_draft)
			$criteria['status'] = array(Content_model::CONTENT_STATUS_DRAFT, Content_model::CONTENT_STATUS_PUBLISHED);
		else
			$criteria['status'] = Content_model::CONTENT_STATUS_PUBLISHED;
		
		$id = intval($id);
		if ($id == NULL || $id <= 0)
			return $content;
		$criteria['id'] = $id;
		
		$this->_set_db_criteria($criteria);
		
		$a = $this->db->get('contents',1,0);
		
		if ($a->num_rows() > 0)
		{
			$a = $a->row();
			foreach ($a as $i => $j)
				$content->$i = $j;		
		}
		return $content;
	}
	
	public function get_by_name($name,$criteria=array())
	{
		$content = new Content_object;
		
		if (!$this->ignore_draft)
			$criteria['status'] = array(Content_model::CONTENT_STATUS_DRAFT, Content_model::CONTENT_STATUS_PUBLISHED);
		else
			$criteria['status'] = Content_model::CONTENT_STATUS_PUBLISHED;
		
		$name = (string) $name;
		if ($name == '' || $name == NULL)
			return $content;
			
		$criteria['name'] = $name;
				
		$this->_set_db_criteria($criteria);
		
		$a = $this->db->get('contents',1,0);
		
		if ($a->num_rows() > 0)
		{
			$a = $a->row();
			foreach ($a as $i => $j)
				$content->$i = $j;
		}
		return $content;
	}
	
	public function get_num($num=-1,$criteria=array(),$offset=0)
	{
		$contents = array();
		
		if (!$this->ignore_draft)
			$criteria['status'] = array(Content_model::CONTENT_STATUS_DRAFT, Content_model::CONTENT_STATUS_PUBLISHED);
		else
			$criteria['status'] = Content_model::CONTENT_STATUS_PUBLISHED;
		
		$this->_set_db_criteria($criteria);
		
		if ($num >= 0)
			$ar = $this->db->get('contents', $num, $offset);
		else
			$ar = $this->db->get('contents');
			
		if ($ar->num_rows() > 0)
		{
			$ar = $ar->result_array();
			foreach ($ar as $a)
			{
				$content = new Content_object;
				foreach ($a as $i => $j)
					$content->$i = $j;
				array_push($contents, $content);
			}
		}
		return $contents;
	}
	
	public function gets_by_id($id=array(),$criteria=array(), $num=-1, $offset=0)
	{
		$contents = array();
		
		if (!$this->ignore_draft)
			$criteria['status'] = array(Content_model::CONTENT_STATUS_DRAFT, Content_model::CONTENT_STATUS_PUBLISHED);
		else
			$criteria['status'] = Content_model::CONTENT_STATUS_PUBLISHED;
		
		if (empty($id))
			return $contents;
		$criteria['id'] = $id;
		
		$this->_set_db_criteria($criteria);
		
		if ($num < 0)
			$content = $this->db->get('contents');		
		else
			$content = $this->db->get('contents', $num, $offset);
		if ($content->num_rows() > 0)
		{
			$content = $content->result_array();
			
			foreach ($content as $a)
			{
				$ar = new Content_object;
				foreach ($a as $i => $j)
					$ar->$i = $j;		
				array_push($contents, $ar);
			}
		}
		return $contents;
	}
	
	public function get_by_category($category=0, $criteria=array(), $num=-1, $offset=0)
	{
		$category = intval($category);
		$this->load->model('content_category_model', 'Content_category');
		$content_ids = $this->Content_category->get_content_category_contents($category);
		return $this->gets_by_id($content_ids, $criteria, $num, $offset);
	}
	
	public function default_content($criteria=array())
	{
		$content = new Content_object;
		
		if (!$this->ignore_draft)
			$criteria['status'] = array(Content_model::CONTENT_STATUS_DRAFT, Content_model::CONTENT_STATUS_PUBLISHED);
		else
			$criteria['status'] = Content_model::CONTENT_STATUS_PUBLISHED;
		
		$this->_set_db_criteria($criteria);
		
		$this->db->select_min('id');
		$a = $this->db->get('contents');
		if ($a->num_rows() > 0)
		{
			$a = $a->result_array();
			$a = $a[0]['id'];
			$content = $this->get_by_id($a);
		}
		return $content;
	}
	
	public function create($content)
	{
		foreach ($content as $key => $val)
			if (empty($val) || $val == '' || $val == NULL)
				unset($content[$key]);
		
		if (!isset($content['name']) || $content['name'] == '')
			return FALSE;
		if (!isset($content['title']))
			$content['title'] = '';
		if (!isset($content['subtitle']))
			$content['subtitle'] = '';
		if (isset($content['category']))
			$content['category'] = intval($content['category']);
		if (!isset($content['category']) || $content['category'] < 0)
			$content['category'] = 0;
		if (isset($content['status']))
			$content['status'] = intval($content['status']);
		if (!isset($content['status']) || $content['status'] < 1 || $content['status'] > 2)
			$content['status'] = 1;
		if (!isset($content['content']))
			$content['content'] = '<p>empty</p>';
		
		$content['author'] = user('id');
		
		if (isset($content['name']))
			$identifier = $content['name'];
		else
			return FALSE;
		if ($identifier == NULL || $identifier == '')
			return FALSE;
			
		if ($content['status'] == 2)
			$content['datepublished'] = mktime();
		else
			$content['datepublished'] = 0;
		$content['datecreated'] = mktime();
		$content['dateupdated'] = 0;
		
		foreach($content as $i => $j)
		{
			switch($i)
			{
				case 'name':
				case 'title':
				case 'subtitle':
				case 'category':
				case 'author':
				case 'datecreated':
				case 'dateupdated':
				case 'datepublished':
				case 'content':
				case 'status':
					break;
				default:
					unset($content[$i]);
					break;
			}
		}
		
		$this->db->insert('contents', $content);		
		
		return $this->db->insert_id();	
	}
	
	public function update($id, $content)
	{
		foreach ($content as $key => $val)
			if (empty($val) || $val == '' || $val == NULL)
				unset($content[$key]);
		
		if (!isset($id))
			$id = 0;
		$id = intval($id);
		if ($id <= 0)
			return FALSE;
		
		if (!isset($content['name']) || $content['name'] == '')
			return FALSE;
		if (!isset($content['title']))
			$content['title'] = '';
		if (!isset($content['subtitle']))
			$content['subtitle'] = '';
		if (isset($content['category']))
			$content['category'] = intval($content['category']);
		if (!isset($content['category']) || $content['category'] < 0)
			$content['category'] = 0;
		if (isset($content['status']))
			$content['status'] = intval($content['status']);
		if (!isset($content['status']) || $content['status'] < 1 || $content['status'] > 2)
			$content['status'] = 1;
		if (!isset($content['content']))
			$content['content'] = '<p>empty</p>';
		
		$content['author'] = user('id');
		
		if (isset($content['name']))
			$identifier = $content['name'];
		else
			return FALSE;
		if ($identifier == NULL || $identifier == '')
			return FALSE;
		
		$content['dateupdated'] = mktime();
		
		foreach($content as $i => $j)
		{
			switch($i)
			{
				case 'name':
				case 'title':
				case 'subtitle':
				case 'category':
				case 'author':
				case 'datecreated':
				case 'dateupdated':
				case 'datepublished':
				case 'content':
				case 'status':
					break;
				default:
					unset($content[$i]);
					break;
			}
		}
		
		$this->db->where('id', $id)->update('contents', $content);
		
		return $this->db->affected_rows();
	}
	
	public function delete($id)
	{
		if ($id > 0)
		{
			$this->db->delete('contents', array('id' => $id));
			return TRUE;
		}
		return FALSE;
	}
		
}

class Content_object {
	
	private $id					= 0;
	private $name				= '';
	private $title				= '';
	private $subtitle			= '';
	private $author				= 0;
	private $datecreated		= 0;
	private $dateupdated		= 0;
	private $datepublished		= 0;
	private $content			= '';
	private $status 			= 1;
	private $category 			= 0;
	
	private $_CI;
	
	public function __construct()
	{
		$this->_CI = & get_instance();
	}
	
	/**
	 * Content Validity Check
	 *
	 * @access public
	 */
	public function is_valid()
	{
		// if id > 0 and name not '' and author > 0 and status > 0 then return true
		if ($this->id > 0 && $this->name != '' && $this->author > 0 && $this->status > 0)
			return TRUE;
		return FALSE;
	}
	
	public function __call($method, $arguments)
	{
		switch($method)
		{
			case 'id':
			case 'name':
			case 'title':
			case 'subtitle':
			case 'datecreated':
			case 'dateupdated':
			case 'datepublished':
			case 'content':
			case 'status':
				return $this->$method;
			case 'author':
				$this->_CI->load->model('user_model', 'User');
				return $this->_CI->User->get_by_id($this->author);
			case 'category':
				$this->_CI->load->model('content_category_model', 'Content_category');
				return $this->Content_category->get_by_id($this->category);
				break;
			default:
				return call_user_func_array(array($this, $method), $arguments);
		}
	}
	
	public function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	public function __get($property)
	{
		return $this->$property;
	}
	
	public function obj()
	{
		$content = new stdClass();
		$content->id 			= $this->id;
		$content->name			=	$this->name;
		$content->title			=	$this->title;
		$content->subtitle		=	$this->subtitle;
		$content->author		=	$this->author;
		$content->datecreated	=	$this->datecreated;
		$content->dateupdated	=	$this->dateupdated;
		$content->datepublished =	$this->datepublished;
		$content->content		=	$this->content;
		$content->status		=	$this->status;
		$content->category		=	$this->category;
		return $content;
	}
}

/* End of file content_model.php */
/* Location: ./application/models/content_model.php */