<?php

class Auth_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library("session");
	}
	
	public function is_logged_in()
	{
		$user = $this->session->userdata('pen_user');
		if (empty($user) || $user == NULL || $user === NULL)
		{
			return FALSE;
		}else
		{
			return TRUE;
		}
	}
	
	/*
	 * Get current logged in user
	 */
	public function user($key="")
	{
		$user = $this->session->userdata("pen_user");
		if (empty($user) || $user == NULL || $user === NULL)
		{
			return NULL;
		}
		if (isset($user->$key))
		{
			return $user->$key;
		}
		return $user;
	}
	
	public function is_unauthorized()
	{
		$u = $this->get_current_user();
		if ($u != NULL && $u !== NULL)
		{
			return FALSE;
		}
		return TRUE;
	}
	
	public function is_admin()
	{
		$user = $this->user();
		if (isset($user->group) && $user->group == 1)
		{
			return TRUE;
		}
		return FALSE;
	}

	public function is_group($group=0)
	{
		$user = $this->user();
		if (isset($user->groups) && array_search($group, $user->groups))
		{
			return TRUE;
		}
		return FALSE;
	}
	
	public function login ($username="", $password="")
	{
		if (empty($username) || empty($password))
		{
			return NULL;
		}
		
		$query=$this->db->get_where('users', array('username'=>$username),1);
					
		if ($query->num_rows() === 1)
		{
			$user=$query->row();
			$password=md5($password)==$user->password;
			unset($user->password);
			
			if ($password)
			{
				$this->db->where('id',$user->id)->update('users', array('date_lastvisit'=>date('Y-m-d')));

				$user->groups = array();
				$query = $this->db->get_where('user_group', array('user'=>$user->id));
				$query = $query->result_array();
				foreach ($query as $q)
					array_push($user->groups, $q['group']);
				
				$user->data = json_decode($user->data);
				
				$this->session->set_userdata("pen_user", $user);
				
				return TRUE;
			}
		}
		
		return NULL;		
	}
	
	public function logout()
	{
		$this->session->unset_userdata("pen_user");
		return TRUE;
	}
	
}

/* End of file auth_model.php */
/* Location: ./application/models/auth_model.php */