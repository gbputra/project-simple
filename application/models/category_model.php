<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model {

	const CATEGORY_STATUS_DRAFT = 1;
	const CATEGORY_STATUS_PUBLISHED = 2;

	private $draft = TRUE;

	/**
	 * Model Constructor
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function ignore_draft() {
		$this->draft = FALSE;
	}

	private function _normalize_criteria(&$criteria, &$article_criteria)
	{
		$this->load->model('article_model','Article');
		if ($this->draft)
		{
			$criteria['status'] = array(Category_model::CATEGORY_STATUS_DRAFT, Category_model::CATEGORY_STATUS_PUBLISHED);
			$criteria['article'] = array('status'=>array(Article_model::ARTICLE_STATUS_DRAFT, Article_model::ARTICLE_STATUS_PUBLISHED));
		}else
		{
			$criteria['status'] = Category_model::CATEGORY_STATUS_PUBLISHED;
			$criteria['article'] = array('status'=>Article_model::ARTICLE_STATUS_PUBLISHED);
		}

		$article_criteria = array();
		if (isset($criteria['article']))
		{
			$article_criteria = $criteria['article'];
			if (!is_array($article_criteria))
				$article_criteria = array();
		}
		unset($criteria['article']);
	}

	private function _set_db_criteria($criteria)
	{
		foreach ($criteria as $i => $j) {
			if (is_array($j))
			{
				if (!empty($j))
					$this->db->where_in($i,$j);
				else
					$this->db->where($i,NULL);
			}else if (!is_array($j))
				$this->db->where($i,$j);
		}
	}

	/**
	 * Get one category by id
	 *
	 * @param	integer		id of category
	 * @param	boolean		Optional. is allow get draft category
	 * @return	Category_object	requested category
	 *
	 */
	public function get_by_id($id,$criteria=array())
	{
		$category = new Category_object;

		if (!is_array($criteria))
			$criteria = array();

		if (empty($id) || $id == '' || $id == NULL)
			return $category;

		$criteria['id'] = $id;
		$article_criteria = array();

		$this->_normalize_criteria($criteria, $article_criteria);

		$this->_set_db_criteria($criteria);

		$p = $this->db->get('categories',1,0);
		if ($p->num_rows() > 0)
		{
			$p = $p->row();
			foreach ($p as $i => $j)
				$category->$i = $j;
			if (($category->view == NULL) || ($category->view === NULL) || ($category->view == ''))
				$category->view = 'category';
			$category->articles = $this->get_category_articles($category->id, -1, $article_criteria);
		}

		return $category;
	}

	/**
	 * Get one category by name
	 *
	 * @param	integer		name of category
	 * @param	boolean		Optional. is allow get draft category
	 * @return	Category_object	requested category
	 *
	 */
	public function get_by_name($name,$criteria=array())
	{
		$category = new Category_object;

		if (!is_array($criteria))
			$criteria = array();

		if (empty($name) || $name == '' || $name == NULL)
			return $category;

		$criteria['name'] = $name;
		$article_criteria = array();

		$this->_normalize_criteria($criteria, $article_criteria);

		$this->_set_db_criteria($criteria);

		$p = $this->db->get('categories',1,0);
		if ($p->num_rows() > 0)
		{
			$p = $p->row();
			foreach ($p as $i => $j)
				$category->$i = $j;
			if (($category->view == NULL) || ($category->view === NULL) || ($category->view == ''))
				$category->view = 'category';
			$category->articles = $this->get_category_articles($category->id, -1, $article_criteria);
		}

		return $category;
	}

	public function gets_by_id($id=array(),$criteria=array())
	{
		$categories = array();

		$criteria['id'] = $id;
		$article_criteria = array();

		$this->_normalize_criteria($criteria, $article_criteria);

		$this->_set_db_criteria($criteria);

		$category = $this->db->get('categories');

		if ($category->num_rows() > 0)
		{
			$category = $category->result_array();

			foreach ($category as $c)
			{
				$cr = new Category_object;
				foreach ($c as $i => $j)
					$cr->$i = $j;
				$cr->articles = $this->get_category_articles($cr->id, -1, $article_criteria);
				if ($cr->view == NULL || $cr->view === NULL || $cr->view == '')
					$cr->view = 'article';
				array_push($categories, $cr);
			}
		}
		return $categories;
	}

	/**
	 * Get some category
	 *
	 * @param	integer		Number of category that want to get. -1 mean all category
	 * @param	array		Optional. Criteria of category
	 * @return	array		array of Category_object
	 *
	 */
	public function get_num($num=-1,$offset=0,$criteria=array())
	{
		$categories = array();

		if (!is_array($criteria))
			$criteria = array();

		$article_criteria = array();

		$this->_normalize_criteria($criteria, $article_criteria);

		$this->_set_db_criteria($criteria);

		// Get categories from mysql
		if ($num >= 0)
			$pg = $this->db->get('categories', $num,$offset);
		else
			$pg = $this->db->get('categories');

		// If there is a category
		if ($pg->num_rows() > 0)
		{
			$pg = $pg->result_array();
			foreach ($pg as $p)
			{
				$category = new Category_object;

				// Set properties of category
				foreach ($p as $i => $j)
					$category->$i = $j;

				// If view invalid then view is category
				if (($category->view == NULL) || ($category->view === NULL) || ($category->view == ''))
					$category->view = 'category';

				// Set category articles
				$category->articles = $this->get_category_articles($category->id, -1, $article_criteria);
				array_push($categories, $category);
			}
		}
		return $categories;
	}

	/**
	 * Get articles id of category by id
	 *
	 * @param	integer		id of category
	 * @param	integer		Optional. number of article that want to get
	 * @param	boolean		Optional. is allow get draft articles
	 * @return	array		array of article id
	 *
	 */
	public function get_category_articles($id,$num = -1,$criteria=array())
	{
		$articles = array();

		if (!is_array($criteria))
			$criteria = array();

		if (empty($id) || $id == '' || $id == NULL)
			return $articles;

		$articles = array();

		$category_article = $this->db->get_where('articles', array('category'=>$id));

		if ($category_article->num_rows() > 0)
		{
			$category_article = $category_article->result_array();

			$c = 0;
			foreach ($category_article as $i => $j)
			{
				array_push($articles, $j['id']);
				$c++;
				if (($c >= $num) && ($num >= 0))
					break;
			}
		}

		return $articles;
	}

	/**
	 * Get the minimum category id
	 *
	 * @return	Category_object		default category
	 *
	 */
	public function default_category($criteria=array())
	{
		$category = new Category_object;

		$article_criteria = array();

		$this->_normalize_criteria($criteria, $article_criteria);

		$this->_set_db_criteria($criteria);

		$this->db->select_min('id');
		$p = $this->db->get('categories');
		if ($p->num_rows() > 0)
		{
			$p = $p->result_array();
			$p = $p[0]['id'];
			$category = $this->get_by_id($p,array('article'=>$article_criteria));
		}
		return $category;
	}

	public function home_category($criteria=array())
	{
		$category = $this->get_by_id(conf_item('home_category'),$criteria);
		if ($category == NULL || $category === NULL || !$category->is_valid())
			return $this->default_category();
		return $category;
	}

	public function save($category)
	{
		$this->load->model('auth_model', 'Auth');

		// If not admin then return false, cannot save data
		if ($this->Auth->is_logged_in() && $this->Auth->is_admin())
		{
			// Remove empty variable in category
			foreach ($category as $key => $val)
			{
				if (empty($val) || $val == '' || $val == NULL)
					unset($category[$key]);
			}

			// Define category's author by current logged user
			$category['author'] = $this->Auth->user('id');

			// Setting identifier
			if (isset($category['name']))
				$identifier = $category['name'];
			else
				return FALSE;
			if ($identifier == NULL || $identifier == '')
				return FALSE;

			// Is using table update
			$update = TRUE;

			// Setting date data article
			$c_category = $this->get_by_name($identifier,TRUE);

			$category['date_updated'] = mktime();
			if (!$c_category->is_valid())
			{
				// New article
				$update = FALSE;
				$category['date_created'] = mktime();
			}

			$articles = array();
			$articles_tmp = array();
			if (!empty($category['articles']))
				$articles_tmp = $category['articles'];

			sort($articles_tmp,SORT_NUMERIC);
			for ($i = 0; $i < count($articles_tmp); $i++) {
				if (($i == count($articles_tmp)-1) || ($articles_tmp[$i] != $articles_tmp[$i+1]))
					array_push($articles, $articles_tmp[$i]);
			}
			unset($articles_tmp);

			foreach ($category as $key => $val)
			{
				switch ($key)
				{
					case 'id':
					case 'name':
					case 'title':
					case 'subtitle':
					case 'window_title':
					case 'view':
					case 'status':
					case 'author':
					case 'date_created':
					case 'date_updated':
						break;
					default:
						unset($category[$key]);
				}
			}

			$id = $c_category->id;
			unset($category['id']);

			if ($update)
				$this->db->where('name',$category['name'])->update('categories', $category);
			else{
				$this->db->insert('categories',$category);
				$id = $this->db->insert_id();
			}

			$this->db->delete('category_article', array('category'=>$id));
			foreach ($articles as $article) {
				$this->db->insert('category_article', array('category'=>$id, 'article'=>$article));
			}
			return TRUE; // ?????
			$return = TRUE;
			if ($this->db->affected_rows() > 0)
				$return = $return && TRUE;
			else
				$return = $return && FALSE;

			return $return;
		}
		return FALSE;
	}

	public function delete($id)
	{
		if ($id > 0)
		{
			$this->db->delete('categories', array('id' => $id));
			$this->db->delete('category_article', array('category' => $id));
			return TRUE;
		}
		return FALSE;
	}

}

// ------------------------------------------------------------------------

/**
 * Gafest Category Object	category object presentation
 *
 * @author				Jauhar Arifin <jauhararifin10@gmail.com>
 *
 */

class Category_object {

	private $id				= 0;
	private $name			= '';
	private $title		 	= '';
	private $subtitle		= '';
	private $window_title	= '';
	private $author			= 0;
	private $date_created	= 0;
	private $date_updated	= 0;
	private $articles		= array();
	private $view			= 'category';
	private $status			= 1;

	private $admin			= false;

	private $_CI;

	public function __construct()
	{
		$this->_CI = & get_instance();
	}

	public function is_valid()
	{
		if ($this->id > 0 && $this->name != '' && $this->author > 0 && $this->status > 0)
			return TRUE;
		return FALSE;
	}

	public function __set($property,$value)
	{
		$this->$property = $value;
	}

	public function __call($method, $arguments)
	{
		switch($method)
		{
			case 'id':
				return $this->id;
			case 'name':
				return $this->name;
			case 'title':
				return $this->title;
			case 'subtitle':
				return $this->subtitle;
			case 'window_title':
				return $this->window_title;
			case 'author':
				$this->_CI->load->model('user_model', 'User');
				return $this->_CI->User->get_by_id($this->author);
			case 'date_created':
				return $this->date_created;
			case 'date_updated':
				return $this->date_updated;
			case 'articles':
				$this->_CI->load->model('article_model', 'Article');
				if ($this->admin)
					$this->_CI->Article->ignore_draft();
				return $this->_CI->Article->gets_by_id($this->articles);
			case 'view':
				$this->view = ''.$this->view;
				return ($this->view == '') ? 'category' : $this->view;
			case 'status':
				return $this->status;
			default:
				return call_user_func_array(array($this, $method), $arguments);
		}
	}

	public function __get($property)
	{
		switch ($property)
		{
			case 'url':
				return config_item('base_url').'category/'.$this->name;
				break;
			default:
				return $this->$property;
		}

		// Useless
		return NULL;
	}

	public function obj()
	{
		$content = new stdClass();
		$content->id 			= 	$this->id;
		$content->name			=	$this->name;
		$content->title			=	$this->title;
		$content->subtitle		=	$this->subtitle;
		$content->window_title	=	$this->window_title;
		$content->author		=	$this->author;
		$content->date_created	=	$this->date_created;
		$content->date_updated	=	$this->date_updated;
		$content->articles		=	$this->articles;
		$content->view			=	$this->view;
		$content->status		=	$this->status;
		return $content;
	}

}

/* End of file category_model.php */
/* Location: ./application/model/category_model.php */
