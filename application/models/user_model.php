<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_by_id($id)
	{
		$user = new User_object;
		
		$id = intval($id);
		if (!is_integer($id) || $id == NULL || $id <= 0)
			return $user;
		
		$u = $this->db->get_where('users', array('id' => $id));
		if ($u->num_rows() > 0)
		{
			$u = $u->result_array();
			$u = $u[0];
			
			foreach ($u as $i => $j)
				$user->$i = $j;
			$user->data = json_decode($user->data);
			$user->groups = $this->get_user_group_by_id($user->id);
		}
		
		return $user;
	}
	
	public function get_by_username($username)
	{
		$user = new User_object;
		
		$username = (string) $uername;
		if ($username == '' || $username == NULL)
			return $user;
			
		$u = $this->db->get_where('users', array('username'=>$username));
		
		if ($u->num_rows() > 0)
		{
			$u = $u->result_array();
			$u = $u[0];
			foreach ($u as $i => $j)
			{
				$user->$i = $j;
			}
			$user->data = json_decode($user->data);
			$user->groups = $this->get_user_group_by_id($user->id);
		}
		
		return $user;
	}
	
	public function get_by_email($email)
	{
		$user = new User_object;
		
		$email = (string) $email;
		if ($email == '' || $email == NULL)
			return $user;
		
		$u = $this->db->get_where('users', array('email'=>$email));
		if ($u->num_rows() > 0)
		{
			$u = $u->result_array();
			$u = $u[0];
			foreach ($u as $i => $j)
			{
				$user->$i = $j;
			}
			$user->data = json_decode($user->data);
			$user->groups = $this->get_user_group_by_id($user->id);
		}
		return $user;
	}
	
	public function get_user_group_by_id($id)
	{
		$groups = array();
		
		$id = intval($id);
		if (!is_integer($id) || $id <= 0 || $id == NULL)
			return $groups;
			
		$g = $this->db->get_where('user_group', array('user'=>$id));
		if ($g->num_rows() > 0)
		{
			$g = $g->result_array();
			foreach ($g as $j)
			{
				if (!in_array(intval($j['group']), $groups))
					array_push($groups, intval($j['group']));
			}
		}
		return $groups;
	}

	public function get_group_by_id($id)
	{
		$group = new Group_object;
		
		$id = intval($id);
		if (is_integer($id) || $id <= 0 || $id == NULL)
			return $group;
		
		$g = $this->db->get_where('groups', array('id'=>$id));
		if ($g->num_rows() < 0)
		{
			$g = $g->result_array();
			$g = $g[0];
			foreach ($g as $i => $j)
			{
				$group->$i = $j;
			}
		}
		return $group;
	}

	public function get_group_by_name($name)
	{
		$group = new Group_object;
		
		$name = (string) $name;
		if ($name == ''|| $name == NULL)
			return $group;
		
		$g = $this->db->get_where('groups', array('name'=>$name));
		if ($g->num_rows() < 0)
		{
			$g = $g->result_array();
			$g = $g[0];
			foreach ($g as $i => $j)
			{
				$group->$i = $j;
			}
		}
		return $group;
	}
	
	public function register($user=array(), $group=array(2))
	{
		$this->load->model("auth_model", "Auth");
		
		if (!is_logged_in())
		{
			if (!is_array($user) || empty($user))
				return FALSE;
			
			$u = array();
			
			$u['password'] = md5($user['password']);
			unset($user['password']);			
			$u['date_created'] = date('Y-m-d');
			$u['data'] = array();
			
			foreach ($user as $key => $val)
			{
				switch ($key)
				{
					case 'username':
					case 'email':
					case 'password':
					case 'fullname':
					case 'nickname':
					case 'birthdate':
					case 'gender':
						$u[$key] = $val;
						break;
					default:
						$u['data'][$key] = $val;
						unset($user[$key]);
						break;
				}
			}
			
			$u['data'] = json_encode($u['data']);
			
			$this->db->trans_strict(FALSE);
			$this->db->trans_off();
			
			$this->db->trans_begin();
						
			$this->db->insert('users', $u);
			$user_id = $this->db->insert_id();
			
			if (!is_array($group))
			{
				$group = intval($group);
				$group = array($group);
			}
			
			$gi = array();
			foreach ($group as $g)
			{
				array_push($gi, array('user'=>$user_id, 'group'=>$g));
			}			
			$this->db->insert_batch('user_group', $gi);
			
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				return FALSE;
			}
			else
			{
				$this->db->trans_commit();
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function update($user=array())
	{
		if (is_logged_in())
		{
			if (!is_array($user) || empty($user))
				return FALSE;
				
			$u = array();
			
			$u['data'] = array();
			$u['date_updated'] = date('Y-m-d');
			
			foreach ($user as $key => $val)
			{
				switch ($key)
				{
					case 'fullname':
					case 'nickname':
					case 'birthdate':
					case 'gender':
						$u[$key] = $val;
						break;
					default:
						$u['data'][$key] = $val;
						unset($user[$key]);
						break;
				}
			}
			
			$u['data'] = json_encode($u['data']);
			
			$this->db->where('id',user('id'))->update('users',$u);
			
			$affected_rows = $this->db->affected_rows();
			
			if ($affected_rows > 0)
			{			
				$user = $this->db->get_where('users',array('id'=>user('id')));
				
				if ($user->num_rows() > 0)
				{
					$user = $user->row();
					
					unset($user->password);
					$user->groups = $this->get_user_group_by_id($user->id);
					
					$user->data = json_decode($user->data);
					
					$this->session->set_userdata("pen_user", $user);
				}
			}
			return $affected_rows;
		}
		return 0;
	}
	
	public function update_password($oldpassword='',$password='')
	{
		if (is_logged_in())
		{							
			if (!is_string($password))
				$password = '';
			if (!is_string($oldpassword))
				$oldpassword = '';
			
			$check = $this->db->get_where('users', array('id'=>user('id')));
			if ($check->num_rows() > 0)
			{
				$oldpassword = md5($oldpassword);
				
				$check = $check->row();
				if ($check->password != $oldpassword)
					return FALSE;
							
				$password = md5($password);
			
				$date_updated = date('Y-m-d');
				$this->db->where('id',user('id'))->update('users',array('password'=>$password, 'date_updated'=>$date_updated));
				return $this->db->affected_rows() > 0;
			}
			return FALSE;
		}
		return FALSE;
	}
	
}

class User_object {
	
	public $id;
	public $username;
	public $email;
	public $password;
	public $groups;
	public $date_created;
	public $date_updated;
	public $fullname;
	public $nickname;
	public $data;
	public $date_lastvisit;
	public $date_birth;

	private $_CI;

	public function __construct()
	{
		$this->_CI =& get_instance();
	}
	
	public function is_valid()
	{
		$this->id = intval($this->id);
		if (is_integer($this->id) && ($this->id > 0))
			return TRUE;
		return FALSE;
	}
	
	public function __call($method, $arguments)
	{
		switch($method)
		{
			case 'id':
			case 'username':
			case 'email':
			case 'password':
			case 'groups':
			case 'date_created':
			case 'date_updated':
			case 'fullname':
			case 'nickname':
			case 'data':
			case 'date_lastvisit':
			case 'date_birth':
				return $this->$method;
			case 'default':
				return call_user_func_array(array($this, $method), $arguments);
		}
	}

	public function is_group($group='')
	{		
		$group = (string) $group;
		
		$g = M_USER()->get_group_by_name($group);
		if ($g->is_valid())
		{
			$gid = $g->id;
			if (is_array($gid, $this->groups))
				return TRUE;
		}else
		{
			$g = M_USER()->get_group_by_id($group);
			if ($g->is_valid())
			{
				$gid = $g->id;
				if (is_array($gid, $this->groups))
					return TRUE;
			}
		}
		return FALSE;
	}
	
	public function obj()
	{
		$user = new stdClass();
		$user->id 				= $this->id;
		$user->username			= $this->username;
		$user->email			= $this->email;
		$user->groups			= $this->groups;
		$user->date_created		= $this->date_created;
		$user->date_updated 	= $this->date_updated;
		$user->fullname			= $this->fullname;
		$user->nickname			= $this->nickname;
		$user->data				= $this->data;
		$user->date_lastvisit	= $this->date_lastvisit;
		$user->date_birth		= $this->date_birth;		
		return $user;
	}

}

class Group_object {

	public $id;
	public $name;
	public $description;

	public function is_valid()
	{
		if (is_integer($id) && $id > 0)
			return TRUE;
		return FALSE;
	}

}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */