<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

	public function __construct($config=array())
	{
		parent::__construct($config);
	}
	
	public function __get ($object)
    {
        $this->CI = &get_instance () ;
        return $this->CI->$object ;
    }
	
	public function alpha_numeric_dash_space($str)
	{
		return ( ! preg_match('/^([-a-z0-9_ ])+$/i', $str)) ? FALSE : TRUE;
	}
	
	public function valid_date_yyyy_mm_dd($str)
	{
		return ( ! preg_match('/^((?:19|20)\d\d)[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$/i', $str)) ? FALSE : TRUE;
	}
	
	public function valid_address($str)
	{
		return ( ! preg_match('/^([-a-z0-9_ .,\s])+$/i', $str)) ? FALSE : TRUE;
	}
	
	public function valid_phone($str)
	{
		return ( ! preg_match('/^\+?([0-9])+$/', $str)) ? FALSE : TRUE;
	}
	
}

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */