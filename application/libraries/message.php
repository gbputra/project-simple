<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message
{
   
   private $CI;
   private $local_message = array(); 
   
   public function __construct($param = "")
   {
      $this->CI=& get_instance();
      $this->CI->load->library("session");
   }
   
   public function get_messages()
   {
	  $messages = array_merge_recursive($this->local_message, (array)$this->CI->session->flashdata("gaf_message"));
	  return $messages;
   }

   public function get_messages_type($type="")
   {
	  $messages = array_merge_recursive($this->local_message, (array)$this->CI->session->flashdata("gaf_message"));
      
      if(isset($messages[$type]) && !empty($messages[$type]) && $messages[$type] != NULL && $messages[$type] !== NULL)
      {
         return $messages["type"];
      }
	  return NULL;
   }

   public function render($before="",$after="")
   {      
      $messages = array_merge_recursive($this->local_message, (array)$this->CI->session->flashdata("gaf_message"));
      
      if(empty($messages) || $messages == NULL || $messages === NULL)
      {
         return;
      }
      
      foreach ($messages as $type=>$m)
      {
         foreach ($m as $i)
         {
            echo str_replace("{type}", $type, $before);
            echo $i;
            echo str_replace("{type}", $type, $after);
         }
      }
   }
   
   public function render_type($type="",$before="",$after="",$inverse=FALSE)
   {
      $messages = array_merge_recursive($this->local_message, (array)$this->CI->session->flashdata("gaf_message"));
      
      if(isset($messages[$type]) && !empty($messages[$type]) && $messages[$type] != NULL && $messages[$type] !== NULL)
      {
         foreach ($messages[$type] as $message)
         {
            echo str_replace("{type}", $type, $before);
            echo $message;
            echo str_replace("{type}", $type, $after);  
         }
      }
   }

   public function add_message($type="",$message="", $private=FALSE)
   {
      if (!empty($message) && $message!=NULL && $message!==NULL && $message!="")
      {
         if ($private)
            $messages=$this->local_message;
         else
            $messages=$this->CI->session->flashdata("gaf_message");
         if (!isset($messages[$type]) || $messages[$type]==NULL || $messages[$type]===NULL || !is_array($messages[$type]))
         {
            $messages[$type] = array();
         }
         array_push($messages[$type],$message);
         if ($private)
            $this->local_message = $messages;
         else
            $this->CI->session->set_flashdata("gaf_message", $messages);
      }
   }
   
   public function keep_data()
   {
	   $messages = array_merge_recursive($this->local_message, (array)$this->CI->session->flashdata("gaf_message"));
	   $this->CI->session->set_flashdata('gaf_message', $messages);
   }
   
}
   
/* End of file message.php */
/* Location: ./application/libraries/message.php */
