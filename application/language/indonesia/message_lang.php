<?php

$lang['article_not_found']				= "Artikel tidak ditemukan";
$lang['article_error']					= "Kesalahan artikel";
$lang['category_not_found']				= "Kategori tidak ditemukan";
$lang['login_successfull']				= "Login  sukses";
$lang['login_authentication_error']		= "Autentification failed. Please try again.";
$lang['data_not_valid']					= "Data tidak valid";
$lang['login_information_not_valid']	= "Please try again.";
$lang['you_must_logout_first']			= "Anda harus logout terlebih dahulu";
$lang['you_must_login_first']			= "Anda harus login terlebih dahulu";
$lang['you_are_not_logged_in']			= "Anda belum melakukan login";
$lang['you_must_login_as_admin_first']	= "Anda harus login sebagai admin terlebih dahulu";
$lang['user_registered']				= "User telah dibuat";
$lang['register_error']					= "Gagal registrasi user";
$lang['logout_successfull']				= "Logout sukses";
$lang['user_updated']					= "User telah diedit";
$lang['user_update_error']				= "Tidak ada data yang diganti";
$lang['password_updated']				= "Password telah diganti";
$lang['password_update_error']			= "Penggantian password gagal";
$lang['setting_saved']					= "Pengaturan disimpan";
$lang['setting_save_error']				= "Pengaturan gagal disimpan";
$lang['content_category_deleted']		= "Content category terhapus";
$lang['content_category_delete_failed']	= "Gagal menghapus content category";
$lang['content_deleted']				= "Content terhapus";
$lang['content_delete_failed']			= "Gagal menghapus content";
$lang['content_category_created']		= "Sukses membuat content category";
$lang['create_content_category_failed']	= "Gagal membuat content category";
$lang['content_category_updated']		= "Sukses memperbarui content category";
$lang['update_content_category_failed']	= "Gagal memperbarui content category";
$lang['content_updated']				= "Sukses memperbarui content";
$lang['update_content_failed']			= "Gagal memperbarui content";
$lang['error_fetch_data']				= "Gagal mengambil data";
$lang['content_created']				= "Sukses membuat content";
$lang['create_content_error']			= "Gagal membuat content";
$lang['article_create_error']			= "Error membuat artikel";
$lang['article_created']				= "Artikel berhasil dibuat";
$lang['article_updated']				= "Artikel berhasil diperbarui";
$lang['article_update_failed']			= "Gagal memperbarui artikel";
$lang['article_deleted']				= "Artikel terhapus";
$lang['article_delete_failed']			= "Gagal menghapus artikel";

/* End of file message_lang.php */
/* Location: ./application/language/indonesia/message_lang.php */