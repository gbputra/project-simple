<?php

$lang['fullname_field']					= "Nama Lengkap";
$lang['nickname_field']					= "Nama Panggilan";
$lang['gender_field']					= "Jenis Kelamin";
$lang['birthdate_field']				= "Tanggal Lahir";
$lang['school_name_field']				= "Nama Sekolah";
$lang['school_address_field']			= "Alamat Sekolah";
$lang['grade_field']					= "Kelas";
$lang['email_field']					= "Email";
$lang['phone_number_field']				= "Nomor Telepon";
$lang['home_address_field']				= "Alamat Rumah";
$lang['home_city_field']				= "Kota";
$lang['home_province_field']			= "Provinsi";
$lang['username_field']					= "Username";
$lang['oldpassword_field']				= "Password lama";
$lang['password_field']					= "Password";
$lang['cpassword_field']				= "Konfirmasi Password";
$lang['name_field']						= "Nama";
$lang['title_field']					= "Judul";
$lang['subtitle_field']					= "Subjudul";
$lang['window_title_field']				= "Judul Window";
$lang['view_field']						= "View";
$lang['parent_field']					= "Parent";
$lang['status_field']					= "Status";
$lang['category_field']					= "Category";
$lang['content_field']					= "Content";


/* End of file form_lang.php */
/* Location: ./application/language/indonesia/form_lang.php */