<?php

$lang['register']					= "Daftar";
$lang['login']						= "Masuk";
$lang['logout']						= "Keluar";
$lang['profile']					= "Profil";
$lang['administrator']				= "Administrator";
$lang['category']					= "Kategori";
$lang['categories']					= "Kategori";
$lang['content']					= "Konten";
$lang['create_content_category']	= "Buat content category";
$lang['edit_content_category']		= "Edit content category";
$lang['create_content']				= "Buat content";
$lang['edit_content']				= "Edit content";

/* End of file common_lang.php */
/* Location: ./application/language/indonesia/common_lang.php */