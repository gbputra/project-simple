<?php

$lang['required']					= "Kotak %s harus diisi.";
$lang['isset']						= "Kotak %s harus memiliki nilai.";
$lang['valid_email']				= "Kotak %s harus berisi alamat email yang valid.";
$lang['valid_emails']				= "Kotak %s harus berisi alamat email yang valid.";
$lang['valid_url']					= "Kotak %s harus berisi URL yang valid.";
$lang['valid_ip']					= "Kotak %s harus berisi IP yang valid.";
$lang['min_length']					= "Kotak %s harus memiliki paling tidak %s karakter.";
$lang['max_length']					= "Kotak %s harus memiliki karakter yang tidak lebih dari %s.";
$lang['exact_length']				= "Kotak %s harus memiliki tepat %s karakter.";
$lang['alpha']						= "Kotak %s hanya boleh berisi karakter alfabet.";
$lang['alpha_numeric']				= "Kotak %s hanya boleh berisi karakter alfabet dan angka.";
$lang['alpha_dash']					= "Kotak %s hanya boleh berisi karakter alfabet, angka, underscore, dan dash.";
$lang['numeric']					= "Kotak %s hanya boleh berisi angka.";
$lang['is_numeric']					= "Kotak %s hanya boleh berisi karakter angka.";
$lang['integer']					= "Kotak %s hanya boleh berisi bilangan bulat.";
$lang['regex_match']				= "Kotak %s harus sesuai format.";
$lang['matches']					= "Kotak %s harus sama dengan kotak %s.";
$lang['is_unique'] 					= "Isi kotak %s sudah terpakai.";
$lang['is_natural']					= "Kotak %s harus berisi bilangan positif.";
$lang['is_natural_no_zero']			= "Kotak %s harus berisi bilangan positif bukan nol.";
$lang['decimal']					= "Kotak %s hanya boleh berisi bilangan desimal.";
$lang['less_than']					= "Kotak %s harus berisi angka kurang dari %s.";
$lang['greater_than']				= "Kotak %s harus berisi angka lebih dari %s.";
$lang['alpha_numeric_dash_space'] 	= "Kotak %s hanya boleh berisi karakter alfabet, angka, underscore, spasi, dan dash.";
$lang['valid_date_yyyy_mm_dd']		= "Kotak %s harus berisi format tanggal yang valid (yyyy-mm-dd).";
$lang['valid_address']				= "Kotak %s hanya boleh berisi karakter alfabet, angka, underscore, spasi, titik, koma, whitespace, dan dash.";
$lang['valid_phone']				= "Kotak %s harus berisi nomor telepon yang valid.";

/* End of file form_validation_lang.php */
/* Location: ./application/language/indonesia/form_validation_lang.php */