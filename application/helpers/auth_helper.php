<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! (function_exists('is_logged_in')))
{
	function is_logged_in($group=array())
	{
		if (!M_AUTH()->is_logged_in())
			return FALSE;
		$r = TRUE;
		$user = M_AUTH()->user('groups');
		
		if (is_array($group))
		{
			foreach ($group as $g)
				$r = $r && in_array($g,$user);
		}else
			$r = in_array($group,$user);
		return $r;
	}	
}

if ( ! (function_exists('user')))
{
	function user()
	{
		$args = func_get_args();
		
		if (!M_AUTH()->is_logged_in())
			return FALSE;
		$u = M_AUTH()->user($args[0]);
		for ($i = 1; $i < func_num_args(); $i++)
		{
			if (is_array($u) && array_key_exists($args[$i], $u))
			{
				$u = $u[$args[$i]];
			}else if (is_object($u) && property_exists($u, $args[$i]))
			{	
				$u = $u->$args[$i];
			}else
			{
				return FALSE;
			}
		}
		return $u;
	}
}

if ( ! (function_exists('login')))
{
	function login($username='', $password='')
	{
		return M_AUTH()->login($username, $password);
	}
}

if ( ! (function_exists('logout')))
{
	function logout()
	{
		return M_AUTH()->logout();
	}	
}

/* End of file auth_helper.php */
/* Location: ./application/helpers/auth_helper.php */