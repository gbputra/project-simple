<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! (function_exists('contnent_category_ignore_draft')))
{
	function contnent_category_ignore_draft()
	{
		M_CONTENT_CATEGORY()->ignore_draft();
	}
}

if ( ! (function_exists('get_contnent_category_by_id')))
{
	function get_contnent_category_by_id($id,$criteria=array())
	{
		return M_CONTENT_CATEGORY()->get_by_id($id,$criteria);
	}
}

if ( ! (function_exists('get_contnent_category_by_name')))
{
	function get_contnent_category_by_name($name,$criteria=array())
	{
		return M_CONTENT_CATEGORY()->get_by_name($name,$criteria);
	}
}

if ( ! (function_exists('get_contnent_categories')))
{
	function get_contnent_categories($num=-1,$criteria=array(),$offset=0)
	{
		return M_CONTENT_CATEGORY()->get_num($num,$criteria,$offset);
	}
}

if ( ! (function_exists('get_contnent_categories_by_id')))
{
	function get_contnent_categories_by_id($id=array(),$criteria=array())
	{
		return M_CONTENT_CATEGORY()->gets_by_id($id,$criteria);
	}
}

if ( ! (function_exists('get_default_contnent_category')))
{
	function get_default_contnent_category($criteria=array())
	{
		return M_CONTENT_CATEGORY()->default_category($criteria);
	}
}

/* End of file article_helper.php */
/* Location: ./application/helpers/article_helper.php */