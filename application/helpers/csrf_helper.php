<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('csrf_token_name'))
{
	function csrf_token_name()
	{
		$CI =& get_instance();
		return $CI->security->get_csrf_token_name();
	}
}

if (!function_exists('csrf_hash'))
{
	function csrf_hash()
	{
		$CI =& get_instance();
		return $CI->security->get_csrf_hash();
	}
}