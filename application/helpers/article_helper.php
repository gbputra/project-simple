<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! (function_exists('article_ignore_draft')))
{
	function article_ignore_draft()
	{
		M_ARTICLE()->ignore_draft();
	}
}

if ( ! (function_exists('get_article_by_id')))
{
	function get_article_by_id($id,$criteria=array())
	{
		return M_ARTICLE()->get_by_id($id,$criteria);
	}
}

if ( ! (function_exists('get_article_by_name')))
{
	function get_article_by_name($name,$criteria=array())
	{
		return M_ARTICLE()->get_by_name($name,$criteria);
	}
}

if ( ! (function_exists('get_articles')))
{
	function get_articles($num=-1,$criteria=array(),$offset=0)
	{
		return M_ARTICLE()->get_num($num,$criteria,$offset);
	}
}

if ( ! (function_exists('get_articles_by_id')))
{
	function get_articles_by_id($id=array(),$criteria=array())
	{
		return M_ARTICLE()->gets_by_id($id,$criteria);
	}
}

if ( ! (function_exists('get_article_categories')))
{
	function get_article_categories($id=0)
	{
		return M_ARTICLE()->get_article_categories($id);
	}
}

if ( ! (function_exists('get_default_article')))
{
	function get_default_article($criteria=array())
	{
		return M_ARTICLE()->default_article($criteria);
	}
}

/* End of file article_helper.php */
/* Location: ./application/helpers/article_helper.php */