<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! (function_exists('category_ignore_draft')))
{
	function category_ignore_draft()
	{
		M_CATEGORY()->ignore_draft();
	}
}

if ( ! (function_exists('get_category_by_id')))
{
	function get_category_by_id($id,$criteria=array())
	{
		return M_CATEGORY()->get_by_id($id,$criteria);
	}
}

if ( ! (function_exists('get_category_by_name')))
{
	function get_category_by_name($name,$criteria=array())
	{
		return M_CATEGORY()->get_by_name($name,$criteria);
	}
}

if ( ! (function_exists('get_categories')))
{
	function get_categories($num=-1,$criteria=array(),$offset=0)
	{
		return M_CATEGORY()->get_num($num,$criteria,$offset);
	}
}

if ( ! (function_exists('get_categories_by_id')))
{
	function get_categories_by_id($id=array(),$criteria=array())
	{
		return M_CATEGORY()->gets_by_id($id,$criteria);
	}
}

if ( ! (function_exists('get_default_category')))
{
	function get_default_category($criteria=array())
	{
		return M_CATEGORY()->default_category($criteria);
	}
}

/* End of file article_helper.php */
/* Location: ./application/helpers/article_helper.php */