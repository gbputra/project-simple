<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! (function_exists('conf_item')))
{
	function conf_item($item)
	{
		$items = explode('.',$item);

		$config =& get_config();
		if (count($items) > 1)
		{
			$item = $config;
			for ($i = 0; $i < count($items); $i++)
			{
				if (array_key_exists($items[$i], $item))
					$item =& $item[$items[$i]];
				else
					return FALSE;
			}
			return $item;
		}

		if ( ! isset($config[$item]))
			return FALSE;
		return $config[$item];
	}
}

/* End of file config_helper.php */
/* Location: ./application/helpers/config_helper.php */