<?php

if (!function_exists('M_USER'))
{
	function M_USER()
	{
		$_CI =& get_instance();
		$_CI->load->model('user_model','User');
		return $_CI->User;
	}
}

if (!function_exists('M_AUTH'))
{
	function M_AUTH()
	{
		$_CI =& get_instance();
		$_CI->load->model('auth_model','Auth');
		return $_CI->Auth;
	}
}

if (!function_exists('M_CATEGORY'))
{
	function M_CATEGORY()
	{
		$_CI =& get_instance();
		$_CI->load->model('category_model','Category');
		return $_CI->Category;
	}
}

if (!function_exists('M_MENU'))
{
	function M_MENU()
	{
		$_CI =& get_instance();
		$_CI->load->model('menu_model','Menu');
		return $_CI->Menu;
	}
}

if (!function_exists('M_ARTICLE'))
{
	function M_ARTICLE()
	{
		$_CI =& get_instance();
		$_CI->load->model('article_model','Article');
		return $_CI->Article;
	}
}

if (!function_exists('M_CONTENT_CATEGORY'))
{
	function M_CONTENT_CATEGORY()
	{
		$_CI =& get_instance();
		$_CI->load->model('content_category_model','Content_category');
		return $_CI->Category;
	}
}

if (!function_exists('out'))
{
	function out($str)
	{
		echo htmlentities($str);
	}
}