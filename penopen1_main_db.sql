-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2018 at 05:30 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penopen1_main_db`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `data siswa`
--
CREATE TABLE `data siswa` (
`username` varchar(31)
);

-- --------------------------------------------------------

--
-- Table structure for table `pen_articles`
--

CREATE TABLE `pen_articles` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `author` int(11) NOT NULL,
  `date_created` varchar(10) NOT NULL,
  `date_updated` varchar(10) NOT NULL,
  `date_published` varchar(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` text NOT NULL,
  `content` longtext NOT NULL,
  `category` int(11) NOT NULL,
  `view` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1:draft 2:published 3:page_visible 4:protected'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pen_articles`
--

INSERT INTO `pen_articles` (`id`, `name`, `author`, `date_created`, `date_updated`, `date_published`, `title`, `subtitle`, `content`, `category`, `view`, `status`) VALUES
(41, 'penopenlab', 12, '1403619173', '1516182969', '1403619173', 'Selamat Datang di Blog ALSTE 2012 Symphonie', 'tentang pen openlab', '<p>Untuk melihat blog, silakan pilih kategori pada menu di atas</p>\r\n\r\n<p><strong>Selamat Datang!</strong></p>\r\n', 1, 'article', 2),
(44, 'about-us', 14, '1403619958', '1470198818', '1403619958', 'Tentang Kami', 'tentang kami', '<h2><strong>Siapa Kami?</strong></h2>\r\n\r\n<p>ALSTE 2012 Symphonie merupakan .....</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Who prepare Symphonie 2012 Website?</h2>\r\n\r\n<p><img alt="" src="https://ghamdan.files.wordpress.com/2014/08/pen11.jpg" /></p>\r\n\r\n<p><strong>Ghamdan Bagus Putra</strong> Lahir di Semarang, 10 Januari 1994. Saat ini, remaja yang mempunyai panggilan akrab Ghamdan, menetap di ibukota bersama orang tuanya dan menjadi mahasiswa aktif di jurusan Sistem Informasi Universitas Bina Nusantara (Binus), Jakarta.</p>\r\n', 14, 'article', 2),
(57, 'enjociti', 14, '1407680825', '1407680846', '0', 'ENJOCITI', 'enjociti', '<p><a href="http://www.enjociti.com/" target="_blank"><img alt="" src="https://ghamdan.files.wordpress.com/2014/08/enjociti.jpg" /></a></p>\n\n<p>Date: 13/09/2014<br />\nOrganisers: Fundaci&oacute;n SOLYDEUS, Colegio San Ignacio, Scouts de la Ciencia<br />\nPlace: Tandil, Buenos Aires Province, Argentina<br />\nNumber of participants: 200<br />\nLevel: International<br />\nAge limits: 19<br />\nFee: 150<br />\nContact address:&nbsp;<a href="mailto:info@enjociti.com" target="_blank">info@enjociti.com</a><br />\nWebsite:&nbsp;<a href="http://www.enjociti.com/" target="_blank">http://www.enjociti.com</a></p>\n', 15, 'article', 1),
(59, 'penopenlab-misi', 12, '1516182888', '1516183656', '0', 'Pengantar', 'misi pen openlab', '<p>Untuk dapat menikmati fitur yang ada di dalam website ini, silakan login terlebih dahulu</p>\r\n', 1, 'article', 1),
(60, 'berita3', 12, '1516183415', '1516183648', '1516183415', 'Modifikasi Mobil 60 Juta', 'modif mobil', '<p><img alt="" src="https://akcdn.detik.net.id/community/media/visual/2018/01/17/3d8517a7-6d09-44fd-8df1-816dc444cac8_169.jpeg?w=780&q=90" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Jakarta</strong> - Pemilik mobil bermuka dua, Roni Gunawan mengaku untuk bisa menjadikan Toyota Limo tahun 2012 memiliki muka dua, dirinya harus menghabiskan waktu berbulan-bulan. Tapi berapa banyak biaya yang dikeluarkan dirinya untuk bisa menjadikan mobil bekas taksi tersebut seperti itu ya?<br />\r\n<br />\r\nTerlebih dirinya mengaku baru pertama kali memodifikasi mobil. Keinginan modifikasi mobil ini muncul secara tiba-tiba saat tengah melamun. Ia lalu ingin merelisasikan idenya dengan konsep mobil bermuka dua.</p>\r\n\r\n<p>&quot;Saya baru pertama kali modifikasi kayak gini. Total biaya yang saya habiskan sekitar Rp 60 juta, itu diluar harga mobil,&quot; kata Roni kepada detikOto di garasi GR Taksi, Jalan Babakan Cibeureum, Kota Bandung, Rabu (17/1/2018).<br />\r\n<br />\r\nIa menuturkan proses modifikasi mobil bermuka dua ini berlangsung di bengkel GR taksi yang berada di Jalan Babakan Cibeureum, Kota Bandung. Kebetulan, ia juga menjabat sebagai kepala bengkel.<br />\r\n<br />\r\n&quot;Yang modifikasi ini masih anak buah saya semua di bengkel. Ada tiga orang yang ngelas, ada dua yang ngecat. Prosesnya 3,5 bulanan (sebelumnya dikatakan menghabiskan waktu 6 bulan-Red),&quot; ungkap pria yang juga merintis GR taksi tersebut.<br />\r\n<br />\r\nBaca: <a href="https://oto.detik.com/mobil/d-3819321/ini-sosok-pemilik-mobil-bermuka-dua-di-bandung?_ga=2.242521602.1416965668.1515987007-832764529.1495769398" target="_blank">Ini Sosok Pemilik Mobil Bermuka Dua di Bandung</a><br />\r\n<br />\r\nIa mengaku cukup puas dengan hasil modifikasi mobil tersebut. Apalagi sejak awal ia tidak mempermasalahkan apabila proses modifikasi mobil tersebut gagal dilakukan anak buahnya.<br />\r\n<br />\r\n&quot;Waktu saya minta mereka modifikasi mobil saya, gagal juga gak masalah. Tapi akhirnya jadi, saya cukup puas hasilnya,&quot; kata ayah dua orang anak dan tiga cucu tersebut.<br />\r\n<br />\r\nMobil bermuka dua itu hanya memiliki satu tangki. Sedangkan knalpot menempel pada bagian pinggir mobil. Bagian interior dalam juga unik. Mobil memiliki dua kemudi dan dua pedal. Persneling juga ada dua.<br />\r\n<br />\r\n&quot;Mesin dua-duanya berfungsi. Jadi mau mundur atau maju bisa,&quot; kata Roni.</p>\r\n', 13, 'article', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pen_categories`
--

CREATE TABLE `pen_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `title` varchar(64) NOT NULL,
  `subtitle` varchar(64) NOT NULL,
  `window_title` varchar(64) NOT NULL,
  `author` int(11) NOT NULL,
  `date_created` varchar(10) NOT NULL,
  `date_updated` varchar(10) NOT NULL,
  `view` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pen_categories`
--

INSERT INTO `pen_categories` (`id`, `name`, `title`, `subtitle`, `window_title`, `author`, `date_created`, `date_updated`, `view`, `status`) VALUES
(1, 'home', 'Beranda', 'beranda', 'Beranda', 1, '1387586399', '1393746399', 'category_home', 2),
(13, 'news', 'Berita Inspiratif', 'berita inspiratif', 'Berita Inspiratif', 1, '1403619602', '1403619602', 'category', 2),
(14, 'about', 'Kenali Kami', 'kenali kami', 'Kenali Kami', 1, '1403619958', '1403619958', 'category', 2),
(15, 'event', 'Agenda Mendatang', 'agenda mendatang', 'Agenda Mendatang', 1, '1403619958', '1403619958', 'category', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pen_contents`
--

CREATE TABLE `pen_contents` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `subtitle` text NOT NULL,
  `author` int(11) NOT NULL,
  `datecreated` varchar(20) NOT NULL,
  `dateupdated` varchar(20) NOT NULL,
  `datepublished` varchar(20) NOT NULL,
  `category` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pen_contents`
--

INSERT INTO `pen_contents` (`id`, `name`, `title`, `subtitle`, `author`, `datecreated`, `dateupdated`, `datepublished`, `category`, `content`, `status`) VALUES
(1, 'pelajaran-1', 'Pelajaran 1', 'pelajaran 1', 1, '1401919200', '1401919200', '1401919200', 10, '<legend>Lesson 1</legend>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 2),
(2, 'test-content', 'Test Content', 'test content', 1, '1406261510', '0', '0', 10, '<p>Test Content</p>', 1),
(3, 'test2', 'Test 2', 'test 2', 1, '1406262254', '0', '0', 10, '<p>test 2</p>', 1),
(5, 'test2', 'Test 2', 'test 2', 1, '1406266729', '0', '0', 10, '<p>test 2</p>', 1),
(6, 'content1', 'Content 1', 'content 1', 1, '1406277258', '1407210864', '1406277258', 1, '<p><strong>Sebenarnya apa itu Karya Ilmiah?</strong></p>\n\n<p>Secara etimologi, karya ilimiah terdiri dari kata majemuk karya tulis dan ilmiah</p>\n\n<p>Seorang penulis juga harus memperhatikan tiga persoalan dasar dalam penulisan karya ilmiah, yaitu:</p>\n\n<ol>\n <li><strong>Kebahasaan</strong> (ejaan, diksi, afiksasi, kalimat, dan lain-lain),</li>\n <li><strong>Non</strong> <strong>kebahasaan</strong> (motivasi, intelegensi, dan lain-lain), dan</li>\n <li><strong>Tata penulisan</strong> (organisasi penulisan, mekanisme tulisan, sistematik tulisan, kutipan, dan lain-lain).</li>\n</ol>\n', 2),
(7, 'content2', 'Content 2', 'content 2', 1, '1406277285', '1406361059', '0', 1, '<p>Lorem ipsum dolor sit amet, in per tempor postea suscipiantur, atomorum efficiendi conclusionemque his ad, cum graeci constituto ex. No dolor copiosae dissentiet duo, eu mea autem graece appareat. Sea munere reprehendunt ei. Et audiam labores voluptatum mel, ea nec amet vivendo albucius, an iusto utinam qualisque qui. Sed et lucilius salutatus definitiones.</p>\r\n\r\n<p>Sea no audiam sanctus, qui mollis delicatissimi ad. Nam munere tempor et, ius elitr platonem ut. Qui ad quod philosophia, case novum semper mea cu, aeque interpretaris cu vis. Magna putent percipit cu duo, in per nibh nusquam placerat.</p>\r\n\r\n<p>Labore iudicabit eum et, in nisl perpetua pro, usu in ludus vitae nonumes. Usu viris dolor adversarium ut. Eos fugit concludaturque at, duo quod natum blandit te. Meis ubique albucius nam id, phaedrum legendos expetendis te cum. Eos habeo intellegat ea, eam veniam inermis phaedrum an.</p>\r\n', 1),
(8, 'content3', 'Content 3', 'content 3', 1, '1406277335', '1406361076', '0', 1, '<p>Lorem ipsum dolor sit amet, novum atomorum no sed. Vis suas scripta incorrupte cu, unum affert ea qui. Ne option diceret referrentur sed, est dolore nonumy ad. Qui purto albucius deseruisse no, at sed solum nemore. Augue vulputate referrentur eum no, ius paulo fabulas detracto eu. Eu ius dolorum appetere mandamus.</p>\r\n\r\n<p>&nbsp;Meis dicunt delicatissimi his no. Per ad soluta doming, id ipsum hendrerit per. Eam ea solum eligendi menandri. Eos unum hendrerit efficiendi te, tempor mentitum an eos, erant clita integre te eam. Ut minim soluta reprehendunt vel, duo ut impedit minimum, possit euripidis definiebas cu ius.</p>\r\n\r\n<p>&nbsp;Et sit veri placerat. Quis doming perpetua nec ex, ut cibo complectitur pri. Quo ut viderer habemus interesset. Vix eros audire reprimique te, tractatos argumentum quo no, ius dicat possit ea.</p>\r\n\r\n<p>&nbsp;Minimum nominati at eum. Consulatu evertitur ei sed, no vis recteque molestiae, est consul partem adolescens eu. Et oblique quaerendum usu. Affert aliquando tincidunt vel ad, pri tempor pertinacia te, ius in populo assentior.</p>\r\n\r\n<p>&nbsp;Enim impetus eum at, persius moderatius persequeris usu in. Has id nibh torquatos. Ridens sententiae ea mei. Suas erant pro et. Duo exerci abhorreant ei, sed copiosae persecuti in.</p>\r\n\r\n<p>&nbsp;Ludus sanctus hendrerit eum te, et sea oratio appellantur, et sit augue euismod. Mei at detraxit theophrastus complectitur. Quo integre consequuntur ex. Omnes mentitum splendide eos ne, ei wisi corrumpit consetetur est. Id omnes epicuri maiestatis ius, in purto dicit aliquid pri, ne sint equidem epicurei sit. Ad mei clita argumentum, te ludus deserunt his.</p>\r\n\r\n<p>&nbsp;Te vix nullam mediocritatem, te eos tantas discere, mei et nominati patrioque sadipscing. No denique reprimique eam. In doctus aliquid disputando eos, no duo discere tractatos, pro alii veri et. Vim vitae iudicabit in, quem labitur menandri eam ea, cum ea amet movet honestatis.</p>\r\n\r\n<p>&nbsp;Ne sed dolores pertinacia, ut labores dissentias eum. At nam scripta pericula, mel ad regione quaestio. Qui laoreet docendi at. Vim libris voluptatum cu, ex pri debet concludaturque. Nullam nostro causae nec eu, eu vis iusto molestie verterem, at repudiare disputando est.</p>\r\n\r\n<p>&nbsp;Ex autem propriae vix, at quas adipiscing interesset vix, partem fabulas ei est. Albucius vituperata voluptatibus cu mei. Ferri labitur maiestatis ad has, cum similique mnesarchum at, ut mel tale clita. At sumo nostro expetenda nec. No consul postulant sit, eu nam lucilius pericula.</p>\r\n\r\n<p>&nbsp;Ut sea velit integre theophrastus, ut vide honestatis eam. Et doctus temporibus consectetuer pro. Ex nobis intellegat has. An qui saperet omnesque. Id stet ridens euripidis eam, utroque scaevola sea ex.</p>\r\n\r\n<p>&nbsp;Vel ut augue argumentum, esse commune quaerendum ei sit. Fabulas persequeris definitiones mea eu, usu meis definitiones id. Sit iudicabit constituam delicatissimi in, ut voluptatum scripserit vel. Dicit maiorum hendrerit in usu, in magna congue utinam sed, ad exerci legere maiorum eum. Ut falli soleat instructior sit.</p>\r\n\r\n<p>&nbsp;Tempor gubergren philosophia ex sed. At has tempor aeterno probatus, nam fabellas repudiare ei, inani rationibus interpretaris pro ea. Lobortis assueverit te pri, at docendi interpretaris pro, usu laoreet inermis scaevola ex. Vel at nostro appareat, sed cu error ullamcorper, dicam nominati vis ea.</p>\r\n\r\n<p>&nbsp;Mel legimus cotidieque contentiones ne, et possit percipit vix. Id case animal vix, populo bonorum maiorum no nam. Electram assentior ea duo. Vix tantas mediocritatem ex. Has mundi congue consectetuer ea, nam te diam stet, at modo denique platonem eam.</p>\r\n\r\n<p>&nbsp;Eam ex prima omnesque, eos ad mutat veniam, at cum audire commune. Pro equidem constituto et, pri quidam viderer fierent id, sit at meis ludus. Ea vim simul dolorem, quo sale ornatus definitiones et. Hinc iudicabit at mel, impedit facilis moderatius duo ex.</p>\r\n\r\n<p>&nbsp;Mel modo intellegat cu. Ea indoctum expetenda tincidunt ius, ea usu quodsi eloquentiam. Quando mediocrem ne vis, amet tota omittantur at eos. Vero assum praesent id eum. Ea pro nihil labores, cu vero primis mea.</p>\r\n\r\n<p>&nbsp;Ne sit dolores singulis periculis, duo ei explicari repudiare interpretaris. No inermis definiebas vim, suas ignota senserit pro ex, usu ei saepe tantas ignota. Reque causae civibus qui cu, sed ad impetus civibus copiosae. Nam accusata praesent an. Efficiendi disputando ad est, est electram postulant ex, an cum dico omnes. At quo consul tempor, pri nulla solet id, cum ut vide ancillae scripserit.</p>\r\n\r\n<p>&nbsp;Ex vivendum repudiandae disputationi pro. Eu mei nisl reformidans, minimum commune voluptatum vim id. An decore noluisse euripidis mel, dicant ornatus reprimique vis te. Nisl sensibus maluisset nec ex. Discere persecuti te eam, et alia albucius eleifend cum, aeterno antiopam ex his.</p>\r\n\r\n<p>&nbsp;Mei dolorum volutpat assueverit ut, ad mazim erroribus necessitatibus per. Vim diam adipisci ad, numquam menandri ut vel. Accusata patrioque vulputate pri et, oportere suscipiantur te vim. Sit ea omnis tempor. Accusata eloquentiam eos at, sit ex verear eleifend. Discere dolorem delectus ad est, te agam tantas convenire duo, mea noster fabellas probatus te.</p>\r\n\r\n<p>&nbsp;Dicant putant viderer quo id. An quando everti gloriatur eos. Dicunt admodum reformidans ea pri. Feugiat delicata cum cu.</p>\r\n\r\n<p>&nbsp;Quodsi offendit definiebas ad vis, ea feugait salutatus sea. Quo nulla dolorum ei, qualisque persecuti mel at. Et vel semper laoreet necessitatibus, iuvaret maiestatis duo an, in primis salutandi sea. Ius decore corrumpit in, ne illum feugiat mea, ex exerci aliquid ancillae nec. An maluisset consulatu definitiones sed.</p>\r\n\r\n<p>&nbsp;Graeci utamur invidunt ne sea, te has magna deleniti scriptorem. Per ad quot liber, ne delicata signiferumque usu. Qui ne incorrupte sadipscing, perpetua reprehendunt signiferumque eos id. Ut elit causae melius mea, vis ad sonet appareat scaevola, ut sit odio probatus repudiandae. Sea at constituam inciderint repudiandae, an has veritus invidunt iudicabit, te pri postea consequat. Per omnis mentitum id, sea eros vulputate te.</p>\r\n\r\n<p>&nbsp;Ei mei ullum postulant. Id cum magna ullum evertitur. Similique mnesarchum vis ex, pro torquatos abhorreant at, modo quando persecuti ad mel. Ei mucius repudiare prodesset est, mel in saepe accusata gloriatur, pro clita intellegam honestatis ut. In habeo noster mandamus mei, est esse minimum at.</p>\r\n\r\n<p>&nbsp;Cu quo autem meliore. Doming appareat splendide ne his. Duo id postea nostrud probatus. Nam ne movet decore nonumy, ne vidit saepe eum. Mei et odio persecuti interesset.</p>\r\n\r\n<p>&nbsp;Diceret similique mel ea, in duo insolens intellegam. Putent dissentias ei duo, quot iudicabit sadipscing usu an. Movet omittantur qui.</p>\r\n', 1),
(9, 'content4', 'Content 4', 'content 4', 1, '1406277360', '1406361099', '0', 1, '<p>Lorem ipsum dolor sit amet, has nullam honestatis sadipscing ut, cum ipsum delectus forensibus ea, discere pertinax per eu. Usu perpetua consulatu temporibus cu. Nam dolor alienum eu. Alii verear iudicabit vim ad. Sint possit reprimique ne ius, ei eum porro ridens. Duo no ipsum laudem regione, graeci pericula at vel, vel convenire abhorreant vituperata ad.</p>\r\n\r\n<p>Vix ad habeo postulant. Nisl legere docendi ei ius, semper voluptatum id vis, vitae urbanitas mnesarchum eum ne. Qui eu dictas aliquip maluisset. In mentitum intellegat forensibus vim. No qualisque posidonium est, affert aliquando has cu. Et eos ullum alienum pericula, ut semper alterum tincidunt mel, ne ius primis vocibus.</p>\r\n', 1),
(14, 'content9', 'Content 9', 'content 9', 1, '1406277465', '1406361111', '0', 1, '<p>Lorem ipsum dolor sit amet, modus mnesarchum eam et. An alii quas volumus vis, id vix salutandi mediocritatem, et eruditi facilisis eum. Cum tempor eligendi mediocrem in. At soleat vidisse utroque sea, no pri nihil possim conceptam. Quo et amet constituto. Sea eros eloquentiam ut, sit illum quidam laoreet ei, mea menandri praesent te. His cu suscipit sensibus recteque, et quo mentitum appetere.</p>\r\n', 1),
(16, 'c1', 'Content 1', 'content 1', 1, '1406278768', '0', '0', 2, '<p>Content 1</p>', 1),
(17, 'c2', 'Content 2', 'content 2', 1, '1406278856', '0', '0', 2, '<p>content 2</p>', 1),
(18, 'masterpiece1', 'Master Piece 1', 'masterpiece 1', 1, '1406278887', '0', '0', 13, '<p>Master piece</p>', 1),
(19, 'masterpiece2', 'Master Piece 2', 'masterpiece 2', 1, '1406278914', '0', '0', 13, '<p>master piece 2</p>', 1),
(20, 'masterpiece3', 'Master Piece 3', 'masterpiece 3', 1, '1406278943', '0', '0', 13, '<p>master piece 3</p>', 1),
(21, 'masterpiece4', 'Master Piece 4', 'masterpiece 4', 1, '1406278970', '0', '0', 13, '<p>masterpiece 4</p>', 1),
(22, 'masterpiece5', 'Masterpiece 5', 'master piece 5', 1, '1406278997', '0', '0', 13, '<p>master piece 5</p>', 1),
(23, 'masterpiece6', 'Master Piece 6', 'master piece 6', 1, '1406279021', '0', '0', 13, '<p>master piece 6</p>', 1),
(24, 'masterpiece7', 'Masterpiece 7', 'masterpiece 7', 1, '1406279044', '0', '0', 13, '<p>master piece 7</p>', 1),
(25, 'masterpiece8', 'Masterpiece 8', 'masterpiece 8', 1, '1406279072', '0', '0', 13, '<p>master piece 8</p>', 1),
(26, 'masterpiece9', 'Masterpiece 9', 'masterpiece 9', 1, '1406279102', '0', '0', 13, '<p>master piece 9</p>', 1),
(27, 'noidea', 'No Idea', 'no idea', 1, '1406279142', '0', '0', 20, '<h1>No Idea</h1>', 1),
(28, 'wow1', 'Wow 1 EDITED', 'wow 1 edited', 1, '1406279446', '1406296886', '0', 3, '<p>WOW WOW WOW</p>\r\n', 1),
(29, 'wow2', 'WOW 2', 'wow 2', 1, '1406279478', '0', '0', 3, '<p>wow 2</p>\r\n', 1),
(30, 'wow3', 'Wow 3', 'wow 3', 1, '1406281193', '0', '0', 3, '<p>wow 3</p>\r\n', 1),
(31, 'wow4', 'Wow 4', 'wow 4', 1, '1406281213', '0', '0', 3, '<p>wow 4</p>\r\n', 1),
(32, 'wow5', 'Wow 5', 'wow 5', 1, '1406281226', '0', '0', 3, '<p>wow 5</p>\r\n', 1),
(33, 'wow6', 'Wow 6', 'wow 6', 1, '1406281239', '0', '0', 3, '<p>wow 6</p>\r\n', 1),
(34, 'wow7', 'Wow 7', 'wow 7', 1, '1406281249', '0', '0', 3, '<p>wow 7</p>\r\n', 1),
(35, 'wow8', 'Wow 8', 'wow 8', 1, '1406281261', '0', '0', 3, '<p>wow 8</p>\r\n', 1),
(36, 'wow9', 'Wow 9', 'wow 9', 1, '1406281273', '0', '0', 3, '<p>wow 9</p>\r\n', 1),
(39, 'new-one', 'New one', 'new one', 1, '1406281334', '0', '0', 41, '<p>this is a new one</p>\r\n', 1),
(40, 'test', 'Apa itu Karya Ilmiah', 'karya ilmiah', 1, '1407211018', '1407211425', '1407211018', 42, '<p><strong>Sebenarnya apa itu Karya Ilmiah?</strong></p>\n\n<p>Secara etimologi, karya ilimiah terdiri dari kata majemuk karya tulis dan ilmiah</p>\n\n<p>Seorang penulis juga harus memperhatikan tiga persoalan dasar dalam penulisan karya ilmiah, yaitu:</p>\n\n<ol>\n <li><strong>Kebahasaan</strong> (ejaan, diksi, afiksasi, kalimat, dan lain-lain),</li>\n <li><strong>Non</strong> <strong>kebahasaan</strong> (motivasi, intelegensi, dan lain-lain), dan</li>\n <li><strong>Tata penulisan</strong> (organisasi penulisan, mekanisme tulisan, sistematik tulisan, kutipan, dan lain-lain).</li>\n</ol>\n\n<p>&nbsp;</p>\n <p><img alt="" src="https://ghamdan.files.wordpress.com/2014/08/kti.jpg"  /></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', 1),
(41, 'ide', 'Dari Mana Ide Berasal', 'ide ide', 1, '1407211676', '1407211801', '0', 42, '<p>Ide? Darimana?</p>\n\n<p>Ide datang darimana saja? Bisa dari orang lain ataupun dari diri sendiri.</p>\n\n<p><strong>Dari orang lain: </strong>ide terkonsep dari guru, teman, orang lain (tinggal melaksanakan)</p>\n\n<p><strong>Dari diri sendiri: </strong>ide non-konsep dari guru, teman, orang lain atau <strong>ide karena pengalaman pribadi/ lingkungan sekitar.</strong></p>\n', 1),
(42, 'tahapan', 'Tahapan Penulisan Karya Ilmiah', 'tahap', 1, '1407211951', '0', '0', 42, '<p><strong>Tahapan-tahapan penulisan karya ilmiah:</strong></p>\n\n<ol>\n <li>Observasi (Mencari masalah)</li>\n <li>Pendalaman (Mendalami dan mencari akar masalah hasil temuan saat observasi)</li>\n <li>Pemecahan masalah (Mencari solusi)</li>\n <li>Conceptual design (Merancang solusi secara terkonsep)</li>\n <li>Eksperimen/Penelitian</li>\n <li>Analisis hasil</li>\n <li>Penulisan karya tulis ilmiah</li>\n <li>Pengaplikasian</li>\n</ol>\n', 1),
(43, 'komponen', 'Komponen Organisasi Karya Tulis Ilmiah', 'komponen', 1, '1407212126', '1407212168', '1407212126', 42, '<p><strong>Secara umum komponen organisasi karya tulis ilmiah terdiri dari tiga bagian utama: </strong></p>\n\n<p><img alt="" src="https://ghamdan.files.wordpress.com/2014/08/kti2.jpg" /></p>\n\n<p><strong>Bagian Awal</strong></p>\n\n<ul>\n <li>Sampul (halaman judul)</li>\n <li>Halaman pengesahan (kalau dipelukan)</li>\n <li>Pernyataan (jika diperlukan)</li>\n <li>Abstrak (jika diperlukan, biasanya sering diperlukan)</li>\n <li>Kata pengantar</li>\n <li>Daftar isi</li>\n <li>Daftar tabel (jika diperlukan)</li>\n</ul>\n\n<p><strong>Bagian Tengah (Isi)</strong></p>\n\n<ul>\n <li>Bab I Pendahuluan</li>\n <li>Bab II Pembahasan</li>\n <li>Bab III Penutup</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p><strong>Bab I Pendahuluan</strong></p>\n\n<p>Berisi pengantar kepermasalahan yang diangkat dalam karya tulis tersebut yang terbagi atas:</p>\n\n<ol>\n <li><strong>Latar belakang </strong></li>\n <li><strong>Rumusan masalah </strong></li>\n <li><strong>Tujuan </strong></li>\n <li>Manfaat (tentatif / jika diperlukan)</li>\n <li>Pertanyaan (tentatif / jika diperlukan)</li>\n</ol>\n\n<p>Dan lain-lain.</p>\n\n<p><strong>Bab II Landasan Teori</strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Berisi tentang acuan-acuan teori yang akan digunakan dalam pembuatan karya ilmiah, sebisa mungkin disesuaikan dengan kata kunci karya ilmiah</p>\n\n<p><strong>Bab III Metodologi</strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Berisi tentang metode-metode yang digunakan dalam penyusunan karya ilmiah. Metode kuantitatif (penyajian berupa angka), kualitatif (penyajian berupa deskripsi interpretatif), eksperimen, deskriptif, wawancara, literatur.</p>\n\n<p><strong>Bab I</strong><strong>V Pembahasan</strong></p>\n\n<p>Merupakan inti dari sebuah karya tulis yang berisi uraian isi/gagasan dari judul karya tulis. Perlu diperhatikan Bab II harus disesuaikan dengan rumusan permasalahan pada bab I.</p>\n\n<p>Contoh masalah:</p>\n\n<p>Bidang&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Humaniora</p>\n\n<p>Topik utama&nbsp;&nbsp;&nbsp; : Membaca pemahaman</p>\n\n<p>Maka komponen pada bab II dapat dibuat sebagai berikut:</p>\n\n<ol>\n <li>Pandangan umum tentang membaca</li>\n <li>Pengertian membaca pemahaman</li>\n <li>Fungsi membaca pemahaman</li>\n <li>Faktor-faktor yang mempengaruhi</li>\n <li>Evaluasi terhadap keterampilan membaca pemahaman</li>\n</ol>\n\n<p><strong>Bab </strong><strong>V</strong><strong> Penutup</strong></p>\n\n<p>Pada umumnya bagian penutup berisi:</p>\n\n<ol>\n <li>Simpulan</li>\n <li>Saran.</li>\n</ol>\n\n<p><strong>Bagian Akhir</strong></p>\n\n<p>Komponen-komponen yang ada pada bagian akhir suatu karya tulis ilmiah adalah:</p>\n\n<ol>\n <li>Daftar Pustaka/Daftar Referensi (website)</li>\n <li>Lampiran -&gt; Tentatif (jika ada)</li>\n</ol>\n', 1),
(44, 'sitasi', 'Menambahkan Sumber Referensi dan Mengutip ke Dalam Teks', 'Menggunakan MS Word 2010', 12, '1415372531', '1415374300', '1415372531', 5, '<h3>1. Siapkan tulisan Anda terlebih dahulu</h3>\n <p><img alt="" src="https://ghamdan.files.wordpress.com/2014/11/w01.jpg"  /></p>\n\n<h3>2. Masuk Tab <strong>REFERENCES</strong>, cari bagian <strong>Citations &amp; Biblography</strong></h3>\n\n<p><img alt="" src="https://ghamdan.files.wordpress.com/2014/11/w21.png" /></p>\n\n<h3>3. Pilih standar penulisan sitasi yang akan digunakan (misal standar Harvard, APA, dsb) pada bagian <strong>Style. </strong><em>(nb: dalam tulisan ini akan mencontohkan penggunaan sitasi dalam standar Harvard)</em></h3>\n\n<p><img alt="" src="https://ghamdan.files.wordpress.com/2014/11/w31.png" /></p>\n\n<p>4.&nbsp; Untuk menambahkan sumber referensi dan mengutipnya ke dalam teks, klik tab<br />\n<strong>REFERENCES -&gt; Citations &amp; Biblography -&gt; Insert Citation -&gt; Add New Source</strong></p>\n\n<p><img alt="" src="https://ghamdan.files.wordpress.com/2014/11/w41.png" /></p>\n\n<p>5. Akan muncul jendela Create Sources</p>\n\n<p><img alt="" src="https://ghamdan.files.wordpress.com/2014/11/w5.png" /></p>\n\n<p><em>(screenshot di atas merupakan contoh jika Type Of Source-nya berasal dari buku)</em></p>\n\n<p><strong>Type of Source</strong>: Sumber Referensi</p>\n\n<p>Pilihan Type of Source:</p>\n\n<ul>\n <li>Book</li>\n <li>Book Section (bagian tertentu dari buku)</li>\n <li>Journal Article</li>\n <li>Article in a Periodical</li>\n <li>Conference Proceedings</li>\n <li>Report</li>\n <li>Web site</li>\n <li>Document from Web site</li>\n <li>Electronic Source</li>\n <li>Art</li>\n <li>Sound recording</li>\n <li>Performance</li>\n <li>Film</li>\n <li>Interview</li>\n <li>Patent</li>\n <li>Case</li>\n <li>Miscellaneous</li>\n</ul>\n\n<p>Author : Penulis</p>\n\n<p>untuk menambahkan Author, klik Edit, maka akan muncul tampilan sebagai berikut</p>\n\n<p><img alt="" src="https://ghamdan.files.wordpress.com/2014/11/w6.png" /></p>\n\n<p>Masukkan nama belakang dan nama depan dari penulis, kemudian klik Add.</p>\n\n<p><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TIPS : Jika penulis lebih dari satu, ulangi cara sebelumnya, kemudian klik Add.</em></p>\n\n<p>Setelah selesai, klik OK.</p>\n\n<p><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TIPS : Pada jendela Create Source, centang Corporate Author bila penulis merupakan suatu organisasi</em></p>\n\n<p>Jika sudah terisi semua, klik OK</p>\n\n<p>&nbsp;</p>\n\n<p>6. Sumber referensi yang baru saja ditambahkan langsung dikutip dan dimasukkan ke dalam teks</p>\n\n<p><img alt="" src="https://ghamdan.files.wordpress.com/2014/11/w71.png" /></p>\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pen_content_categories`
--

CREATE TABLE `pen_content_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `subtitle` text NOT NULL,
  `parent` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `datecreated` varchar(20) NOT NULL,
  `dateupdated` varchar(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pen_content_categories`
--

INSERT INTO `pen_content_categories` (`id`, `name`, `title`, `subtitle`, `parent`, `author`, `datecreated`, `dateupdated`, `status`) VALUES
(1, 'bab1', 'Bab 1', 'ini bab 1', 0, 1, '1401919200', '1401919200', 2),
(4, 'bab4', 'Bab 4 EDITED', 'ini bab 4', 0, 1, '1405997936', '1406176527', 2),
(5, 'Word', 'Penggunaan Microsoft Word', 'ini sitasi', 0, 12, '1401919200', '1415372245', 2),
(6, 'bab6', 'Bab 6 EDITED', 'ini bab 6', 0, 1, '1401919200', '1405997975', 2),
(7, 'subbab1bab1', 'Sub Bab 1 Dari Bab 1', 'ini sub bab 1 dari bab 1', 1, 1, '1401919200', '1401919200', 2),
(8, 'subbab2bab1', 'Sub Bab 2 Dari Bab 1', 'ini sub bab 2 dari bab 1', 1, 1, '1401919200', '1401919200', 2),
(9, 'subbab3bab1', 'Sub Bab 3 Dari Bab 1', 'ini sub bab 3 dari bab 1', 1, 1, '1401919200', '1401919200', 2),
(10, 'subbab1subbab2bab1', 'Sub Bab 1 Dari Sub Bab 2 Dari Bab 1', 'ini sub bab 1 dari sub bab 2 dari bab 1', 8, 1, '1401919200', '1401919200', 2),
(13, 'masterpiece', 'Masterpiece', 'Masterpiece', 0, 1, '1406176572', '0', 1),
(14, 'testc', 'testc', 'testc', 1, 1, '1406203556', '0', 1),
(15, 'testd', 'testd', 'testd', 1, 1, '1406203610', '0', 1),
(28, 'teste', 'Test E', 'test e', 1, 1, '1406208416', '0', 1),
(29, 'testf', 'Test F', 'test f', 1, 1, '1406208428', '0', 1),
(30, 'testg', 'Test G', 'test g', 1, 1, '1406208436', '0', 1),
(31, 'testh', 'test H', 'test H', 1, 1, '1406208445', '0', 1),
(32, 'testi', 'Test I', 'test i', 1, 1, '1406208464', '0', 1),
(33, 'testj', 'Test J', 'test j', 1, 1, '1406208473', '0', 1),
(34, 'testk', 'Test K', 'test k', 1, 1, '1406208483', '0', 1),
(35, 'testl', 'Test L', 'test l', 1, 1, '1406208494', '0', 1),
(36, 'testm', 'Test M', 'test m', 1, 1, '1406208502', '0', 1),
(37, 'subbab2subbab2bab1', 'Sub Bab 2 Dari Sub Bab 2 Dari Bab 1', 'sub bab 2 dari sub bab 2 dari bab 1', 10, 1, '1406210526', '0', 1),
(38, 'confusing', 'Confusing', 'confusing', 8, 1, '1406210550', '0', 1),
(39, 'more-confusing', 'More Confusing', 'more confusing', 8, 1, '1406210574', '0', 1),
(40, 'subbab-more-confusing', 'Sub Bab More Confusing', 'sub bab more confusing', 39, 1, '1406210602', '0', 1),
(41, 'something-new', 'Something new', 'something new', 0, 1, '1406281316', '0', 1),
(42, 'Pengenalan', 'Pengenalan', 'ini pengenalan', 0, 1, '1407210465', '1407210770', 2),
(43, 'Sitasi', 'Sitasi', 'a', 43, 12, '1415371680', '0', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pen_groups`
--

CREATE TABLE `pen_groups` (
  `id` mediumint(8) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pen_groups`
--

INSERT INTO `pen_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'student', 'students group');

-- --------------------------------------------------------

--
-- Table structure for table `pen_menu`
--

CREATE TABLE `pen_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `items` longtext NOT NULL,
  `author` int(11) NOT NULL,
  `date_created` varchar(10) NOT NULL,
  `date_updated` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pen_menu`
--

INSERT INTO `pen_menu` (`id`, `name`, `items`, `author`, `date_created`, `date_updated`) VALUES
(8, 'main-menu', '[{"title":"Beranda","url":"http:\\/\\/localhost\\/project"},{"title":"Kenali Kami","url":"http:\\/\\/localhost\\/project/category\\/about"},{"title":"Berita Inspiratif","url":"http:\\/\\/localhost\\/project/category\\/news"},{"title":"Agenda Mendatang","url":"http:\\/\\/localhost\\/project/category\\/event"}]', 1, '1394367694', '1394370646');

-- --------------------------------------------------------

--
-- Table structure for table `pen_sessions`
--

CREATE TABLE `pen_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pen_sessions`
--

INSERT INTO `pen_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('487199554b585f6f1ac6240db83ac4cc', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', 1516183952, 'a:1:{s:9:"user_data";s:0:"";}');

-- --------------------------------------------------------

--
-- Table structure for table `pen_settings`
--

CREATE TABLE `pen_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pen_settings`
--

INSERT INTO `pen_settings` (`id`, `name`, `value`) VALUES
(1, 'theme.theme_name', 'penopenlab'),
(2, 'site_name', 'Symphonie 2012'),
(4, 'site_description', ''),
(5, 'theme.themes_path', ''),
(6, 'theme.themes_url', ''),
(7, 'home_category', '');

-- --------------------------------------------------------

--
-- Table structure for table `pen_users`
--

CREATE TABLE `pen_users` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `nickname` varchar(64) NOT NULL,
  `birthdate` date NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `data` text NOT NULL,
  `date_lastvisit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pen_users`
--

INSERT INTO `pen_users` (`id`, `username`, `email`, `password`, `date_created`, `date_updated`, `fullname`, `nickname`, `birthdate`, `gender`, `data`, `date_lastvisit`) VALUES
(1, 'jauhararifin', 'jauhararifin10@gmail.com', '7d87fe11e6df2f2cfd2e078aed5d0029', '2014-06-24', '2014-06-24', 'Jauhar Arifin', 'Jauhar', '1997-06-18', 1, '', '2015-10-03'),
(6, 'siswa1', 'siswa1@gmail.com', '013f0f67779f3b1686c604db150d12ea', '2014-06-25', '2014-07-01', 'Siswa 1', 'NS 1', '1996-06-18', 1, '{"school_name":"SMAN 3 Semarang","school_address":"jl pemuda 149 semarang\\r\\njateng","grade":"11","phone_number":"085727060366","home_address":"jl kutilang raya a.428 ungaran\\r\\nsemarang\\r\\njateng","home_city":"Semarang","home_province":"Jawa Tengah"}', '2015-08-31'),
(7, 'siswa2', 'siswa2@gmail.com', '331633a246a4e1ceefc9539a71fcd124', '2014-06-26', '0000-00-00', 'Siswa 2', 'NS 2', '1995-09-27', 1, '{"school_name":"SMAN 3 Semarang","school_address":"jl pemuda no.149 semarang\\r\\njawa tengah","grade":"12","phone_number":"081390684384","home_address":"jl kutilang raya a.428 ungaran\\r\\nsemarang\\r\\njawa tengah","home_city":"Semarang","home_province":"jawa tengah"}', '2014-07-02'),
(10, 'siswa3', 'siswa3@gmail.com', 'df8e1ec27c47f2b8223d984f87aa571e', '2014-07-02', '2014-07-02', 'Siswa 3', 'NS 3', '1999-04-07', 0, '{"school_name":"SMAN 3 Semarang","school_address":"jl pemuda 149","grade":"11","phone_number":"081687284618","home_address":"jl kutilang","home_city":"Semarang","home_province":"Bali"}', '2014-07-02'),
(11, 'dina', 'andyaupra@yahoo.com', '2582791fd85a53d5be7a069645c07b17', '2014-07-29', '0000-00-00', 'Dina', 'Dina', '2000-08-06', 0, '{"school_name":"SD WONOTINGAL 1","school_address":"SEMARANG","grade":"7","phone_number":"0816169759","home_address":"SAMBIROTO TEMBALANG SEMARANG JAWA TENGAH INDONESIA","home_city":"SEMARANG","home_province":"Jawa Tengah"}', '2014-07-31'),
(12, 'gbputra', 'anak_espero@yahoo.com', 'fb704da33ca8e10fbfcfc536f040340a', '2014-08-05', '0000-00-00', 'Ghamdan Bagus', 'Ghamdan Bagus', '1994-01-10', 1, '{"school_name":"SMA 3 SEMARANG","school_address":"JALAN PEMUDA 149","grade":"12","phone_number":"085920020035","home_address":"TENTARA PELAJAR 24","home_city":"SEMARANG","home_province":"Jawa Tengah"}', '2018-01-17'),
(13, 'aupra', 'andyauliaprahardika@gmail.com', '657e0f957564883c8cf0b503d23933b4', '2014-08-05', '0000-00-00', 'Andy Aulia Prahardika', 'Andy Aulia Prahardika', '1995-09-15', 1, '{"school_name":"SMA 3 SEMARANG","school_address":"JALAN PEMUDA SEMARANG","grade":"12","phone_number":"085811291827","home_address":"SAMBIROTO, TEMBALANG","home_city":"SEMARANG","home_province":"Jawa Tengah"}', '2014-08-12'),
(14, 'admin', 'ghamdan_bagus@yahoo.co.id', 'fb704da33ca8e10fbfcfc536f040340a', '2014-08-06', '0000-00-00', 'Administrator', 'Admin', '1999-01-01', 1, '{"school_name":"SMA 3 Semarang","school_address":"JALAN PEMUDA 149","grade":"12","phone_number":"081325344253","home_address":"TENTARA PELAJAR 24 ","home_city":"SEMARANG","home_province":"Jawa Tengah"}', '2016-12-12'),
(15, 'dewinurc', 'dewinurc@gmail.com', '52d2ab2259b37d354f02b91b0eeb883e', '2014-08-09', '0000-00-00', 'Dewi Nur Cahyaningsih', 'Dewi Nur Cahyaningsih', '1995-07-24', 0, '{"school_name":"Universitas Diponegoro","school_address":"Tembalang, Semarang","grade":"12","phone_number":"085727367667","home_address":"Jl. Pandansari Raya No. 65 Rt 07 Rw 02","home_city":"Semarang","home_province":"Jawa Tengah"}', '2014-08-09'),
(16, 'andi', 'andy.aulia.p@mail.ugm.ac.id', '2582791fd85a53d5be7a069645c07b17', '2014-08-10', '0000-00-00', 'Andi', 'Andi', '1999-01-01', 1, '{"school_name":"SD WONOTINGAL 1","school_address":"SEMARANG","grade":"7","phone_number":"0246711806","home_address":"Sambiroto","home_city":"Semarang","home_province":"Jawa Tengah"}', '2014-08-10'),
(17, 'muhammadiqbalfauzi', 'muhammad.iqbalfauzi6666@gmail.com', 'fb550e5f284a4a2ec8858add15711c47', '2014-08-14', '2014-08-14', 'muhammad iqbal fauzi', 'iqbal', '1999-01-29', 1, '{"school_name":"sma negeri 3 semarang","school_address":"jalan pemuda 149 semarang ","grade":"11","phone_number":"+6281542427347","home_address":"jalan permata ngaliyan 2,52 ngaliyan semarang","home_city":"semarang","home_province":"Jawa Tengah"}', '2014-08-14'),
(18, 'Fransjaya', 'candrajayafashionherbal@gmail.com', '77102abfd5aeca3cd8d601df8dac307a', '2014-09-24', '0000-00-00', 'Fransjaya Candra Gunawan', 'Frans', '1998-04-24', 1, '{"school_name":"SMA N 1 Semarang","school_address":"Jl. Taman Menteri Supeno No.1 Semarang","grade":"11","phone_number":"085712013421","home_address":"Pucung RT.04 RW.01 Kel.Bambankerep Kec.Ngaliyan Semarang","home_city":"Semarang","home_province":"Jawa Tengah"}', '2014-09-24'),
(19, 'fransjayaCG', 'cfransjaya@yahoo.com', '77102abfd5aeca3cd8d601df8dac307a', '2014-09-26', '0000-00-00', 'Fransjaya Candra Gunawan', 'Frans', '1998-04-24', 1, '{"school_name":"SMA N 1 Semarang","school_address":"Jl. Taman Menteri Supeno No.1 Semarang","grade":"11","phone_number":"085712013421","home_address":"Pucung RT.04 RW.01 Kel.Bambankerep Kec.Ngaliyan Semarang","home_city":"Semarang","home_province":"Jawa Tengah"}', '2014-09-26'),
(20, 'dhf', 'dimashanif12012000@gmail.com', 'f7e92613b77d4442c3c08c80f9abd01b', '2014-10-24', '0000-00-00', 'Dimas Hanif F', 'DHF', '2000-01-12', 1, '{"school_name":"SMP Negeri 5 Semarang","school_address":"Jl.Sultan Agung No 9","grade":"9","phone_number":"089663726113","home_address":"Jl.Tentara Pelajar No 24","home_city":"Semarang","home_province":"Jawa Tengah"}', '2014-10-24'),
(21, 'coba1', 'juki@juki.com', 'f63f4fbc9f8c85d409f2f59f2b9e12d5', '2015-09-16', '0000-00-00', 'Jono', 'Sujono', '1999-01-01', 1, '{"school_name":"SMA 3 Kuta","school_address":"Jalan jalan","grade":"9","phone_number":"0127895624","home_address":"aaa","home_city":"a","home_province":"Nanggroe Aceh Darussalam"}', '2015-09-16'),
(22, 'ujicoba', 'ujicoba@yahoo.com', 'fb704da33ca8e10fbfcfc536f040340a', '2016-07-11', '0000-00-00', 'aku', 'aku', '1999-01-02', 1, '{"school_name":"smp aku","school_address":"aku","grade":"8","phone_number":"79870270709","home_address":"aku","home_city":"aku","home_province":"Nanggroe Aceh Darussalam"}', '2016-12-11'),
(23, 'budiman', 'budi@budi.com', 'fb704da33ca8e10fbfcfc536f040340a', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '2016-08-05'),
(24, 'budiman01', 'budiman@budi.com', 'fb704da33ca8e10fbfcfc536f040340a', '0000-00-00', '0000-00-00', 'Budiman', 'Budi', '2015-11-03', 1, '', '0000-00-00'),
(27, 'budiman02', 'budiman02@budi.com', 'fb704da33ca8e10fbfcfc536f040340a', '2016-08-05', '2016-08-05', 'Budiman', 'budi', '2016-08-02', 1, '', '2016-08-05');

-- --------------------------------------------------------

--
-- Table structure for table `pen_user_group`
--

CREATE TABLE `pen_user_group` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pen_user_group`
--

INSERT INTO `pen_user_group` (`id`, `user`, `group`) VALUES
(1, 1, 1),
(7, 6, 2),
(8, 7, 2),
(10, 10, 2),
(11, 11, 2),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 2),
(17, 17, 2),
(18, 18, 2),
(19, 19, 2),
(20, 20, 2),
(21, 21, 2),
(22, 22, 2);

-- --------------------------------------------------------

--
-- Structure for view `data siswa`
--
DROP TABLE IF EXISTS `data siswa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`penopen1`@`localhost` SQL SECURITY DEFINER VIEW `data siswa`  AS  select 'usernamefullnamebirthdategender' AS `username` from `pen_users` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pen_articles`
--
ALTER TABLE `pen_articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `name_2` (`name`);

--
-- Indexes for table `pen_categories`
--
ALTER TABLE `pen_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `pen_contents`
--
ALTER TABLE `pen_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pen_content_categories`
--
ALTER TABLE `pen_content_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pen_groups`
--
ALTER TABLE `pen_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `pen_menu`
--
ALTER TABLE `pen_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `pen_sessions`
--
ALTER TABLE `pen_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `pen_settings`
--
ALTER TABLE `pen_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `pen_users`
--
ALTER TABLE `pen_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `pen_user_group`
--
ALTER TABLE `pen_user_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `group` (`group`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pen_articles`
--
ALTER TABLE `pen_articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `pen_categories`
--
ALTER TABLE `pen_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `pen_contents`
--
ALTER TABLE `pen_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `pen_content_categories`
--
ALTER TABLE `pen_content_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `pen_groups`
--
ALTER TABLE `pen_groups`
  MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pen_menu`
--
ALTER TABLE `pen_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pen_settings`
--
ALTER TABLE `pen_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pen_users`
--
ALTER TABLE `pen_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `pen_user_group`
--
ALTER TABLE `pen_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
